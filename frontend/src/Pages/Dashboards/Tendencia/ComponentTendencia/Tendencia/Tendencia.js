import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import Loader from 'react-loaders';
import BlockUi from 'react-block-ui';


import CanvasJSReact from '../../../../../assets/js/canvasjs.react';
import {Row, Col, Card, CardBody, InputGroup, InputGroupAddon, Input,FormGroup,Button,CardHeader,ButtonGroup} from 'reactstrap';
import {faCalendarAlt} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import DateTimeRangeContainer from 'react-advanced-datetimerange-picker';
import moment from 'moment';
import {  ResponsiveContainer} from 'recharts';
import { API_ROOT} from '../../../../../api-config';
import axios from 'axios';
import {TagServices} from '../../../../../services/comun';

import ReactTable from "react-table";
import "react-table/react-table.css";
import { CSVLink } from "react-csv";
import "../../../../../assets/css/style.css"


const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataCha = []
let dataChaExport = []
let dataChaAxisy= []
let i = 0;
let charts=[];
let sin_rep=[];
let check=true;
let check2=true;
let disabled=false;


export default class Tendencia extends Component {


    constructor(props) {
        super(props);
        let now = new Date();
        let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
        let end = moment(start).add(1, "days").subtract(1, "seconds");

        this.state = {
          start: start,
          end: end,
          TagsSelecionado: null,
          Tags:[],
          dataCharts:[],
          dataExport:[],
          dataAxisy: [],
          blocking: false,
          loaderType: 'ball-triangle-path',
          buttonExport:'none'
        };
        this.tagservices = new TagServices();
        this.applyCallback = this.applyCallback.bind(this);
        this.handleChange =this.handleChange.bind(this);
        this.filtrar =this.filtrar.bind(this);
        this.filtrar2 =this.filtrar2.bind(this);
        this.loadDataChart =this.loadDataChart.bind(this);


    }


    componentDidMount = () => {
        this.tagservices.getDataTag().then(data =>

            this.setState({Tags: data})

            );
      }

    // sortJSON=(data, key, orden)=> {
    //     return data.sort(function (a, b) {
    //         var x = a[key],
    //         y = b[key];
    
    //         if (orden === 'asc') {
    //             return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    //         }
    
    //         if (orden === 'desc') {
    //             return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    //         }
    //     });
    // }

     loadDataChart = (shortName, URL,chartcolor,unity,sw) => {

       // this.setState({blocking: true});
        const token = "tokenFalso";

        axios
          .get(URL, {
           headers: {
             'Authorization': 'Bearer ' + token
            }
          })
         .then(response => {
          
            let axysList="";
            let axysPosition="";

            if(check){
                axysList=dataChaAxisy.map(data => {
                    let suffix=data.suffix.slice(1)
                  return suffix
                })
                axysPosition=axysList.indexOf(unity)
        
                if(axysPosition!=-1){
                    i--
                }
            }
            
            
             //const dataChart = ;
            //  this.setState({gridDataChart:dataChart});

           // console.log(dataChart);
            console.time('loop2');
        //      let _dataCharts = dataChart.map( item => {
        //         return { x: moment(item.dateTime.substr(0,19)) , y : item.value };
        //       });

               //console.log(dataChart);

            //   let _dataChartsExport = dataChart.map( item => {
            //     return { x: moment(item.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm') , y : item.value.toString().replace(".",",") };
            //   });

            let mydatachart="";
            let axisy="";

            axisy= {
                labelFontSize: 11,
                lineColor: chartcolor,
                tickColor: chartcolor,
                labelFontColor: chartcolor,
                titleFontColor: chartcolor,
                suffix: " "+ unity,
                includeZero: check2
                }
            if(check){
                mydatachart = {
                axisYIndex: axysList.includes(unity) ? axysPosition : i,
                axisYType: "primary",
                type: "spline",
                legendText: shortName,
                name: shortName,
                color:chartcolor,
                dataPoints : response.data.data,
                xValueType: "dateTime",
                indexLabelFontSize:"30",
                showInLegend: true,
                markerSize: 0,
                lineThickness: 3
                }
            
            //,charts.push(mydatachart);

            if(!axysList.includes(unity))
                dataChaAxisy.push(axisy);
            dataCha.push(mydatachart);
            }else{
                mydatachart = {
                    axisYIndex:i,
                    axisYType: "primary",
                    type: "spline",
                    legendText: shortName,
                    name: shortName,
                    color:chartcolor,
                    dataPoints : response.data.data,
                    xValueType: "dateTime",
                    indexLabelFontSize:"30",
                    showInLegend: true,
                    markerSize: 0,
                    lineThickness: 3
                    }
                
                //,charts.push(mydatachart);
                dataChaAxisy.push(axisy);
                dataCha.push(mydatachart);
            }
                

            this.setState({
                dataAxisy:dataChaAxisy,
                dataCharts:dataCha
           });
            i++;
            
           
            //  let mydatachartExport = {
            //         unity : unity,
            //         sonda: shortName,
            //         measurements : _dataChartsExport
            //          }

               

             
            //                 dataExport:dataChaExport,
            //   this.setState({dataExport:dataChaExport});
            //   this.setState({dataCharts:dataCha});
             if (sw === 1)
                disabled=false;
                this.setState({blocking: false});

                console.timeEnd('loop2');
         })
         .catch(error => {
           console.log(error);
         });


     }


    filtrar =() => {
 
        charts=[];

        const {TagsSelecionado} = this.state;

        //console.log(TagsSelecionado);
        //console.log(sortJSON(TagsSelecionado,"unity","asc"));

        //this.setState({dataExport:[]});

        if (TagsSelecionado !== null){

            const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
            const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";

            dataChaAxisy = [] ;
            dataCha = [] ;
            dataChaExport = [];
            i = 0;
            const ColorChart =["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

            console.time('loop');

            
            for (let i = 0; i < TagsSelecionado.length; i++) {
            //   if(i==0){
            //     for (let j = 0; j < TagsSelecionado.length; j++) {
            //         unitys.push(TagsSelecionado[j].unity);
            //     } 
            //   }
              this.setState({blocking: true});
              const APItagMediciones = `${API_ROOT}/measurement/xy/tag/${TagsSelecionado[i]._id}/${f1}/${f2}`;

            // unitys.push(unity);
            // console.log(unitys);
            // let no_rep=unitys.filter((valor, indiceActual, arreglo) => arreglo.indexOf(valor) === indiceActual);
            
              let sw =0
              if (i=== TagsSelecionado.length- 1)
                sw = 1;
              
              this.loadDataChart(TagsSelecionado[i].shortName,APItagMediciones, ColorChart[i],TagsSelecionado[i].unity,sw);
            }
            //console.log(this.state.dataAxisy)
            //console.log(unitys)
            //console.log(charts)
            console.timeEnd('loop');



         //   console.log (dataChaAxisy);

        }


    }

    loadDataChart2 = (shortName, URL ,unity) => {

        // this.setState({blocking: true});
         const token = "tokenFalso";

         axios
           .get(URL, {
            headers: {
              'Authorization': 'Bearer ' + token
             }
           })
          .then(response => {
              const dataChart = response.data.data;
              //console.log(dataChart);
             //  this.setState({gridDataChart:dataChart});

            // console.log(dataChart);
             console.time('loop2');
            //   let _dataCharts = dataChart.map( item => {
            //      return { x: moment(item.dateTime.substr(0,19)) , y : item.value };
            //    });

                //console.log(dataChart);

               let _dataChartsExport = dataChart.map( item => {
                 return { x: moment(item.dateTime.substr(0,19)).format('DD-MM-YYYY HH:mm') , y : item.value.toString().replace(".",",") };
               });


            //   const mydatachart = {
            //      axisYIndex: i,
            //      type: "spline",
            //      legendText: shortName,
            //      name: shortName,
            //      color:chartcolor,
            //      dataPoints : response.data.data,
            //      xValueType: "dateTime",
            //      indexLabelFontSize:"30",
            //      showInLegend: true,
            //      markerSize: 0,
            //      lineThickness: 3
            //       }
            //       i++;

            //      const  axisy= {
            //          labelFontSize: 11,
            //          lineColor: chartcolor,
            //          tickColor: chartcolor,
            //          labelFontColor: chartcolor,
            //          titleFontColor: chartcolor,
            //          suffix: " "+ unity
            //         // includeZero: false
            //        }

              let mydatachartExport = {
                     unity : unity,
                     sonda: shortName,
                     measurements : _dataChartsExport
                      }

                //dataChaAxisy.push(axisy);
                 dataChaExport.push(mydatachartExport);
                //dataCha.push(mydatachart);

              this.setState({
                dataExport:dataChaExport
             });
             //console.log("dataExport: ",dataChaExport);

             //                 dataExport:dataChaExport,
             //   this.setState({dataExport:dataChaExport});
             //   this.setState({dataCharts:dataCha});

                 console.timeEnd('loop2');
                 //this.filtrar2();
          })
          .catch(error => {
            console.log(error);
          });


      }


     filtrar2 =() => {

        this.setState({buttonExport:'none'});

         const TagsSelecionado = this.state.TagsSelecionado;


         if (TagsSelecionado !== null){

            this.filtrar();

             const f1 = this.state.start.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";
             const f2 = this.state.end.format("YYYY-MM-DDTHH:mm:ss") + ".000Z";

             //dataChaAxisy = [] ;
             //dataCha = [] ;
             dataChaExport = [];
             i = 0;

             console.time('loop');
             for (let i = 0; i < TagsSelecionado.length; i++) {
               this.setState({blocking: true});
               const APItagMediciones = `${API_ROOT}/measurement/tag/${TagsSelecionado[i]._id}/${f1}/${f2}`;

               //console.log(APItagMediciones);

               this.loadDataChart2(TagsSelecionado[i].shortName,APItagMediciones,TagsSelecionado[i].unity);
             }
             console.timeEnd('loop');



          //   console.log (dataChaAxisy);

         }


     }

    applyCallback = (startDate, endDate) =>{
        this.setState({
            start: startDate,
            end: endDate
        });
    }
    handleChange = (TagsSelecionado) => {

        this.setState({ TagsSelecionado });

    }

    renderSelecTag = (Tags) =>{
        return (
          <div>
               <FormGroup>
                    <Select
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option._id}
                        isMulti
                        name="colors"
                        options={Tags}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        placeholder="Seleccione Tag"
                        onChange={this.handleChange}
                    />
                </FormGroup>
          </div>
        );
      }


    renderPickerAutoApply=(ranges, local, maxDate)=>{
        let value1 = this.state.start.format("DD-MM-YYYY")
        let value2 = this.state.end.format("DD-MM-YYYY");
        //let value2 = this.state.end.format("DD-MM-YYYY HH:mm");
        //console.log(value1);
        return (
          <div>
            <DateTimeRangeContainer
              ranges={ranges}
              start={this.state.start}
              end={this.state.end}
              local={local}
              maxDate={maxDate}
              applyCallback={this.applyCallback}
              rangeCallback={this.rangeCallback}
              autoApply
            >
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon={faCalendarAlt}/>
                        </div>
                    </InputGroupAddon>
                    <Input
                        id="formControlsTextA"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value1}
                    />
                         <Input
                        id="formControlsTextb"
                        type="text"
                        label="Text"
                        placeholder="Enter text"
                        style={{ cursor: "pointer" }}
                        disabled
                        value={value2}
                    />
                </InputGroup>

            </DateTimeRangeContainer>
          </div>
        );
      }

      inputChangeHandler = (event) => { 
            // console.log(event.target.value);  
            this.setState( { 
                ...this.state,
                [event.target.id]: event.target.value
            } );
        }

    render() {
        //dataExport
        const { dataCharts,dataAxisy,dataExport} = this.state;




        ///***********************************+++ */
           let now = new Date();
           let start = moment(new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0));
           let end = moment(start).add(1, "days").subtract(1, "seconds");
           let ranges = {
           "Solo hoy": [moment(start), moment(end)],
           "Solo ayer": [
             moment(start).subtract(1, "days"),
             moment(end).subtract(1, "days")
           ],
           "3 Dias": [moment(start).subtract(3, "days"), moment(end)],
           "5 Dias": [moment(start).subtract(5, "days"), moment(end)],
           "1 Semana": [moment(start).subtract(7, "days"), moment(end)],
           "2 Semanas": [moment(start).subtract(14, "days"), moment(end)],
           "1 Mes": [moment(start).subtract(1, "months"), moment(end)],
           "90 Dias": [moment(start).subtract(90, "days"), moment(end)],
           "1 Aaño": [moment(start).subtract(1, "years"), moment(end)]
         };
         let local = {
           format: "DD-MM-YYYY HH:mm",
           sundayFirst: false
         };
         let maxDate = moment(start).add(24, "hour");
         ///***********************************+++ */

         ///***********************************+++ */



         let optionsChart1 = {


             data: dataCharts,

             height:400,
             zoomEnabled: true,
             exportEnabled: true,
             animationEnabled: false,

             toolTip: {
                 shared: true,
                 contentFormatter: function (e) {
                     var content = " ";
                     for (var i = 0; i < e.entries.length; i++){
                         content = moment(e.entries[i].dataPoint.x).format("DDMMM HH:mm");
                      }
                      content +=   "<br/> " ;
                     for (let i = 0; i < e.entries.length; i++) {
                         // eslint-disable-next-line no-useless-concat
                         content += e.entries[i].dataSeries.name + " " + "<strong>" + e.entries[i].dataPoint.y  + "</strong>";
                         content += "<br/>";
                     }
                     return content;
                 }
             },
             legend: {
               horizontalAlign: "center",
               cursor: "pointer",
               fontSize: 11,
               itemclick: (e) => {
                   if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                       e.dataSeries.visible = false;
                   } else {
                       e.dataSeries.visible = true;
                   }
                   this.setState({renderChart:!this.state.renderChart});

               }
           },

             axisX:{
                  valueFormatString:  "DDMMM HH:mm",
                  labelFontSize: 10


             },
             axisY :
                 dataAxisy,


             }


         ///***********************************+++ */

        return (
            <Fragment>
                     <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                        Tendencia Historica
                                    </div>
                                </div>
                            </div>
                        </div>
                     <Row>

                        <Col md="12">
                            <Card className="main-card mb-1 p-0">
                                <CardBody className="p-3">
                                    <Row>
                                        <Col   md={12} lg={3}>
                                            {this.renderPickerAutoApply(ranges, local, maxDate)}
                                        </Col>
                                        <Col md={12}  lg={7}>
                                            {this.renderSelecTag(this.state.Tags.filter(t=>!t.name.includes('Totalizador1')))}
                                        </Col>
                                        <Col   md={12} lg={2}>
                                            <ButtonGroup>
                                                <Button color="primary"
                                                        outline
                                                        className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                                        disabled={disabled}
                                                        onClick={() => {
                                                            if( this.state.TagsSelecionado!=null){
                                                            disabled=true;
                                                            this.setState({
                                                                dataExport:[],
                                                                buttonExport:'block'
                                                            });
                                                             this.filtrar();
                                                            }
                                                        }}
                                                >Filtrar
                                                </Button>
                                                <label className="container" style={{marginLeft:25,marginTop:5}}  data-tooltip="Incluye cero">
                                                        <input type="checkbox" checked={check2} onClick={(()=>{
                                                            if(check2){
                                                                check2=false;
                                                            }else{
                                                                check2=true;
                                                            }
                                                        })} onChange={e => this.inputChangeHandler(e)}></input>
                                                        <span className="checkmark"></span>
                                                </label>
                                                <label className="container" style={{marginLeft:5,marginTop:5}}  data-tooltip="Igualar escala">
                                                        <input type="checkbox" checked={check} onClick={(()=>{
                                                            if(check){
                                                                check=false;
                                                            }else{
                                                                check=true;
                                                            }
                                                        })} onChange={e => this.inputChangeHandler(e)}></input>
                                                        <span className="checkmark"></span>
                                                </label>
                                                
                                            </ButtonGroup>
                                            
                                        </Col>



                                    </Row>
                                </CardBody>
                            </Card>

                        </Col>
                        <Col md="12">
                        <BlockUi tag="div" blocking={this.state.blocking}
                                    loader={<Loader active type={this.state.loaderType}/>}>
                                    <Card className="main-card mb-1 p-0">
                                        <CardBody className="p-3">
                                            <Row>
                                                <Col   md={12} lg={12}>

                                                    <ResponsiveContainer height='100%' width='100%' >
                                                        <CanvasJSChart options = {optionsChart1} className="altografico  "  />
                                                    </ResponsiveContainer>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                            </BlockUi>

                        </Col>

                    </Row>
                    <Row>
                    <Col xs="6" sm="4"></Col>
                    <Col xs="6" sm="4">
                            <Button color="primary"
                                outline
                                className={"btn-shadow btn-wide btn-outline-2x btn-block "}
                                style={{ display:this.state.buttonExport,marginTop:`${5}%` }}
                                onClick={() => {
                                        this.filtrar2();
                                        //console.log(dataExport);
                                    }}
                                >Ver Tablas de datos
                            </Button>
                    </Col>
                    <Col sm="4"></Col>


                    {console.time("loop 3")}

                    {

                         dataExport
                            .map(data=>(

                                  <Col md="3" key={data.sonda}  >
                                            <Card className="main-card mb-5 mt-3">
                                                 <CardHeader className="card-header-tab  ">
                                                 <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                     {data.sonda}
                                                </div>
                                                <div className="btn-actions-pane-right text-capitalize">
                                                        <CSVLink

                                                                        separator={";"}
                                                                        headers={
                                                                            [
                                                                                { label: data.sonda, key: "NOMBRE" },
                                                                                { label: "FECHA", key: "x" },
                                                                                { label: "VALUE("+ data.unity + ")", key: "y" }
                                                                            ]

                                                                        }
                                                                        data={data.measurements}
                                                                        filename={data.sonda +".csv"}
                                                                        className="btn btn-primary btn-shadow btn-wide btn-outline-2x pb-2 btn-block"
                                                                        target="_blank">
                                                                        Exportar Datos
                                                                    </CSVLink>
                                                </div>

                                                </CardHeader>
                                                <CardBody className="p-10 m-10">

                                                        <ReactTable
                                                            data={

                                                                data.measurements}

                                                            // loading= {false}
                                                            showPagination= {true}
                                                            showPaginationTop= {false}
                                                            showPaginationBottom= {true}
                                                            showPageSizeOptions= {false}
                                                            pageSizeOptions = {[5, 10, 20, 25, 50, 100]}
                                                            defaultPageSize={10}
                                                            columns={[
                                                                   {
                                                                    Header: "FECHA",
                                                                    accessor: "x"

                                                                    },
                                                                    {
                                                                    Header: "VALUE ("+ data.unity+ ")",
                                                                    accessor: "y",
                                                                    width: 90
                                                                    }
                                                                ]
                                                            }

                                                            className="-striped -highlight"
                                                            />
                                                </CardBody>
                                            </Card>

                                        </Col>

                         )

                            )

                    }
                    {console.timeEnd("loop 3")}


                    </Row>


















            </Fragment>
        );
    }
}
