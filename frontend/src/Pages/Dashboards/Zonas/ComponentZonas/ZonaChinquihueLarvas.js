import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
import { API_ROOT } from '../../../../api-config';
import Loader from 'react-loaders';
import {
    toast

} from 'react-toastify';
import '../style.css'

import moment from 'moment';
import {
    Row, Col,
    Button,
    CardHeader,
    Card, CardBody,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText
} from 'reactstrap';
//CustomInput,

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis,
    ReferenceLine
} from 'recharts';
import Slider from "react-slick";
import BlockUi from 'react-block-ui';
// import Ionicon from 'react-ionicons';

import ventilador from '../../../../../src/assets/utils/images/misimagenes/fan.png';
import valvula from '../../../../../src/assets/utils/images/misimagenes/pipe.png';




const S1 = "5ced38e2197f5732a44eb515";
const S2 = "5ced38e5197f5732a44eb516";
const S3 = "5ced38e9197f5732a44eb517";
const Z1 = "5ced3565197f5732a44eb50e";





export default class IndexZonaLLinquihueLarvas extends Component {
    constructor() {
        super();
        this.state = {
            myDataChart1: [],
            myDataChart2: [],
            myDataChart3: [],
            misTag: [],
            isLoading: false,
            error: null,
            modal1: false,
            setpointTemp:1,
            setpointtemps:[],
            histeresisTemp:3,
            histeresisTemps:[],
            offsetTemp:5,
            offsetTemps:[],
            alarmTemp:7,
            maxTemp:7,
            maxTemps:[],
            minTemp:9,
            minTemps:[],


            setpointHum:2,
            setpointHums:[],
            histeresisHum:4,
            histeresisHums:[],
            offsetHum:6,
            offsetHums:[],
            alarmHum:7,
            maxHum:8,
            maxHums:[],
            minHum:10,
            minHums:[],

            timeOnHum:10,
            timeOnHums:[],
            timeOffHum:10,
            timeOffHums:[],

            activoTemp: true,
            activoHum: true,
            blocking1: false,
            intentosGuardado: 0,

            bitValS1:0,
            bitVenS1:0,
            bitValS2:0,
            bitVenS2:0,
            bitValS3:0,
            bitVenS3:0,
            mySalaModal:0

        }
        this.loadDataChar = this.loadDataChar.bind(this);
        this.loadDataTag = this.loadDataTag.bind(this);
        this.toggleModal1 = this.toggleModal1.bind(this);
        this.almacenarConfig = this.almacenarConfig.bind(this);
        this.confirmacionTermino = this.confirmacionTermino.bind(this);


    }

    componentDidMount = () => {
     //   window.location.reload();
        const intervaloRefresco =240000;
        this.setState({ isLoading: true });
        this.intervalIdChart = setInterval(() => this.loadDataChar(),intervaloRefresco);
        this.loadDataChar();
        this.loadDataTag();
        this.intervalIdtag = setInterval(() => this.loadDataTag(),2000);

    }
    componentWillUnmount = () => {
       clearInterval(this.intervalIdChart);
       clearInterval(this.intervalConfirmacion);
       clearInterval(this.intervalIdtag);


    }

    loadDataTag = () => {
        const endpointmistag = `${API_ROOT}/zone/${Z1}/tag`;
       // console.log(endpointmistag);
        const token = "tokenfalso";
        axios
        .get(endpointmistag, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const mystag = response.data.data;

           const bitValS1 =  mystag.filter((tag) => tag.nameAddress === "bitValS1")[0].lastValue;
           const bitVenS1 =  mystag.filter((tag) => tag.nameAddress === "bitVenS1")[0].lastValue;
           const bitValS2 =  mystag.filter((tag) => tag.nameAddress === "bitValS2")[0].lastValue;
           const bitVenS2 =  mystag.filter((tag) => tag.nameAddress === "bitVenS2")[0].lastValue;
           const bitValS3 =  mystag.filter((tag) => tag.nameAddress === "bitValS3")[0].lastValue;
           const bitVenS3 =  mystag.filter((tag) => tag.nameAddress === "bitVenS3")[0].lastValue;
         //  console.log(bitValS1 + " - " + bitVenS1 + " - "+ bitValS2 + " - " + bitVenS2 + " - " + bitValS3 + " - " + bitVenS3 + " - ") ;

           if (this.state.bitValS1 !== bitValS1 )
                this.setState({bitValS1 });
            if (this.state.bitVenS1 !== bitVenS1 )
                this.setState({bitVenS1 });
            if (this.state.bitValS2 !== bitValS2 )
                this.setState({bitValS2 });
            if (this.state.bitVenS2 !== bitVenS2 )
                this.setState({bitVenS2 });
            if (this.state.bitValS3 !== bitValS3 )
               this.setState({bitValS3 });
            if (this.state.bitVenS3 !== bitVenS3 )
                this.setState({bitVenS3 });



            //console.log(this.state.bitValS1);


        })
        .catch(error => {
          console.log(error);
        });



    }

    loadDataChar = () => {

        let now = new Date();
        const f1 = moment(now).subtract(1, "days").format('YYYY-MM-DDT00:00:00') + ".000Z";
        const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";

        const API1 = `${API_ROOT}/tag/location/${S1}/${f1}/${f2}`;
        const API2 = `${API_ROOT}/tag/location/${S2}/${f1}/${f2}`;
        const API3 = `${API_ROOT}/tag/location/${S3}/${f1}/${f2}`;
      //  console.log(API1);
        //const token = localStorage.getItem("token");
        const token = "tokenfalso";
        axios
        .get(API1, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart1 = response.data.data;

           this.setState({ myDataChart1,isLoading: false });
        })
        .catch(error => {
          console.log(error);
        });


        axios
        .get(API2, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart2 = response.data.data;

           this.setState({ myDataChart2,isLoading: false });
        })
        .catch(error => {
          console.log(error);
        });


        axios
        .get(API3, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart3 = response.data.data;

           this.setState({ myDataChart3,isLoading: false });
        })
        .catch(error => {
          console.log(error);
        });

    }
    getMinY = (data)=> {
        return data.reduce((min, p) => p.lastValue < min ? p.lastValue  : min, data[0].lastValue );
    }
    toggleModal1(sala) {
        //console.log(sala);



        if (this.state.modal1 === false){


                let sa = "";

                switch (sala) {
                    case "1":
                        sa= "92-93";
                    break;
                    case "2":
                        sa = "94";
                    break;
                    case "3":
                        sa = "95";
                    break;
                    default:
                        sa = "No definido";
                    break;

                }
                this.setState({
                    mySalaModal:sa
                });

                const EndPointTag = `${API_ROOT}/tag`;
                let mysala = "S" + sala;
                let tags = [];
                const token = "tokenfalso";
                axios
                .get(EndPointTag, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
                })
                .then(response => {
                    tags = response.data.data;
                    //console.log(tags.filter((tag) => tag.nameAddress === "maxTempS1V"))
                    this.setState({
                        setpointTemps : tags.filter((tag) => tag.nameAddress === "setpointTemp" + mysala),
                        setpointTemp: tags.filter((tag) => tag.nameAddress === "setpointTemp" + mysala )[0].lastValue,
                        histeresisTemps : tags.filter((tag) => tag.nameAddress === "histeresisTemp" + mysala),
                        histeresisTemp: tags.filter((tag) => tag.nameAddress === "histeresisTemp" + mysala )[0].lastValue,
                        offsetTemps : tags.filter((tag) => tag.nameAddress === "offsetTemp" + mysala),
                        offsetTemp: tags.filter((tag) => tag.nameAddress === "offsetTemp" + mysala)[0].lastValue,


                        alarmTemp: tags.filter((tag) => tag.nameAddress === "alarmTemp" + mysala)[0]._id,
                        maxTemps: tags.filter((tag) => tag.nameAddress === "maxTemp" + mysala),
                        maxTemp: tags.filter((tag) => tag.nameAddress === "maxTemp" + mysala)[0].lastValue,
                        minTemps: tags.filter((tag) => tag.nameAddress === "minTemp" + mysala),
                        minTemp: tags.filter((tag) => tag.nameAddress === "minTemp" + mysala)[0].lastValue,



                        setpointHums: tags.filter((tag) => tag.nameAddress === "setpointHum" + mysala),
                        setpointHum: tags.filter((tag) => tag.nameAddress === "setpointHum" + mysala)[0].lastValue,
                        histeresisHums : tags.filter((tag) => tag.nameAddress === "histeresisHum" + mysala),
                        histeresisHum: tags.filter((tag) => tag.nameAddress === "histeresisHum" + mysala )[0].lastValue,
                        offsetHums : tags.filter((tag) => tag.nameAddress === "offsetHum" + mysala),
                        offsetHum: tags.filter((tag) => tag.nameAddress === "offsetHum" + mysala )[0].lastValue,



                        alarmHum: tags.filter((tag) => tag.nameAddress === "alarmHum" + mysala)[0]._id,
                        maxHums: tags.filter((tag) => tag.nameAddress === "maxHum" + mysala),
                        maxHum: tags.filter((tag) => tag.nameAddress === "maxHum" + mysala)[0].lastValue,
                        minHums: tags.filter((tag) => tag.nameAddress === "minHum" + mysala),
                        minHum: tags.filter((tag) => tag.nameAddress === "minHum" + mysala)[0].lastValue,


                        timeOnHums: tags.filter((tag) => tag.nameAddress === "timeOn" + mysala),
                        timeOnHum: tags.filter((tag) => tag.nameAddress === "timeOn" + mysala)[0].lastValue,
                        timeOffHums: tags.filter((tag) => tag.nameAddress === "timeOff" + mysala),
                        timeOffHum: tags.filter((tag) => tag.nameAddress === "timeOff" + mysala)[0].lastValue,
                        modal1: !this.state.modal1,
                        misTag: tags
                    });
                })
                .catch(error => {
                console.log(error);
                });
            }else{
                this.setState({
                    modal1: !this.state.modal1
                });

            }
    }


    confirmacionTermino= () => {
        this.setState({
            intentosGuardado: this.state.intentosGuardado + 1
        });


        //const EndPointTagofLocation =`${API_ROOT}/location/${S1}/tag`;
        const EndPointTag = `${API_ROOT}/tag`;
        const token = "tokenfalso";
        axios
        .get(EndPointTag, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            console.log("INTENTOS: " + this.state.intentosGuardado);
            const tags = response.data.data;
            const TagWrites  = tags.filter((tag) => tag.write === true );
            if (TagWrites.length === 0){
                clearInterval(this.intervalConfirmacion);
                this.setState({blocking1: false});
                toast['success']('Almacenado Correctamente', { autoClose: 4000 })
            }else{
                if (this.state.intentosGuardado=== 10)
                {
                  toast['error']('No se logro almacenar Correctamente', { autoClose: 4000 })
                  this.setState({blocking1: false});
                  clearInterval(this.intervalConfirmacion);
                }
            }

        })
        .catch(error => {
          console.log(error);
        });

    }




    almacenarConfig = () => {
        this.setState({blocking1: true,intentosGuardado:0});
       // console.log(sala)
       // console.log(this.state.setpointTemp + " - " + this.state.histeresisTemp + " - " +this.state.offsetTemp + " - " + this.state.maxTemp + " - " + this.state.minTemp + " - " + this.state.activoTemp);
       // console.log(this.state.setpointHum + " - " + this.state.histeresisHum + " - " +this.state.offsetHum + " - " + this.state.maxHum + " - " + this.state.minHum + " - " + this.state.activoHum);

       //PARA TEMPERATURA
       this.intervalConfirmacion = setInterval(() => this.confirmacionTermino(),3000);

       const token = "tokenfalso";
       const config = { headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token} };

       let EndPointTag = `${API_ROOT}/tag/${this.state.setpointTemps[0]._id}`;
        const  content1 = {
            "lastValue": this.state.setpointTemp,
            "lastValueWrite":this.state.setpointTemp,
            "write":true
        }
        axios.put(EndPointTag, content1, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.histeresisTemps[0]._id}`;
        const  content2 = {
            "lastValue": this.state.histeresisTemp,
            "lastValueWrite":this.state.histeresisTemp,
            "write":true
        }
        axios.put(EndPointTag, content2, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });



        EndPointTag = `${API_ROOT}/tag/${this.state.offsetTemps[0]._id}`;
        const  content3 = {
            "lastValue": this.state.offsetTemp,
            "lastValueWrite":this.state.offsetTemp,
            "write":true
        }
        axios.put(EndPointTag, content3, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.maxTemps[0]._id}`;
        const content4 = {
            "lastValue": this.state.maxTemp,
            "lastValueWrite":this.state.maxTemp,
            "write":true
        }
        axios.put(EndPointTag, content4, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });


        EndPointTag = `${API_ROOT}/tag/${this.state.minTemps[0]._id}`;
        const content5 = {
            "lastValue": this.state.minTemp,
            "lastValueWrite":this.state.minTemp,
            "write":true
        }
        axios.put(EndPointTag, content5, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });


        //SOLO PARA ACTULIZAR LOS LIMINES DE ALARMA PARA TEMPERATURA
        EndPointTag = `${API_ROOT}/tag/${this.state.alarmTemp}`;
        //console.log(EndPointTag);
        const content6 = {
            "alertMax": this.state.maxTemp,
            "alertMin": this.state.minTemp
        }
        axios.put(EndPointTag, content6, config)
        .then(response => {
        //    console.log("actualizado " + response.status);
        });



        //PARA HUMEDAD
        EndPointTag = `${API_ROOT}/tag/${this.state.setpointHums[0]._id}`;
        const  content11 = {
            "lastValue": this.state.setpointHum,
            "lastValueWrite":this.state.setpointHum,
            "write":true
        }
        axios.put(EndPointTag, content11, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.histeresisHums[0]._id}`;
        const  content22 = {
            "lastValue": this.state.histeresisHum,
            "lastValueWrite":this.state.histeresisHum,
            "write":true
        }
        axios.put(EndPointTag, content22, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });



        EndPointTag = `${API_ROOT}/tag/${this.state.offsetHums[0]._id}`;
        const  content33 = {
            "lastValue": this.state.offsetHum,
            "lastValueWrite":this.state.offsetHum,
            "write":true
        }
        axios.put(EndPointTag, content33, config)
        .then(response => {
          //  console.log("actualizado " + response.status);
        });




        EndPointTag = `${API_ROOT}/tag/${this.state.maxHums[0]._id}`;
        const content44 = {
            "lastValue": this.state.maxHum,
            "lastValueWrite":this.state.maxHum,
            "write":true
        }
        axios.put(EndPointTag, content44, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });


        EndPointTag = `${API_ROOT}/tag/${this.state.minHums[0]._id}`;
        const content55 = {
            "lastValue": this.state.minHum,
            "lastValueWrite":this.state.minHum,
            "write":true
        }
        axios.put(EndPointTag, content55, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });


        //SOLO PARA ACTULIZAR LOS LIMINES DE ALARMA PARA HUMEDAD
        EndPointTag = `${API_ROOT}/tag/${this.state.alarmHum}`;
        // console.log(EndPointTag);
        const content66 = {
            "alertMax": this.state.maxHum,
            "alertMin": this.state.minHum
        }
        axios.put(EndPointTag, content66, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });



        EndPointTag = `${API_ROOT}/tag/${this.state.timeOnHums[0]._id}`;
        const content77 = {
            "lastValue": this.state.timeOnHum,
            "lastValueWrite":this.state.timeOnHum,
            "write":true
        }
        axios.put(EndPointTag, content77, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });

        EndPointTag = `${API_ROOT}/tag/${this.state.timeOffHums[0]._id}`;
        const content88 = {
            "lastValue": this.state.timeOffHum,
            "lastValueWrite":this.state.timeOffHum,
            "write":true
        }
        axios.put(EndPointTag, content88, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });

    }

    inputChangeHandler = (event) => {
        // console.log(event.target.value);
        this.setState( {
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    checkChangeHandler = (event) => {
        // console.log(event.target.defaultChecked);
        this.setState( {
            ...this.state,
            [event.target.id]: !event.target.defaultChecked
        } );
    }










    render() {

         //const styleValvula = this.state.bitValS1===1 ? {display:'none'}:{};
         const { myDataChart1,myDataChart2,myDataChart3,isLoading, error} = this.state;
          const settings = {
            autoplaySpeed:6000,
            autoplay: true,
            centerMode: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 600,
            arrows: false,
            dots: true
        };

        const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];
        if (error) {
            return <p>{error.message}</p>;
        }

        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }

        return (
            <Fragment>


                      <div className="app-page-title">
                            <div className="page-title-wrapper">
                                <div className="page-title-heading">
                                    <div>
                                      Planta  Chinquihue C1
                                    </div>
                                </div>
                                <div className="page-title-actions">
                                    {/* Alarmado
                                    <span  className="badge badge-dot badge-dot-lg badge-danger  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span>


                                    Normal
                                    <span  className="badge badge-dot badge-dot-lg badge-success  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span>  */}
                                </div>
                            </div>
                        </div>
                        <Row>

                            <Col sm="6" lg="4" >
                                <Card className="mb-3 mr-20">
                                   <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala 92-93
                                    </div>
                                    <div className="btn-actions-pane-right text-capitalize">
                                        <span className="d-inline-block">
                                                <Button color="primary" onClick={() => this.toggleModal1("1")}
                                                    outline
                                                    className={"btn-shadow btn-wide btn-outline-2x btn-block "}>
                                                <i className="pe-7s-tools btn-icon-wrapper"> </i>

                                                </Button>

                                                    <Modal isOpen={this.state.modal1} toggle={() => this.toggleModal1("-1")} className={this.props.className}>
                                                    <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                                        <ModalHeader toggle={() => this.toggleModal1("-1")}>Configuracion Sala {this.state.mySalaModal}</ModalHeader>
                                                        <ModalBody>
                                                        <Row>
                                                        <Col  xs="0" md="2" lg="2">
                                                        </Col>
                                                            <Col  xs="6" md="4" lg="4">
                                                                <Card className="main-card mb-3">
                                                                    <CardBody>
                                                                        <CardTitle>Temperatura</CardTitle>
                                                                        <Form>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="setpointTemp" className="m-0">Set Point</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="setpointTemp" defaultValue={this.state.setpointTemp}  onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>°c</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="histeresisTemp" className="m-0">Histeresis</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="histeresisTemp" defaultValue={this.state.histeresisTemp}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>°c</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="offsetTemp" className="m-0">Offset</Label>
                                                                                <InputGroup>
                                                                                    <Input  id="offsetTemp" defaultValue={this.state.offsetTemp}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>°c</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="maxTemp" className="m-0">Max. Alarm</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="maxTemp" defaultValue={this.state.maxTemp}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>°c</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="minTemp" className="m-0">Min. Alarm</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="minTemp" defaultValue={this.state.minTemp}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>°c</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="timeOnHum2" className="m-0">Time On</Label>
                                                                                <InputGroup>
                                                                                    <Input disabled  id="timeOnHum2" defaultValue={0}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>Seg</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup   className="mt-2">
                                                                                <Label for="timeOffHum2" className="m-0">Time Off</Label>
                                                                                <InputGroup >
                                                                                    <Input disabled  id="timeOffHum2" defaultValue={0}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>Seg</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            {/* <FormGroup  className="mt-3">
                                                                                <CustomInput type="checkbox" id="activoTemp" label="Activo" defaultChecked={this.state.activoTemp} onChange={e => this.checkChangeHandler(e)}/>
                                                                            </FormGroup> */}
                                                                        </Form>
                                                                    </CardBody>
                                                                </Card>
                                                            </Col>
                                                            <Col xs="6" md="4" lg="4">
                                                                <Card className="main-card mb-3">
                                                                    <CardBody>
                                                                        <CardTitle>Humedad</CardTitle>
                                                                        <Form>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="setpointHum" className="m-0">Set Point</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="setpointHum" defaultValue={this.state.setpointHum}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>%</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="histeresisHum" className="m-0">Histeresis</Label>
                                                                                <InputGroup>
                                                                                    <Input  id="histeresisHum" defaultValue={this.state.histeresisHum}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>%</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="offsetHum" className="m-0">Offset</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="offsetHum" defaultValue={this.state.offsetHum}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>%</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="maxHum" className="m-0">Max. Alarm</Label>
                                                                                <InputGroup>
                                                                                    <Input  id="maxHum" defaultValue={this.state.maxHum}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>%</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="minHum" className="m-0">Min. Alarm</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="minHum" defaultValue={this.state.minHum}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>%</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>

                                                                            <FormGroup className="mt-2">
                                                                                <Label for="timeOnHum" className="m-0">Time On</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="timeOnHum" defaultValue={this.state.timeOnHum}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>Seg</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            <FormGroup className="mt-2">
                                                                                <Label for="timeOffHum" className="m-0">Time Off</Label>
                                                                                <InputGroup>
                                                                                    <Input   id="timeOffHum" defaultValue={this.state.timeOffHum}   onChange={e => this.inputChangeHandler(e)}/>
                                                                                    <InputGroupAddon addonType="append">
                                                                                    <InputGroupText>Seg</InputGroupText>
                                                                                    </InputGroupAddon>
                                                                                </InputGroup>
                                                                            </FormGroup>
                                                                            {/* <FormGroup  className="mt-3">
                                                                                <CustomInput type="checkbox" id="activoHum" label="Activo" defaultChecked={this.state.activoHum}  onChange={e => this.checkChangeHandler(e)}/>
                                                                            </FormGroup> */}
                                                                        </Form>
                                                                    </CardBody>
                                                                </Card>

                                                            </Col>
                                                            <Col  xs="0" md="2" lg="2">
                                                            </Col>
                                                        </Row>

                                                        </ModalBody>
                                                        <ModalFooter>

                                                                <Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button>
                                                                <Button color="primary"  onClick={() => this.almacenarConfig()}>Guardar</Button>{' '}

                                                        </ModalFooter>
                                                        </BlockUi>
                                                    </Modal>

                                        </span>
                                    </div>
                                    </CardHeader>
                                    <CardBody className="p-0">

                                    <h6 className="text-muted text-uppercase font-size-md opacity-8 pl-3 pt-3 pr-3 pb-1 font-weight-normal">
                                           Valor actual de Sondas
                                        </h6>
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-4col">
                                                <Row className="no-gutters">

                                                {

                                                             myDataChart1
                                                                 .map((data,i)=>
                                                                    <Col sm="3"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}>
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button
                                                                           onClick={ e => this.slider1.slickGoTo(i)}
                                                                           key={data._id} className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>
                                                                            {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                            <div className="size-boton mt-0  " style={{color:miscolores[i]}} >
                                                                            {data.lastValue}
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>
                                                                    </div>

                                                                 </Col>

                                                                 )

                                                         }

                                                </Row>
                                            </div>
                                        </Card>
                                        <div className="p-1 slick-slider-sm mx-auto">
                                        <Slider ref={slider1 => (this.slider1 = slider1)} {...settings}>

                                        {

                                                             myDataChart1
                                                                 .map((data,i) =>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex ">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                        {data.name}
                                                                                    </div>

                                                                                    <div className="btn-actions-pane-right text-capitalize pr-2" >
                                                                                            <div className={cx("divfloatleft mr-2",this.state.bitVenS1===0 ? 'opacity-1' : ' opacity-8 ',i===0 ? ' oculto ' : '')}>
                                                                                                <img width={23} src={ventilador} alt="" />

                                                                                            </div>
                                                                                            <div className={cx("divfloatleft mr-2", this.state.bitValS1===0  ? 'opacity-1' : 'opacity-8' ,i===1 ? ' oculto ' : '')}>
                                                                                                <img width={23} src={valvula} alt="" />

                                                                                            </div>
                                                                                     </div>



                                                                                </div>

                                                                                 {/* <div className="widget-numbers p-1 m-0">
                                                                                    <div className="widget-chart-flex">
                                                                                        <div>

                                                                                            {data.lastValue}
                                                                                            <small className="opacity-5 pl-1 size_unidad3">  {data.unity}</small>


                                                                                        </div>

                                                                                    </div>


                                                                                </div>  */}



                                                                             <div className=" opacity-8 text-focus pt-0">
                                                                                 <div className=" opacity-5 d-inline">
                                                                                  Max
                                                                                  </div>

                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>

                                                                                       <span className="pl-1 size_prom">
                                                                                            {
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline  ml-2">
                                                                                  Prom
                                                                                  </div>

                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>
                                                                                       <span className="pl-1 size_prom">
                                                                                       {
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)
                                                                                        }
                                                                                       </span>
                                                                                   </div>


                                                                              </div>


                                                                            </div>

                                                                            {/* <div className="d-inline text-secondary pr-1">
                                                                                       <span className="pl-1">
                                                                                              {data.dateTimeLastValue}

                                                                                       </span>
                                                                        </div> */}


                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={150} width='100%'>

                                                                                    <AreaChart data={data.measurements}
                                                                                            // animationDuration={1}
                                                                                            isAnimationActive = {false}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right:0,
                                                                                                left: -30,
                                                                                                bottom: 0
                                                                                            }}>

                                                                                            <Tooltip
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis
                                                                                            tick={{fontSize: '10px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />
                                                                                              <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                             <XAxis
                                                                                                    dataKey={'dateTime'}
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '10px'}}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'

                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>

                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                 )

                                                         }

                                        </Slider>
                                        </div>

                                    </CardBody>
                                </Card>
                            </Col>


                            <Col sm="6" lg="4" >
                                <Card className="mb-3 mr-20">
                                <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala 94
                                    </div>
                                    <div className="btn-actions-pane-right text-capitalize">
                                        <span className="d-inline-block">
                                                <Button color="primary" onClick={() => this.toggleModal1("2")}
                                                    outline
                                                    className={"btn-shadow btn-wide btn-outline-2x btn-block "}>
                                                <i className="pe-7s-tools btn-icon-wrapper"> </i>

                                                </Button>

                                        </span>
                                    </div>
                                    </CardHeader>
                                    <CardBody className="p-0">
                                    <h6 className="text-muted text-uppercase font-size-md opacity-8 pl-3 pt-3 pr-3 pb-1 font-weight-normal">
                                            Valor actual de Sondas
                                        </h6>
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-4col">
                                                <Row className="no-gutters">

                                                {

                                                             myDataChart2
                                                                 .map((data, i)=>
                                                                    <Col sm="3"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}>
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button key={data._id}
                                                                          onClick={ e => this.slider2.slickGoTo(i)}
                                                                           className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>
                                                                            {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                            <div className="size-boton mt-0 size_boton" style={{color:miscolores[i]}}>
                                                                            {data.lastValue}
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>
                                                                    </div>

                                                                 </Col>

                                                                 )

                                                         }

                                                </Row>
                                            </div>
                                        </Card>

                                        <div className="p-1 slick-slider-sm mx-auto">
                                        <Slider  ref={slider2 => (this.slider2 = slider2)} {...settings}>

                                        {

                                                             myDataChart2
                                                                 .map((data,i)=>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg p-2">
                                                                                <div className="widget-chart-flex">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                        {data.name}
                                                                                    </div>
                                                                                    <div className="btn-actions-pane-right text-capitalize pr-2" >
                                                                                            <div className={cx("divfloatleft mr-2",this.state.bitVenS2===0 ? 'opacity-1' : ' opacity-8 ',i===0 ? ' oculto ' : '')}>
                                                                                                <img width={23} src={ventilador} alt="" />

                                                                                            </div>
                                                                                            <div className={cx("divfloatleft mr-2", this.state.bitValS2===0  ? 'opacity-1' : 'opacity-8' ,i===1 ? ' oculto ' : '')}>
                                                                                                <img width={23} src={valvula} alt="" />

                                                                                            </div>
                                                                                    </div>

                                                                                </div>
                                                                                {/* <div className="widget-numbers p-1 m-0">
                                                                                    <div className="widget-chart-flex">
                                                                                        <div>
                                                                                            {data.lastValue}
                                                                                            <small className="opacity-5 pl-1 size_unidad3">  {data.unity}</small>
                                                                                        </div>



                                                                                    </div>
                                                                                </div> */}
                                                                                <div className=" opacity-8 text-focus pt-0">
                                                                                <div className=" opacity-5 d-inline ">
                                                                                  Max
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>

                                                                                       <span className="pl-1 size_prom">
                                                                                            {
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2">
                                                                                  Prom
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>
                                                                                       <span className="pl-1 size_prom">
                                                                                       {
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)
                                                                                        }
                                                                                       </span>
                                                                                   </div>

                                                                              </div>
                                                                            </div>

                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={150} width='100%'>

                                                                                    <AreaChart data={data.measurements}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right: 0,
                                                                                                left: -30,
                                                                                                bottom: 0
                                                                                            }}>
                                                                                               <Tooltip
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis
                                                                                            tick={{fontSize: '10px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />

                                                                                                <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                            <XAxis
                                                                                                    dataKey={'dateTime'}
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '10px'}}
                                                                                                    />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>

                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                 )

                                                         }

                                        </Slider>
                                        </div>

                                    </CardBody>
                                </Card>
                            </Col>


                            <Col sm="6" lg="4" >
                                <Card className="mb-3 mr-20">
                                <CardHeader className="card-header-tab  ">
                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>
                                        Sala 95
                                    </div>
                                    <div className="btn-actions-pane-right text-capitalize">
                                        <span className="d-inline-block">
                                                <Button color="primary" onClick={() => this.toggleModal1("3")}
                                                    outline
                                                    className={"btn-shadow btn-wide btn-outline-2x btn-block "}>
                                                <i className="pe-7s-tools btn-icon-wrapper"> </i>

                                                </Button>
                                        </span>
                                    </div>
                                    </CardHeader>
                                    <CardBody className="p-0">
                                        <h6 className="text-muted text-uppercase font-size-md opacity-8 pt-3 pl-3 pr-3 pb-1 font-weight-normal">
                                           Valor Actual de Sondas
                                        </h6>
                                        <Card className="main-card mb-0">
                                            <div className="grid-menu grid-menu-4col">
                                                <Row className="no-gutters">

                                                    {

                                                             myDataChart3
                                                                 .map((data,i)=>
                                                                    <Col sm="3"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}>
                                                                    <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                                        <Button key={data._id}
                                                                          onClick={ e => this.slider3.slickGoTo(i)}
                                                                          className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>
                                                                            {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                                            <div className="size-boton mt-0 size_boton" style={{color:miscolores[i]}}>
                                                                            {data.lastValue}
                                                                                <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                                            </div>
                                                                            <div className="widget-subheading">
                                                                            {data.shortName}
                                                                            </div>
                                                                        </Button>
                                                                    </div>

                                                                 </Col>

                                                                 )

                                                         }

                                                </Row>
                                            </div>
                                        </Card>

                                        <div className="p-1 slick-slider-sm mx-auto">
                                        <Slider ref={slider3 => (this.slider3 = slider3)} {...settings}>

                                        {

                                                             myDataChart3
                                                                 .map((data,i)=>
                                                                    <div key={data._id}>
                                                                    <div className="widget-chart widget-chart2 text-left p-0">
                                                                        <div className="widget-chat-wrapper-outer">
                                                                            <div className="widget-chart-content widget-chart-content-lg  p-2">
                                                                                <div className="widget-chart-flex">
                                                                                    <div
                                                                                        className="widget-title opacity-9 text-muted text-uppercase">
                                                                                        {data.name}
                                                                                    </div>

                                                                                    <div className="btn-actions-pane-right text-capitalize pr-2" >
                                                                                            <div className={cx("divfloatleft mr-2",this.state.bitVenS2===0 ? 'opacity-1' : ' opacity-8 ',i===0 ? ' oculto ' : '')}>
                                                                                                <img width={23} src={ventilador} alt="" />

                                                                                            </div>
                                                                                            <div className={cx("divfloatleft mr-2", this.state.bitValS2===0  ? 'opacity-1' : 'opacity-8' ,i===1 ? ' oculto ' : '')}>
                                                                                                <img width={23} src={valvula} alt="" />

                                                                                            </div>
                                                                                   </div>

                                                                                </div>
                                                                                {/* <div className="widget-numbers p-1 m-0">
                                                                                    <div className="widget-chart-flex">
                                                                                        <div>
                                                                                            {data.lastValue}
                                                                                            <small className="opacity-5 pl-1 size_unidad3">  {data.unity}</small>
                                                                                        </div>

                                                                                    </div>
                                                                                </div> */}
                                                                            <div className=" opacity-8 text-focus pt-0">
                                                                                  <div className=" opacity-5 d-inline ">
                                                                                  Max
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>

                                                                                       <span className="pl-1 size_prom ">
                                                                                            {
                                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)
                                                                                            }

                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2">
                                                                                  Prom
                                                                                  </div>
                                                                                   <div className="d-inline pr-1" style={{color:miscolores[i]}}>
                                                                                       <span className="pl-1 size_prom">
                                                                                           {
                                                                                            ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                                           }
                                                                                       </span>
                                                                                   </div>
                                                                                   <div className=" opacity-5 d-inline ml-2">
                                                                                  Min
                                                                                  </div>
                                                                                   <div className="d-inline  pr-1" style={{color:miscolores[i]}}>
                                                                                       <span className="pl-1 size_prom">
                                                                                       {
                                                                                        data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)
                                                                                        }
                                                                                       </span>
                                                                                   </div>

                                                                              </div>
                                                                            </div>
                                                                            <div
                                                                                className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                                                <ResponsiveContainer height={150} width='100%'>

                                                                                    <AreaChart data={data.measurements}
                                                                                            margin={{
                                                                                                top: 0,
                                                                                                right: 0,
                                                                                                left: -30,
                                                                                                bottom: 0
                                                                                            }}>
                                                                                               <Tooltip
                                                                                                        labelFormatter={function(value) {
                                                                                                            return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                                            }}
                                                                                                        formatter={function(value, name) {
                                                                                                        return `${value}`;
                                                                                                        }}
                                                                                                    />
                                                                                            <defs>
                                                                                                <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                                                    <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                                                    <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                                                </linearGradient>
                                                                                            </defs>
                                                                                            <YAxis
                                                                                            tick={{fontSize: '10px'}}
                                                                                            // domain={['dataMin - 4','dataMax + 4']}
                                                                                            />

                                                                                                <ReferenceLine y={data.alertMax} label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                              <ReferenceLine y={data.alertMin}  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                                            <XAxis
                                                                                                    dataKey={'dateTime'}
                                                                                                    hide = {false}
                                                                                                    tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                                                    tick={{fontSize: '10px'}}
                                                                                                   />
                                                                                            <Area type='monotoneX' dataKey='value'
                                                                                                stroke={miscolores[i]}
                                                                                                strokeWidth='3'
                                                                                                fillOpacity={1}
                                                                                                fill={"url(#colorPv" + i + ")"}/>
                                                                                        </AreaChart>

                                                                                </ResponsiveContainer>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                 )

                                                         }

                                        </Slider>
                                        </div>

                                    </CardBody>
                                </Card>
                            </Col>




                        </Row>


            </Fragment>
        )
    }
}
