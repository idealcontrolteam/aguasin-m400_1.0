import React, {Component, Fragment} from 'react';
import cx from 'classnames';
import axios from 'axios';
import { API_ROOT } from '../../../../api-config';
import Loader from 'react-loaders';
/*import {
    toast

} from 'react-toastify';*/

//import _ from 'lodash';


import '../style.css'
import moment from 'moment';
import {
    Row, Col,
    Button,
    CardHeader,
    Card, CardBody,
    Collapse,
    Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText

    /*   Modal, ModalHeader, ModalBody, ModalFooter,
    Form,
    FormGroup, Label,InputGroupAddon,
    Input,CardTitle,InputGroup,InputGroupText*/
} from 'reactstrap';
import {
    toast

} from 'react-toastify';
import BlockUi from 'react-block-ui';
//CustomInput,

import {
    ResponsiveContainer,
    AreaChart,
    Tooltip,
    Area,
    XAxis,
    YAxis,
    ReferenceLine
} from 'recharts';
import Slider from "react-slick";
import {connect} from 'react-redux';
import {TagServices} from '../../../../services/comun'
// import BlockUi from 'react-block-ui';
// import Ionicon from 'react-ionicons';


const S1 = "5dfa83a469e895a6c90005b9";
const S2 = "5ced38e2197f5732a44eb602";
const S3 = "5ced38e2197f5732a44eb603";
const S4 = "5ced38e2197f5732a44eb604";
const S5 = "5ced38e2197f5732a44eb605";
const S6 = "5ced38e2197f5732a44eb606";

const tag1="5dfa743969e895a6c90005a5";
const tag2="5dfa743969e895a6c90005a6";
const tag3="5dfa743969e895a6c90005a7";
const tag4="5dfa743969e895a6c90005a8";

const tag5="5dfcd1c069e8951a6a0027e3"; //OXD
const tag6="5dfcd1c069e8951a6a0027e4"; //PH
const tag7="5dfcd1c069e8951a6a0027e5"; 
const tag8="5dfcd1c069e8951a6a0027e6"; //Flujo
const tag9="6052678769e89545ec004d3a"; //Salinidad
const tag10="605d03c169e8957bfc002272"; //Cloruro
const miscolores= ["DODGERBLUE","#13B955","#F3B415","DARKBLUE","FORESTGREEN","#CD5C5C","#FC3939","#EFA31D","#009CDC","#d1bcf6","#593196"];

let card_disable="none";

// const Z1 = "5ced3565197f5732a44eb50e";



class IndexZonaRILES extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myDataChart1: [],
            myDataChart2: [],
            myDataChart3: [],
            myDataChart4: [],
            myDataChart5: [],
            myDataChart6: [],
            maxAlarm:0,
            minAlarm:0,
            maxAlarms:[],
            minAlarms:[],
            scaleMin:0,
            scaleMax:0,
            unity:"",
            modal1: false,
            misTag: [],
            sala:"",
            isLoading: false,
            error: null,
            titleModal:"",
            date_actualizado:"",
            Tags:[],

        }
        this.tagservices = new TagServices(); 
        this.loadDataChar = this.loadDataChar.bind(this);

        //this.loadDataTag = this.loadDataTag.bind(this);
        this.toggleModal1 = this.toggleModal1.bind(this);
        //this.almacenarConfig = this.almacenarConfig.bind(this);
        //this.confirmacionTermino = this.confirmacionTermino.bind(this);
    }



    componentDidMount = () => {
     //   window.location.reload();
        const intervaloRefresco =240000;
        this.setState({ isLoading: true });
        this.intervalIdChart = setInterval(() => this.loadDataChar(),intervaloRefresco);
        this.loadDataChar();
    }
    componentWillUnmount = () => {
       clearInterval(this.intervalIdChart);
       clearInterval(this.intervalConfirmacion);
       //clearInterval(this.intervalIdtag);
    }

    confirmacionTermino= () => {
        this.setState({
            intentosGuardado: this.state.intentosGuardado + 1
        });


        //const EndPointTagofLocation =`${API_ROOT}/location/${S1}/tag`;
        const EndPointTag = `${API_ROOT}/tag`;
        const token = "tokenfalso";
        axios
        .get(EndPointTag, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            console.log("INTENTOS: " + this.state.intentosGuardado);
            const tags = response.data.data;
            const TagWrites  = tags.filter((tag) => tag.write === true );
            if (TagWrites.length === 0 ){
                clearInterval(this.intervalConfirmacion);
                this.loadDataChar()
                this.setState({blocking1: false});
                toast['success']('Almacenado Correctamente', { autoClose: 4000 })
            }else{
                if (this.state.intentosGuardado=== 10)
                {
                  toast['error']('No se logro almacenar Correctamente', { autoClose: 4000 })
                  this.setState({blocking1: false});
                  clearInterval(this.intervalConfirmacion);
                }
            }

        })
        .catch(error => {
          console.log(error);
        });

    }

    getTags=()=>{
        this.tagservices.getTags().then(respuesta=>{
            this.setState({Tags:respuesta})
        })
    }

    loadDataChar = () => {
        this.getTags()
        // console.log("refresh");
        let now = new Date();
        const f1 = moment(now).subtract(6, "hours").format('YYYY-MM-DDT00:00:00') + ".000Z";
        const f2 = moment(now).format('YYYY-MM-DDT23:59:59') + ".000Z";

        const API1 = `${API_ROOT}/tag/location/${S1}/${f1}/${f2}`;
        // const API2 = `${API_ROOT}/tag/location/${S2}/${f1}/${f2}`;
        // const API3 = `${API_ROOT}/tag/location/${S3}/${f1}/${f2}`;
        // const API4 = `${API_ROOT}/tag/location/${S4}/${f1}/${f2}`;
        // const API5 = `${API_ROOT}/tag/location/${S5}/${f1}/${f2}`;
        // const API6 = `${API_ROOT}/tag/location/${S6}/${f1}/${f2}`;

        //console.log(API1);
        //const token = localStorage.getItem("token");
        const token = "tokenfalso";
        axios
        .get(API1, {
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataChart1 = response.data.data;
            let largo=myDataChart1[1].measurements.length;
            //this.setState({date_actualizado:myDataChart1[1].measurements[largo-1].dateTime})
            var fecha=new Date(myDataChart1[1].measurements[largo-1].dateTime);
            var zona_horaria=new Date(myDataChart1[1].measurements[largo-1].dateTime).getTimezoneOffset();
            zona_horaria=zona_horaria/60;
            fecha.setHours(fecha.getHours()+zona_horaria);
            this.setState({date_actualizado:fecha})
           // console.log(myDataChart1)

           this.setState({ myDataChart1,isLoading: false });
        })
        .catch(error => {
          console.log(error);
        });
    }

      toggleModal1(sala,id) {
        //console.log(sala);
        if (this.state.modal1 === false && id!=null){


                let sa = "";

                switch (sala) {
                    case "1":
                        sa= "1";
                    break;
                    case "2":
                        sa = "2";
                    break;
                    case "3":
                        sa = "3";
                    break;
                    case "4":
                        sa = "4";
                    break;
                    case "5":
                        sa = "5";
                    break;
                    case "6":
                        sa = "6";
                    break;
                    case "7":
                        sa = "7";
                    break;
                    case "8":
                        sa = "8";
                    break;
                    case "9":
                        sa = "9";
                    break;
                    case "10":
                        sa = "10";
                    break;
                    case "11":
                        sa = "11";
                    break;
                    default:
                        sa = "No definido";
                    break;

                }
                this.setState({
                    mySalaModal:sa
                });

                const EndPointTag = `${API_ROOT}/tag`;
                let mysala = "S" + sa;
                console.log(mysala)
                let tags = [];
                const token = "tokenfalso";
                // console.log("maxAlarm"+ mysala)
                axios
                .get(EndPointTag, {
                headers: {
                    'Authorization': 'Bearer ' + token
                }
                })
                .then(response => {
                    tags = response.data.data;
                    //console.log(tags.filter((tag) => tag.nameAddress === "maxAlarm1V"))
                    console.log(tags.filter((tag) => tag.nameAddress === "maxAlarm"+ mysala)[0]._id)
                    this.setState({
                        // alarmTemp: tags.filter((tag) => tag.nameAddress === "alarmTemp" + mysala)[0]._id,
                        maxAlarms: tags.filter((tag) => tag.nameAddress === "maxAlarm"+ mysala),
                        maxAlarm: tags.filter((tag) => tag.nameAddress === "maxAlarm"+ mysala)[0].lastValue,
                        minAlarms: tags.filter((tag) => tag.nameAddress === "minAlarm" + mysala),
                        minAlarm: tags.filter((tag) => tag.nameAddress === "minAlarm" + mysala)[0].lastValue,
                        unity: tags.filter((tag) => tag.nameAddress === "minAlarm" + mysala)[0].unity,
                        scaleMax: tags.filter(t=>t._id==id)[0].max,
                        scaleMin: tags.filter(t=>t._id==id)[0].min,
                        sala:mysala,


                        // setpointHums: tags.filter((tag) => tag.nameAddress === "setpointHum" + mysala),
                        // setpointHum: tags.filter((tag) => tag.nameAddress === "setpointHum" + mysala)[0].lastValue,
                        // histeresisHums : tags.filter((tag) => tag.nameAddress === "histeresisHum" + mysala),
                        // histeresisHum: tags.filter((tag) => tag.nameAddress === "histeresisHum" + mysala )[0].lastValue,
                        // offsetHums : tags.filter((tag) => tag.nameAddress === "offsetHum" + mysala),
                        // offsetHum: tags.filter((tag) => tag.nameAddress === "offsetHum" + mysala )[0].lastValue,



                        // alarmHum: tags.filter((tag) => tag.nameAddress === "alarmHum" + mysala)[0]._id,
                        // maxHums: tags.filter((tag) => tag.nameAddress === "maxHum" + mysala),
                        // maxHum: tags.filter((tag) => tag.nameAddress === "maxHum" + mysala)[0].lastValue,
                        // minHums: tags.filter((tag) => tag.nameAddress === "minHum" + mysala),
                        // minHum: tags.filter((tag) => tag.nameAddress === "minHum" + mysala)[0].lastValue,


                        // timeOnHums: tags.filter((tag) => tag.nameAddress === "timeOn" + mysala),
                        // timeOnHum: tags.filter((tag) => tag.nameAddress === "timeOn" + mysala)[0].lastValue,
                        // timeOffHums: tags.filter((tag) => tag.nameAddress === "timeOff" + mysala),
                        // timeOffHum: tags.filter((tag) => tag.nameAddress === "timeOff" + mysala)[0].lastValue,
                        modal1: !this.state.modal1,
                        misTag: tags
                    });
                })
                .catch(error => {
                console.log(error);
                });
            }else{
                this.setState({
                    modal1: !this.state.modal1
                });

            }
    }

    almacenarConfig = () => {
        this.setState({blocking1: true,intentosGuardado:0});
       // console.log(sala)
       // console.log(this.state.setpointTemp + " - " + this.state.histeresisTemp + " - " +this.state.offsetTemp + " - " + this.state.maxTemp + " - " + this.state.minTemp + " - " + this.state.activoTemp);
       // console.log(this.state.setpointHum + " - " + this.state.histeresisHum + " - " +this.state.offsetHum + " - " + this.state.maxHum + " - " + this.state.minHum + " - " + this.state.activoHum);

       //PARA TEMPERATURA
       const hostname = window && window.location && window.location.hostname;
       if(hostname === 'ihc.idealcontrol.cl' || hostname === '169.53.138.44') {
            toast['warning']('En el sistema Online no se guardaran los datos de las Alarmas', { autoClose: 4000 })
            toast['success']('Almacenado Correctamente', { autoClose: 4000 })
            this.loadDataChar()
            this.setState({blocking1: false});
       }else{
            this.intervalConfirmacion = setInterval(() => this.confirmacionTermino(),3000);
       }
       const token = "tokenfalso";
       const config = { headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token} };
       let EndPointTag="";


       this.state.misTag.filter((data)=>data.nameAddress.includes(this.state.sala) && data.register==true)
       .map((data)=>{
            EndPointTag = `${API_ROOT}/tag/${data._id}`;
            const content1 = {
                "max":this.state.scaleMax,
                "min":this.state.scaleMin,
                // "alertMax": this.state.maxAlarm,
                // "alertMin":this.state.minAlarm,
                "write":true
            }
            axios.put(EndPointTag, content1, config)
            .then(response => {
            // console.log("actualizado " + response.status);
            });
            return data;
       })
        

        EndPointTag = `${API_ROOT}/tag/${this.state.maxAlarms[0]._id}`;
        const content2 = {
            "lastValue": this.state.maxAlarm,
            "lastValueWrite":this.state.maxAlarm,
            "write":true
        }
        axios.put(EndPointTag, content2, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });


        EndPointTag = `${API_ROOT}/tag/${this.state.minAlarms[0]._id}`;
        const content3 = {
            "lastValue": this.state.minAlarm,
            "lastValueWrite":this.state.minAlarm,
            "write":true
        }
        axios.put(EndPointTag, content3, config)
        .then(response => {
           // console.log("actualizado " + response.status);
        });
       

    }

    inputChangeHandler = (event) => {
        // console.log(event.target.value);
        this.setState( {
            ...this.state,
            [event.target.id]: event.target.value
        } );
    }

    generateCard=(activeModal,ChartData,tagId,settings,modal,scalaMin,scalaMax)=>{
        // console.log(this.state.Tags)
        // let alarmaMin=this.state.Tags.filter(t=>t.nameAddress==="minAlarmS"+modal)
        // let alarmaMax=this.state.Tags.filter(t=>t.nameAddress==="maxAlarmS"+modal)
        // console.log(alarmaMax);
        // if(alarmaMin.length>0 && alarmaMax.length>0){
        //     console.log(alarmaMax[0].lastValue)
        //     alarmaMin=alarmaMin[0].lastValue
        //     alarmaMax=alarmaMax[0].lastValue
        // }
        return (
            <Col sm="6" md="6" lg="6" style={{marginTop:9}}>
                <Card>
                    {
                    ChartData
                    .filter((data,i)=>data._id==tagId)
                    .map((data, i)=>{
                        return(
                            <Fragment>
                                <CardHeader className="card-header-tab  ">
                                <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                    {/* <i className="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i> */}
                                    {data.name}
                                </div>
                                <div className="btn-actions-pane-right text-capitalize">
                                    <span className="d-inline-block">
                                            {activeModal&&(<Button color="primary" onClick={() => {
                                                this.setState({titleModal:data.name})
                                                this.toggleModal1(modal,data._id)
                                            }}
                                                outline
                                                className={"btn-shadow btn-wide btn-outline-2x btn-block "}>
                                            <i className="pe-7s-tools btn-icon-wrapper"> </i>
                
                                            </Button>)}
                
                                    </span>
                                </div>
                                </CardHeader>
                                <Card className="main-card mb-0">
                                    <div className="grid-menu grid-menu-4col">
                                        <Row className="no-gutters">
                                            <Col sm="12"  key={data._id}  className={cx(data.active ? '' : 'opacity-3')}>
                                                <div className="widget-chart widget-chart-hover  p-0 p-0 ">
                                                    <Button key={data._id}
                                                    //onClick={ e => this.slider2.slickGoTo(i)}
                                                    className="btn-icon-vertical btn-square btn-transition p-3" outline color={miscolores[i]}>
                                                        {/* <span  className={cx("badge badge-dot badge-dot-lg badge-dot-inside", data.state ? 'badge-success' : 'badge-danger')}>> </span>   */}
                                                        <div className="size-boton mt-0 size_boton" style={{color:miscolores[i]}}>
                                                        {data.measurements[data.measurements.length-1].value}
                                                            <span className="opacity-6  pl-0 size_unidad">  {data.unity} </span>
                                                        </div>
                                                        <div className="widget-subheading">
                                                        {data.shortName}
                                                        </div>
                                                    </Button>
                                                </div>
                
                                            </Col>
                                        </Row>
                                    </div>
                                </Card>
                                <CardBody className="p-0">
                
                                    <div className="p-1 slick-slider-sm mx-auto">
                                    <Slider  ref={slider1 => (this.slider1 = slider1)} {...settings}>
                                        <div>
                                            <div className="widget-chart widget-chart2 text-left p-0">
                                                <div className="widget-chat-wrapper-outer">
                                                    <div className="widget-chart-content widget-chart-content-lg p-2">
                                                        <div className="widget-chart-flex">
                                                            <div
                                                                className="widget-title opacity-9 text-muted text-uppercase">
                                                                {data.name}
                                                            </div>
                                                            <div className="btn-actions-pane-right text-capitalize pr-2" >
                                                                    <div className={cx("divfloatleft mr-2",this.state.bitVenS2===0 ? 'opacity-1' : ' opacity-8 ',i===0 ? ' oculto ' : '')}>
                                                                        {/* <img width={23} src={ventilador} alt="" /> */}
                
                                                                    </div>
                                                                    <div className={cx("divfloatleft mr-2", this.state.bitValS2===0  ? 'opacity-1' : 'opacity-8' ,i===1 ? ' oculto ' : '')}>
                                                                        {/* <img width={23} src={valvula} alt="" /> */}
                
                                                                    </div>
                                                            </div>
                
                                                        </div>
                                                        {/* <div className="widget-numbers p-1 m-0">
                                                            <div className="widget-chart-flex">
                                                                <div>
                                                                    {data.lastValue}
                                                                    <small className="opacity-5 pl-1 size_unidad3">  {data.unity}</small>
                                                                </div>
                
                
                
                                                            </div>
                                                        </div> */}
                                                        <div className=" opacity-8 text-focus pt-0">
                                                        <div className=" opacity-5 d-inline ">
                                                            Max
                                                            </div>
                                                            <div className="d-inline  pr-1" style={{color:miscolores[i]}}>
                
                                                                <span className="pl-1 size_prom">
                                                                    {
                                                                    data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value)
                                                                    }
                
                                                                </span>
                                                            </div>
                                                            <div className=" opacity-5 d-inline ml-2">
                                                            Prom
                                                            </div>
                                                            <div className="d-inline  pr-1" style={{color:miscolores[i]}}>
                                                                <span className="pl-1 size_prom">
                                                                    {
                                                                    ( (data.measurements.reduce((a, b) => +a + +b.value, 0)/data.measurements.length)).toFixed(1)
                                                                    }
                                                                </span>
                                                            </div>
                                                            <div className=" opacity-5 d-inline ml-2">
                                                            Min
                                                            </div>
                                                            <div className="d-inline  pr-1" style={{color:miscolores[i]}}>
                                                                <span className="pl-1 size_prom">
                                                                {
                                                                data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value)
                                                                }
                                                                </span>
                                                            </div>
                
                                                        </div>
                                                    </div>
                
                                                    <div
                                                        className="widget-chart-wrapper he-auto opacity-10 m-0">
                                                        <ResponsiveContainer height={250} width='100%'>
                
                                                            <AreaChart data={data.measurements}
                                                                    margin={{
                                                                        top: 0,
                                                                        right: 0,
                                                                        left: -30,
                                                                        bottom: 0
                                                                    }}>
                                                                        <Tooltip
                                                                                labelFormatter={function(value) {
                                                                                    return `${ moment(value.substr(0,19)).format('HH:mm DD-MMM')}`;
                                                                                    }}
                                                                                formatter={function(value, name) {
                                                                                return `${value}`;
                                                                                }}
                                                                            />
                                                                    <defs>
                                                                        <linearGradient id={"colorPv" + i} x1="0" y1="0" x2="0" y2="1">
                                                                            <stop offset="10%" stopColor={miscolores[i]} stopOpacity={0.7}/>
                                                                            <stop offset="90%" stopColor={miscolores[i]}stopOpacity={0}/>
                                                                        </linearGradient>
                                                                    </defs>
                                                                    <YAxis
                                                                    tick={{fontSize: '10px'}}
                                                                    domain={[
                                                                            data.min!=null?data.min:
                                                                            Math.round(
                                                                            (data.measurements.reduce((min, b) => Math.min(min, b.value), data.measurements[0].value) - 1) * 100) / 100,
                                                                            data.max!=null?data.max:
                                                                            data.measurements.reduce((max, b) => Math.max(max, b.value), data.measurements[0].value) + 1]}
                                                                    />
                
                                                                        <ReferenceLine y={
                                                                            this.state.Tags.filter(t=>t.nameAddress==="maxAlarmS"+modal).length>0?
                                                                            this.state.Tags.filter(t=>t.nameAddress==="maxAlarmS"+modal)[0].lastValue:
                                                                            data.alertMax
                                                                        } label={{ position: 'top',  value: 'Max', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                        <ReferenceLine y={
                                                                            this.state.Tags.filter(t=>t.nameAddress==="minAlarmS"+modal).length>0?
                                                                            this.state.Tags.filter(t=>t.nameAddress==="minAlarmS"+modal)[0].lastValue:
                                                                            data.alertMin
                                                                        }  label={{ position: 'top',  value: 'Min', fill: 'red', fontSize: 9 }} stroke="red" strokeDasharray="3 4 5 2" strokeWidth={0.4} />
                                                                    <XAxis
                                                                            dataKey={'dateTime'}
                                                                            hide = {false}
                                                                            tickFormatter={dateTime => moment(dateTime.substr(0,19)).format('HH:mm')}
                                                                            tick={{fontSize: '10px'}}
                                                                            />
                                                                    <Area type='monotoneX' dataKey='value'
                                                                        stroke={miscolores[i]}
                                                                        strokeWidth='3'
                                                                        fillOpacity={1}
                                                                        fill={"url(#colorPv" + i + ")"}/>
                                                                </AreaChart>
                
                                                        </ResponsiveContainer>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                
                                    </Slider>
                                    </div>
                
                                </CardBody>
                            </Fragment>
                            )
                        })
                        
                    }
                </Card>
            </Col>
        )
    }


    render() {
        // let { rolUsuario } = this.props;
        // if(rolUsuario=="5dfa79df69e895a6c90005b2"){
        //     habilitado=true;
        // }
         //const styleValvula = this.state.bitValS1===1 ? {display:'none'}:{};
         const { myDataChart1,myDataChart2, myDataChart3,myDataChart4,myDataChart5,myDataChart6,isLoading, error} = this.state;
          const settings = {
            // autoplaySpeed:6000,
            // autoplay: false,
            // centerMode: false,
            infinite: true,
            slidesToShow: 1,
            // slidesToScroll: 1,
            // speed: 600,
            arrows: false,
            // dots: false
        };
        
        if (error) {
            return <p>{error.message}</p>;
        }

        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }
        return (
            <Fragment>

                            <div className="app-page-title">
                                    <div className="page-title-wrapper">
                                        <div className="page-title-heading">
                                            <div>
                                            Ultima Actualización {moment(this.state.date_actualizado).format('DD-MM-YYYY HH:mm:ss')}
                                            </div>
                                        </div>
                                        <div className="page-title-actions">
                                        </div>
                                    </div>
                                </div>

                                <Modal isOpen={this.state.modal1} toggle={() => this.toggleModal1("-1")} className={this.props.className}>
                                    <BlockUi tag="div" blocking={this.state.blocking1} loader={<Loader active type={"ball-triangle-path"}/>}>
                                        <ModalHeader toggle={() => this.toggleModal1("-1")}>Configuracion {this.state.titleModal}</ModalHeader>
                                        <ModalBody>
                                        <Row>
                                            <Col  xs="0" md="2" lg="2">
                                            </Col>
                                            <Col xs="6" md="8" l g="8">
                                                <Card className="main-card mb-3">
                                                    <CardBody>
                                                        <CardTitle>Control Alarma</CardTitle>
                                                        <Form>
                                                            
                                                            <FormGroup className="mt-2">
                                                                <Label for="maxTemp" className="m-0">Max. Alarm</Label>
                                                                <InputGroup>
                                                                    <Input   id="maxAlarm" defaultValue={this.state.maxAlarm}   onChange={e => this.inputChangeHandler(e)}/>
                                                                    <InputGroupAddon addonType="append">
                                                                        <InputGroupText>{this.state.unity}</InputGroupText>
                                                                    </InputGroupAddon>
                                                                </InputGroup>
                                                            </FormGroup>
                                                            <FormGroup className="mt-2">
                                                                <Label for="minTemp" className="m-0">Min. Alarm</Label>
                                                                <InputGroup>
                                                                    <Input   id="minAlarm" defaultValue={this.state.minAlarm}   onChange={e => this.inputChangeHandler(e)}/>
                                                                    <InputGroupAddon addonType="append">
                                                                    <InputGroupText>{this.state.unity}</InputGroupText>
                                                                    </InputGroupAddon>
                                                                </InputGroup>
                                                            </FormGroup>

                                                            <hr></hr>
                                                            <CardTitle>Escala Grafico</CardTitle>

                                                            <FormGroup className="mt-2">
                                                                <Label for="maxTemp" className="m-0">Max. Escala</Label>
                                                                <InputGroup>
                                                                    <Input type="number"  id="scaleMax" defaultValue={this.state.scaleMax}   onChange={e => this.inputChangeHandler(e)}/>
                                                                    <InputGroupAddon addonType="append">
                                                                        <InputGroupText>{this.state.unity}</InputGroupText>
                                                                    </InputGroupAddon>
                                                                </InputGroup>
                                                            </FormGroup>
                                                            <FormGroup className="mt-2">
                                                                <Label for="minTemp" className="m-0">Min. Escala</Label>
                                                                <InputGroup>
                                                                    <Input type="number"  id="scaleMin" defaultValue={this.state.scaleMin}   onChange={e => this.inputChangeHandler(e)}/>
                                                                    <InputGroupAddon addonType="append">
                                                                    <InputGroupText>{this.state.unity}</InputGroupText>
                                                                    </InputGroupAddon>
                                                                </InputGroup>
                                                            </FormGroup>
                                                        </Form>
                                                    </CardBody>
                                                </Card>
                                            </Col>
                                        </Row>
                                        </ModalBody>
                                        <ModalFooter>
                                            <Button color="link" onClick={() => this.toggleModal1("-1")}>Cancel</Button>
                                            <Button color="primary"  onClick={() => this.almacenarConfig()}>Guardar</Button>{' '}
                                        </ModalFooter>
                                    </BlockUi>
                                </Modal>
                       <Row>
                            {
                                this.generateCard(true,myDataChart1,tag1,settings,"1",0,600)
                            }
                            
                            {
                                this.generateCard(true,myDataChart1,tag2,settings,"2",0,300)
                            }

                            {
                                this.generateCard(true,myDataChart1,tag5,settings,"5")
                            }

                            {
                                this.generateCard(true,myDataChart1,tag6,settings,"6")
                            }

                            {
                                this.generateCard(true,myDataChart1,tag8,settings,"8",null,120)
                            }

                            {
                                this.generateCard(true,myDataChart1,tag9,settings,"10")
                            }

                            {
                                this.generateCard(true,myDataChart1,tag10,settings,"11",0,2200)
                            }

                            {/* {
                                this.generateCard(myDataChart1,tag7,settings,"7")
                            } */}
                        </Row>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    rolUsuario: state.Session.rolUsuario,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(IndexZonaRILES);