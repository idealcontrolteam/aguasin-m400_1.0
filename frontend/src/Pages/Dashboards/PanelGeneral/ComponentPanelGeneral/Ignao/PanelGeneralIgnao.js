import React, {Component, Fragment} from 'react';

 import cx from 'classnames';
import {Row, Col,Button,CardHeader,Card,CardBody} from 'reactstrap';
import Loader from 'react-loaders';
import axios from 'axios';

import { API_ROOT} from '../../../../../api-config';
const APItag1 = `${API_ROOT}/tag`;


class PanelGeneralIgnao extends Component {  
    constructor() {
        super();
        this.state = {         
            myDataTag1: [],           
            error: null         
        }
        this.loadDataTag1 = this.loadDataTag1.bind(this);

        

    }
    

    componentDidMount = () => {
        this.setState({ isLoading: true });    
        this.loadDataTag1(); 
        const intervaloRefresco = 10000;
         this.intervalIdTag1 = setInterval(() => this.loadDataTag1,intervaloRefresco);

      }
  
    componentWillUnmount = () => { 
          clearInterval(this.intervalIdTag1);  
    }

    
    loadDataTag1 = () =>  {  
       //const token = localStorage.getItem("token"); 
       const token = "tokenFalso";         
       
        axios
        .get(APItag1, {
          headers: {  
            'Authorization': 'Bearer ' + token
          }
        })
        .then(response => {
            const myDataTag1 = response.data.data;
            this.setState({ myDataTag1,isLoading: false });   
        })
        .catch(error => {
          console.log(error);
        });

    }     
  
    render() { 
    
        const { myDataTag1,isLoading, error} = this.state;  
        if (error) {
            return <p>{error.message}</p>;
        }  
        if (isLoading) {
            return <Loader type="ball-pulse"/>;
        }   



    return (
        <Fragment>
                  
             

                            <div className="app-page-title">
                                <div className="page-title-wrapper">
                                    <div className="page-title-heading">
                                        <div>
                                        Panel General
                                        </div>
                                    </div>
                                    <div className="page-title-actions">
                                        Alarmado
                                        <span  className="badge badge-dot badge-dot-lg badge-danger  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span>  
                                         Normal
                                        <span  className="badge badge-dot badge-dot-lg badge-success  mb-1 mr-2 pt-0 pr-0 pl-0 pb-0  m-10 "> </span> 
                                     </div>                    
                                </div>
                             </div>
                            
                                <Row>
                   
                                    <Col md="6" lg="6"> 
                                        <Card className="mb-3 ">
                                            <CardHeader className="card-header-tab">
                                                <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                    Alevines                                     
                                                </div>                                                                     
                                            </CardHeader>
                                            <CardBody className="p-0 opacity-1">                         
                                                <Row>  
                                                
                                                    <Col xs={2} md={2} lg={2} className="m-0 p-0">
                                                   
                                                        {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 
                                                                        </Col>
                                                        <Col xs={2} md={2} lg={2} className="m-0 p-0">     
                                                        {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         }                                                   
                                                    </Col>
                                                    <Col  xs={2} md={2} lg={2} className="m-0 p-0">
                                                   
                                                    {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 

                                                        <div  className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                       
                                                    
                                                       <Button   className="mt-1 mr-0 mb-10 mr-0   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                               <div className="  pl-1 size_label">  oxs.  TK100</div> 
                                                           <span className="badge badge-light m-0 ml-0 pl-1 w-100">  0  <span className="opacity-6  pl-0 size_unidad2"> % </span> </span>
                                                
                                                           <span  className={cx("badge badge-dot badge-dot-lg badge-success")}>> </span> 
                                                       </Button>
                                                       </div>   
                                                                       </Col>
                                                       <Col  xs={2} md={2} lg={2}  className="m-0 p-0">     
                                                       {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 

                                                         
                                                        <div  className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                       
                                                    
                                                             <Button   className="mt-1 mr-0 mb-1 mr-0   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                     <div className="  pl-1 size_label">  oxs.  TK100</div> 
                                                                 <span className="badge badge-light m-0 ml-0 pl-1 w-100">  0  <span className="opacity-6  pl-0 size_unidad2"> % </span> </span>
                                                      
                                                                 <span  className={cx("badge badge-dot badge-dot-lg badge-success")}>> </span> 
                                                             </Button>
                                                             </div>                                                   
                                                   </Col>
                                                   <Col  xs={2} md={2} lg={2}  className="m-0 p-0">
                                                   
                                                   {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 
                                                                       </Col>
                                                       <Col xs={2} md={2} lg={2}  className="m-0 p-0">     
                                                       {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         }                                                  
                                                   </Col>
                                            
                                            
                                                </Row>
                                            </CardBody>
                                        </Card> 
                                    </Col>
                                    <Col   md="6" lg="6">               
                                            <Card className="main-card mb-3">
                                                <CardHeader className="card-header-tab">
                                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                        Smolt                            
                                                    </div>
                                                </CardHeader>
                                                <CardBody className="p-0 opacity-1">                             
                                                    <Row className="no-gutters">
                                                    <Col > 
                                                    <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" > TK A  </div>                                      
                                                        <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                           
                                                        {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 
                                                            </div>                        
                                                    </Col>
                                                    
                                                <Col > 
                                                    <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" > TK A  </div>                                      
                                                        <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                           
                                                        {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 
                                                            </div>                        
                                                    </Col>
                                                    <Col > 
                                                    <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" > TK A  </div>                                      
                                                        <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                           
                                                        {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 
                                                            </div>                        
                                                    </Col>
                                                    <Col> 
                                                    <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" > TK A  </div>                                      
                                                        <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                           
                                                        {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 
                                                            </div>                        
                                                    </Col>
                                                    </Row>
                                                
                                                </CardBody>
                                            </Card> 
                                            <Card className="main-card mb-1">
                                            
                                            <CardHeader className="card-header-tab">
                                                    <div className="card-header-title font-size-lg text-capitalize font-weight-normal">
                                                    Broodstock                            
                                                    </div>
                                                </CardHeader>
                                            <CardBody className="p-0"> 
                                                <Row className="no-gutters">
                                                    <Col > 
                                                    <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" > TK A  </div>                                      
                                                        <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                           
                                                        {
                                                             
                                                             myDataTag1
                                                                 .filter((data) => {
                                                                     return data.locationId === '5ccb142f459dc45190294ae8';
                                                                 }).map(data=>
                                                                     <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                             <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                         <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                              
                                                                         <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                     </Button>
                                                                 
                                                                 )
                                                                 
                                                         } 
                                                            </div>                        
                                                    </Col>
                                                    <Col > 
                                                        <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" > TK B  </div>     
                                                                                        
                                                        <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                            
                                                        {
                                                             
                                                                myDataTag1
                                                                    .filter((data) => {
                                                                        return data.locationId === '5ccb142f459dc45190294ae8';
                                                                    }).map(data=>
                                                                        <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                                <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                            <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                                 
                                                                            <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                        </Button>
                                                                    
                                                                    )
                                                                    
                                                            } 
                                                        </div> 
                                                        </Col>
                                                    <Col >  
                                                          <div className="mt-0 mr-0 mb-0 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" > TK B  </div>                          
                                                            <div className="widget-description opacity-10 text-focus  card-btm-border2 card-shadow-primary border-primary">
                                                          
                                                            {
                                                           
                                                                myDataTag1
                                                                    .filter((data) => {
                                                                        return data.locationId === '5ccb142f459dc45190294ae8';
                                                                    }).map(data=>
                                                                        <Button  key={data._id} className="mt-1 mr-0 mb-1 ml-1   pt-0 pr-0 pb-0 pl-0 ancho_btn" color="primary">
                                                                                <div className="  pl-1 size_label">   {data.shortName}.</div> 
                                                                            <span className="badge badge-light m-0 ml-0 pl-1 w-100">   {data.lastValue}  <span className="opacity-6  pl-0 size_unidad2">  {data.unity} </span> </span>
                                                                 
                                                                            <span  className={cx("badge badge-dot badge-dot-lg ", data.state ? 'badge-success' : 'badge-danger')}>> </span> 
                                                                        </Button>
                                                                    
                                                                    )
                                                                    
                                                            } 
                                                        </div> 
                                                    </Col>
                                                </Row>
                                            </CardBody>
                                            </Card>                 
                                    </Col>
                                </Row>
              
          
      </Fragment>
    )
  }

}

export default PanelGeneralIgnao;




