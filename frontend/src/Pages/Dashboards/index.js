import React, {Fragment} from 'react';
import {Route} from 'react-router-dom';

// DASHBOARDS
// import PanelGeneral from './PanelGeneral/IndexPanelGeneral';
import IndexZonaESED from './Zonas/IndexZonaESED';
import IndexZonaRILES from './Zonas/IndexZonaRILES';
import IndexZonaPublica from './Zonas/IndexZonaPublica';
import Tendencia from './Tendencia/IndexTendencia';
// import HistAlarmas from './HistAlarmas/IndexHisAlarmas';
// import HistFallas from './HistFallas/IndexHisFallas';

// Layout
import AppHeader from '../../Layout/AppHeader';
import AppSidebar from '../../Layout/AppSidebar';



// Theme Options
import ThemeOptions from '../../Layout/ThemeOptions';


const Dashboards = ({match}) => (
    <Fragment>
        <ThemeOptions/>
        <AppHeader/>
        <div className="app-main">
            <AppSidebar/>
            <div className="app-main__outer">
                <div className="app-main__inner">
                    {/* <Route path={`${match.url}/panelgeneral`} component={PanelGeneral}/> */}
                    <Route path={`${match.url}/zona_esed`} component={IndexZonaESED}/>
                    <Route path={`${match.url}/zona_riles`} component={IndexZonaRILES}/>
                    <Route path={`${match.url}/public_riles`} component={IndexZonaPublica}/>
                    <Route path={`${match.url}/tendencia`} component={Tendencia}/>
                    {/* <Route path={`${match.url}/histalarmas`} component={HistAlarmas}/>
                    <Route path={`${match.url}/histfallas`} component={HistFallas}/> */}
                </div>           
            </div>
        </div>
    </Fragment>
);

export default Dashboards;