

let backendHost;
const hostname = window && window.location && window.location.hostname;
 

if(hostname === 'ihc.idealcontrol.cl') {
  backendHost = 'https://ihc.idealcontrol.cl/aguasin-m400/apiv1/';
} else {
    backendHost =  `http://${hostname}:3006`;
}


export const API_ROOT = `${backendHost}`;

