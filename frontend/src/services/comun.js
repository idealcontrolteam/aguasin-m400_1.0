import axios from 'axios';
import { API_ROOT} from '../api-config';

const APItag1 = `${API_ROOT}/tag/filteroregister/true`;
//console.log(APItag1);
export class TagServices {    

    getDataTag() { 
        const token = "tokenFalso"; 
        return axios.get(APItag1, {
           headers: {  
             'Authorization': 'Bearer ' + token
           }
         })
         .then(response =>  response.data.data)
   
     }
     
     getTags(){
      let EndPoint=`${API_ROOT}/tag`;
      return axios.get(EndPoint)
      .then(response=>response.data.data)
      .catch((e)=>e)
    }
}