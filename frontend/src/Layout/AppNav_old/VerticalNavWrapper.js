import React, {Component, Fragment} from 'react';
import {withRouter} from 'react-router-dom';
import MetisMenu from 'react-metismenu';
import { Menu } from './NavItems';
//import {Menu,Salas } from './NavItems';
import {connect} from 'react-redux';
import logo from '../../assets/img/camanchaca2.jpg';
import Media from 'react-media';


class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        
      
    }

    render() {
        // let { rolUsuario } = this.props;
        // const token = localStorage.getItem("token");
        // let Menu1 =[]; 
        // if(token){
        //     Menu1.push(
        //         {
        //             icon: 'pe-7s-display1',
        //             label: 'RILES',
        //             to: '#/dashboards/zona_riles',
        //         }
        //     );
        // }else{
        //     Menu1.push(
        //         {
        //             icon: 'pe-7s-display1',
        //             label: 'RILES',
        //             to: '#/dashboards/public_riles',
        //         }
        //     );
        // }
        
        // if(rolUsuario=="5e595ba569e89513d50056a7"){
        //     Menu1.push({
        //         icon: 'pe-7s-display1',
        //         label: 'ESED 1',
        //         to: '#/dashboards/zona_esed',
        //     });
        //     Menu1.push({
        //         icon: 'pe-7s-display1',
        //         label: 'Tendencia Historica',
        //         to: '#/dashboards/tendencia',
        //     })
        // }
        

        return (
            <Fragment>
                <div className="app-sidebar__heading header-btn-lg">
        
                        <div className=" ml-3 ">
                                <div className="">
                                    PISCICULTURA
                                </div>
                                <div className="opacity-4">
                                    PETROHUE
                                </div>
                         </div>
                 
                </div>
                <h5 className="app-sidebar__heading">Menu</h5>
                <MetisMenu  content={Menu} activeLinkFromLocation className="vertical-nav-menu" iconNamePrefix=""  classNameStateIcon="pe-7s-angle-down"/>
                {/* <h5 className="app-sidebar__heading">Salas</h5>
                <MetisMenu  content={Salas} activeLinkFromLocation className="vertical-nav-menu" iconNamePrefix="" classNameStateIcon="pe-7s-angle-down"/> */}
                {/* <img className="animated" src={logo} width="230" style={{marginTop:530,borderRadius:4}} /> */}
                <Media query="(max-height: 725px)">
                    {matches =>
                        matches ? (
                            <img className="animated" src={logo} width="230" style={{marginTop:270,borderRadius:4}} />
                        ) : (
                            <img className="animated" src={logo} width="230" style={{marginTop:470,borderRadius:4}} />
                        )
                    }
                </Media>
            </Fragment>
        );
    }

    isPathActive(path) {
        return this.props.location.pathname.startsWith(path);
    }
}

const mapStateToProps = state => ({
    nameUsuario: state.Session.nameUsuario,
    emailUsuario: state.Session.emailUsuario,
    rolUsuario: state.Session.rolUsuario,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Nav));