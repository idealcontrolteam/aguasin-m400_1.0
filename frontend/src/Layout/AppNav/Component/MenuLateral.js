import React from 'react';
import {useState,useEffect} from 'react';

const MenuLateral = ({listas}) => {

    const [url,setUrl]=useState('')

    useEffect(()=>{
        const url=String(document.URL).split('/');
        // console.log(url[url.length-1]);
        setUrl(url[url.length-1])
    },[])

    return(
        <div>
            <div class="metismenu vertical-nav-menu">
                <ul class="metismenu-container">
                    {
                        listas.map(menu=>{
                            const html=menu.content?menu.content:[]
                            return(
                                <li class="metismenu-item">
                                    {
                                        menu.to?(
                                        <a className={`metismenu-link ${String(menu.to).includes(url)&&'active'}`} 
                                        onClick={((e)=>{setUrl(menu.to)})} href={`${menu.to}`}>
                                            <i class={`metismenu-icon ${menu.icon}`}></i>
                                            {menu.label}
                                            <i class="metismenu-state-icon caret-left rotate-minus-90"></i>
                                        </a>
                                        ):(
                                        <a class="metismenu-link" >
                                            <i class={`metismenu-icon ${menu.icon}`}></i>
                                            {menu.label}
                                            <i class="metismenu-state-icon caret-left rotate-minus-90"></i>
                                        </a>
                                        )
                                    }
                                    
                                    {
                                        html.map(contenido=>{
                                            return(
                                                <ul class="metismenu-container visible">
                                                    <li class="metismenu-item">
                                                        <a className={`metismenu-link ${String(contenido.to).includes(url)&&'active'}`} 
                                                        // <a class={sw?`metismenu-link active`:`metismenu-link`} 
                                                        onClick={((e)=>{setUrl(contenido.to)})} href={`${contenido.to}`}>
                                                            <i class={`metismenu-icon ${contenido.icon}`}></i>
                                                            {contenido.label}
                                                        </a>
                                                    </li>
                                                </ul>
                                            )
                                        })
                                    }
                                </li>
                            )
                        })
                    }
                </ul>
            </div> 
        </div>
    )
}

export default MenuLateral
