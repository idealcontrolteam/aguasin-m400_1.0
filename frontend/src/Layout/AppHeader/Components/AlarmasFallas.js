import React, {Fragment} from 'react';
import { Redirect} from 'react-router-dom';


import Ionicon from 'react-ionicons';

import {
    UncontrolledDropdown, DropdownToggle, DropdownMenu,

} from 'reactstrap';

//import city3 from '../../../assets/utils/images/dropdown-header/abstract1.jpg';
import city3 from '../../../assets/utils/images/dropdown-header/headerbg2.jpg';


import Tabs from 'react-responsive-tabs';
import Alarmas from './TabsContent/Alarmas';
import Fallas from './TabsContent/Fallas';

const tabsContent = [
    {
        title: 'Alarmas',
        content: <Alarmas/>
    },
    {
        title: 'Fallas',
        content: <Fallas/>
    }
];

function getTabs() {
    return tabsContent.map((tab, index) => ({
        title: tab.title,
        getContent: () => tab.content,
        key: index,
    }));
}



class AlarmasFallas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            redirect:false

        };
    }
    setRedirect = () => {
        this.setState({
          myredirect: true
        })
        
       
      }
  // href="#/dashboards/histalarmasfallas" onClick={this.setRedirect}

  
    render() {
      
     
        if (this.state.myredirect) {
             return <Redirect to="tendencia"></Redirect>
     
        } 
        return (
            
            <Fragment>
              
                <div className="header-dots">
             
                    <UncontrolledDropdown>
                    <DropdownToggle className="p-0 mr-2" color="link">
                            <div className="icon-wrapper icon-wrapper-alt rounded-circle">
                                <div className="icon-wrapper-bg bg-danger"/>
                                <Ionicon beat={true} color="#d92550" fontSize="23px" icon="md-notifications-outline"/>
                                <div className="badge badge-dot badge-dot-sm badge-danger">Notifications</div>
                            </div>
                        </DropdownToggle>
                        
                        <DropdownMenu right className="dropdown-menu-xl rm-pointers">
                            <div className="dropdown-menu-header mb-0">
                                <div className="dropdown-menu-header-inner ropdown-menu-header-inner bg-heavy-rain p-3  ">
                                    <div className="menu-header-image opacity-9"
                                        style={{
                                            backgroundImage: 'url(' + city3 + ')'
                                        }}
                                    />
                            
                                    <div className="menu-header-content text-dark">
                                        <h5 className="menu-header-title">Alarmas y Fallas Presentes</h5>
                                        <h6 className="menu-header-subtitle"> <b className="text-warning">5 Alarmas </b></h6>
                                        <h6 className="menu-header-subtitle"> <b className="text-danger">5 Fallas </b></h6>
                                    </div>
                                </div>
                            </div>
                            <Tabs tabsWrapperClass="body-tabs body-tabs-alt2" transform={false} showInkBar={true}
                                  items={getTabs()}/>
                           
                        
                        </DropdownMenu>
                    </UncontrolledDropdown>
             
                </div>
            </Fragment>
        )
    }
}

export default AlarmasFallas;
