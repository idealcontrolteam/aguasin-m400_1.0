import React, {Component, Fragment} from 'react';

import {VerticalTimeline, VerticalTimelineElement} from 'react-vertical-timeline-component';

import PerfectScrollbar from 'react-perfect-scrollbar';
import {
 
    Nav,  Button, NavItem
} from 'reactstrap';

class Fallas extends Component {

    render() {
        return (
            <Fragment>
                <div className="scroll-area-sm">
                    <PerfectScrollbar>
                        <div className="p-3">
                            <VerticalTimeline layout="1-column" className="vertical-without-time">
                      
                             
                            <VerticalTimelineElement
                                    className="vertical-timeline-item"
                                    icon={<i className="badge badge-dot badge-dot-xl badge-danger"> </i>}
                                >
                                     <p>
                                     TK A  Falla termica Bomba Cono <b className="text-danger">12:00 PM</b>
                                    </p>
                                   
                                </VerticalTimelineElement>

                                
                                <VerticalTimelineElement
                                    className="vertical-timeline-item"
                                    icon={<i className="badge badge-dot badge-dot-xl badge-danger"> </i>}
                                >
                                     <p>
                                     TK A  Falla termica Bomba Retorno 1 <b className="text-danger">12:00 PM</b>
                                    </p>
                                   
                                </VerticalTimelineElement>

                                <VerticalTimelineElement
                                    className="vertical-timeline-item"
                                    icon={<i className="badge badge-dot badge-dot-xl badge-danger"> </i>}
                                >
                                     <p>
                                     TK A  Falla termica Ventilador  3 <b className="text-danger">12:00 PM</b>
                                    </p>
                                   
                                </VerticalTimelineElement>
                                <VerticalTimelineElement
                                    className="vertical-timeline-item"
                                    icon={<i className="badge badge-dot badge-dot-xl badge-danger"> </i>}
                                >
                                     <p>
                                     TK A  Falla termica Ventilador  2 <b className="text-danger">12:00 PM</b>
                                    </p>
                                   
                                </VerticalTimelineElement>
                                <VerticalTimelineElement
                                    className="vertical-timeline-item"
                                    icon={<i className="badge badge-dot badge-dot-xl badge-danger"> </i>}
                                >
                                     <p>
                                     TK A  Falla termica Ventilador  1 <b className="text-danger">12:00 PM</b>
                                    </p>
                                   
                                </VerticalTimelineElement>
                               
                            </VerticalTimeline>
                            <Nav vertical>
                               <NavItem className="nav-item-divider"/>
                                <NavItem className="nav-item-btn text-center">
                                
                                    <Button color="primary"
                                     href="#/historialfallas"
                                                   outline
                                                   className={"btn-shadow btn-wide btn-outline-2x pb-2 mr-2 "}
                                   
                                                     
                                                >Ver Historial      
                                              
                                        </Button>
                                
                                </NavItem>
                             
                            </Nav>
                        
                        </div>
                    </PerfectScrollbar>
                </div>
            </Fragment>
        )
    }
}

export default Fallas;