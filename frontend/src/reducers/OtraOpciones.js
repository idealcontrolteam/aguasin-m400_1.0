// import {colourOptions} from '../../src/Pages/Dashboards/Tendencia/Componentes/Filtros/Examples/data';


export const SET_ENABLE_PAGETITLE_ICON = 'OTRAS_OPCIONES/SET_ENABLE_PAGETITLE_ICON';



export const setEnablePageTitleIcon = enablePageTitleIcon => ({
    type: SET_ENABLE_PAGETITLE_ICON,
    enablePageTitleIcon
});

export default function reducer(state = {  
    enablePageTitleIcon:[]
    
}, action) {
    // eslint-disable-next-line default-case
    switch (action.type) {  
        case SET_ENABLE_PAGETITLE_ICON:
            return {
                ...state,
                enablePageTitleIcon: action.enablePageTitleIcon
            };

    }
    return state;
}
