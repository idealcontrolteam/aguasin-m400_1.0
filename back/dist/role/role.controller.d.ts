import { CreateRoleDTO } from './dto/role.dto';
import { RoleService } from './role.service';
export declare class RoleController {
    private roleService;
    constructor(roleService: RoleService);
    createRole(res: any, body: CreateRoleDTO): Promise<any>;
    getRoles(res: any): Promise<any>;
    getUsersByRoleId(res: any, roleId: any): Promise<any>;
    getRole(res: any, roleId: any): Promise<any>;
    updateRole(res: any, body: CreateRoleDTO, roleId: any): Promise<any>;
    deleteRole(res: any, roleId: any): Promise<any>;
}
