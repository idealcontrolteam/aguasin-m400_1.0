"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const role_dto_1 = require("./dto/role.dto");
const role_service_1 = require("./role.service");
let RoleController = class RoleController {
    constructor(roleService) {
        this.roleService = roleService;
    }
    createRole(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newRole = yield this.roleService.createRole(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Role created successfully',
                data: newRole,
            });
        });
    }
    getRoles(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const roles = yield this.roleService.getRoles();
            let msg = roles.length == 0 ? 'Roles not found' : 'Roles fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: roles,
                count: roles.length,
            });
        });
    }
    getUsersByRoleId(res, roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.roleService.getUsersByRoleId(roleId);
            let msg = users.length == 0 ? 'Users not found' : 'Users fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: users,
                count: users.length,
            });
        });
    }
    getRole(res, roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!roleId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Id is not a valid  ObjectId');
            }
            const role = yield this.roleService.getRole(roleId);
            if (!role) {
                throw new common_1.NotFoundException('Role not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Role found',
                data: role,
            });
        });
    }
    updateRole(res, body, roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!roleId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Id is not a valid  ObjectId');
            }
            const updatedRole = yield this.roleService.updateRole(roleId, body);
            if (!updatedRole) {
                throw new common_1.NotFoundException('Role not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Role updated',
                data: updatedRole,
            });
        });
    }
    deleteRole(res, roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedRole = yield this.roleService.deleteRole(roleId);
            if (!deletedRole) {
                throw new common_1.NotFoundException('Role not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Role deleted',
                data: deletedRole,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, role_dto_1.CreateRoleDTO]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "createRole", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "getRoles", null);
__decorate([
    common_1.Get('/:roleId/user'),
    __param(0, common_1.Res()), __param(1, common_1.Param('roleId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "getUsersByRoleId", null);
__decorate([
    common_1.Get('/:roleId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('roleId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "getRole", null);
__decorate([
    common_1.Put('/:roleId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('roleId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, role_dto_1.CreateRoleDTO, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "updateRole", null);
__decorate([
    common_1.Delete('/:roleId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('roleId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], RoleController.prototype, "deleteRole", null);
RoleController = __decorate([
    common_1.Controller('role'),
    __metadata("design:paramtypes", [role_service_1.RoleService])
], RoleController);
exports.RoleController = RoleController;
//# sourceMappingURL=role.controller.js.map