import { Model } from 'mongoose';
import { CreateRoleDTO } from './dto/role.dto';
import { Role } from './interfaces/role.interface';
import { UserService } from '../user/user.service';
import { User } from '../user/interfaces/user.interface';
export declare class RoleService {
    private roleModel;
    private userService;
    constructor(roleModel: Model<Role>, userService: UserService);
    getRoles(): Promise<Role[]>;
    getUsersByRoleId(roleId: any): Promise<User[]>;
    getRole(id: any): Promise<Role>;
    createRole(createRoleDTO: CreateRoleDTO): Promise<Role>;
    deleteRole(id: any): Promise<Role>;
    updateRole(id: string, body: CreateRoleDTO): Promise<Role>;
}
