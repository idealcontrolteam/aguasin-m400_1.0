export declare class CreateRoleDTO {
    name: string;
    active: boolean;
}
