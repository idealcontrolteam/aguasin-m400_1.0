"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.BrandSchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
}, { versionKey: false });
//# sourceMappingURL=brand.schema.js.map