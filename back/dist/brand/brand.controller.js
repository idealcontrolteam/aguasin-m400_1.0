"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const brand_dto_1 = require("./dto/brand.dto");
const brand_service_1 = require("./brand.service");
let BrandController = class BrandController {
    constructor(brandService) {
        this.brandService = brandService;
    }
    createBrand(res, createBrandDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const brand = yield this.brandService.createBrand(createBrandDTO);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Brand created successfully',
                data: brand,
            });
        });
    }
    getBrands(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const brands = yield this.brandService.getBrands();
            console.log("Aqui");
            let msg = brands.length == 0 ? 'Brands not found' : 'Brands fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: brands,
                count: brands.length,
            });
        });
    }
    getBrand(res, brandid) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('[JUST BY BRAND ID]');
            if (!brandid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Brand id is not a valid ObjectId');
            }
            const brand = yield this.brandService.getBrand(brandid);
            if (!brand) {
                throw new common_1.NotFoundException('Brand not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Brand found',
                data: brand,
            });
        });
    }
    getModelsByBrandId(res, brandid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!brandid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Brand id is not a valid ObjectId');
            }
            const models = yield this.brandService.getModelsByBrandId(brandid);
            let msg = models.length == 0 ? 'Models not found' : 'Models fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: models,
                count: models.length,
            });
        });
    }
    updateBrand(res, body, brandid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!brandid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Brand id is not a valid ObjectId');
            }
            const updatedBrand = yield this.brandService.updateBrand(brandid, body);
            if (!updatedBrand) {
                throw new common_1.NotFoundException('Brand not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Brand updated successfully',
                data: updatedBrand,
            });
        });
    }
    deleteBrand(res, brandid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!brandid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Brand id is not a valid ObjectId');
            }
            const brand = yield this.brandService.deleteBrand(brandid);
            if (!brand) {
                throw new common_1.NotFoundException('Brand not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Brand Deleted',
                data: brand,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, brand_dto_1.CreateBrandDTO]),
    __metadata("design:returntype", Promise)
], BrandController.prototype, "createBrand", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], BrandController.prototype, "getBrands", null);
__decorate([
    common_1.Get('/:brandid'),
    __param(0, common_1.Res()), __param(1, common_1.Param('brandid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], BrandController.prototype, "getBrand", null);
__decorate([
    common_1.Get('/:brandid/model'),
    __param(0, common_1.Res()), __param(1, common_1.Param('brandid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], BrandController.prototype, "getModelsByBrandId", null);
__decorate([
    common_1.Put('/:brandid'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('brandid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, brand_dto_1.CreateBrandDTO, Object]),
    __metadata("design:returntype", Promise)
], BrandController.prototype, "updateBrand", null);
__decorate([
    common_1.Delete('/:brandid'),
    __param(0, common_1.Res()), __param(1, common_1.Param('brandid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], BrandController.prototype, "deleteBrand", null);
BrandController = __decorate([
    common_1.Controller('brand'),
    __metadata("design:paramtypes", [brand_service_1.BrandService])
], BrandController);
exports.BrandController = BrandController;
//# sourceMappingURL=brand.controller.js.map