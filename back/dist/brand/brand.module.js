"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const brand_controller_1 = require("./brand.controller");
const brand_service_1 = require("./brand.service");
const mongoose_1 = require("@nestjs/mongoose");
const brand_schema_1 = require("./schemas/brand.schema");
const model_module_1 = require("../model/model.module");
const model_service_1 = require("../model/model.service");
let BrandModule = class BrandModule {
};
BrandModule = __decorate([
    common_1.Module({
        imports: [
            model_module_1.ModelModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'Brand', schema: brand_schema_1.BrandSchema, collection: 'brand' },
            ]),
        ],
        controllers: [brand_controller_1.BrandController],
        providers: [brand_service_1.BrandService, model_service_1.ModelService],
        exports: [brand_service_1.BrandService],
    })
], BrandModule);
exports.BrandModule = BrandModule;
//# sourceMappingURL=brand.module.js.map