import { Document } from 'mongoose';
export interface Model extends Document {
    name: string;
    active: boolean;
    brandId: string;
}
