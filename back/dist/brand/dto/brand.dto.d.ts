export declare class CreateBrandDTO {
    name: string;
    active: boolean;
}
