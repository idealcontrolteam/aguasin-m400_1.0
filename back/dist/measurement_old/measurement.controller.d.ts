import { CreateMeasurementDTO } from './dto/measurement.dto';
import { MeasurementService } from './measurement.service';
export declare class MeasurementController {
    private measurementService;
    constructor(measurementService: MeasurementService);
    validateIds: (body: any) => void;
    createMeasurementMultiple(res: any, body: any): Promise<any>;
    createMeasurement(res: any, body: CreateMeasurementDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByTagfilteredXY(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateMeasurementDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
