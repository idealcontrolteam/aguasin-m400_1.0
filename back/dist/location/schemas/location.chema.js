"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.LocationSchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
    zoneId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Zone',
        required: true,
    },
}, { versionKey: false });
//# sourceMappingURL=location.chema.js.map