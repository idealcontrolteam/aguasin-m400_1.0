export declare class CreateLcationDTO {
    name: string;
    active: boolean;
    zoneId: string;
}
