import { CreateLcationDTO } from './dto/location.dto';
import { LocationService } from './location.service';
export declare class LocationController {
    private locationService;
    constructor(locationService: LocationService);
    createLocation(res: any, body: CreateLcationDTO): Promise<any>;
    getLocations(res: any): Promise<any>;
    getLocationsAll(res: any): Promise<any>;
    getMesurementsByLocationId(res: any, locationId: any): Promise<any>;
    getSensorsByLocationId(res: any, locationId: any): Promise<any>;
    getTagsByLocationId(res: any, locationId: any): Promise<any>;
    getLocation(res: any, locationId: any): Promise<any>;
    updateGateway(res: any, body: CreateLcationDTO, locationId: any): Promise<any>;
    deleteLocation(res: any, locationId: any): Promise<any>;
}
