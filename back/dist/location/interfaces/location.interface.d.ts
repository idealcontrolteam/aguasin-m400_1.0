import { Document } from 'mongoose';
export interface Location extends Document {
    name: string;
    active: boolean;
    zoneId: string;
}
