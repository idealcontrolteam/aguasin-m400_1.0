"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const company_controller_1 = require("./company.controller");
const company_service_1 = require("./company.service");
const mongoose_1 = require("@nestjs/mongoose");
const company_schema_1 = require("./schemas/company.schema");
const work_place_module_1 = require("../work-place/work-place.module");
const work_place_service_1 = require("../work-place/work-place.service");
const user_module_1 = require("../user/user.module");
const user_service_1 = require("../user/user.service");
let CompanyModule = class CompanyModule {
};
CompanyModule = __decorate([
    common_1.Module({
        imports: [
            work_place_module_1.WorkPlaceModule,
            user_module_1.UserModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'Company', schema: company_schema_1.CompanySchema, collection: 'company' },
            ]),
        ],
        controllers: [company_controller_1.CompanyController],
        providers: [company_service_1.CompanyService, work_place_service_1.WorkPlaceService, user_service_1.UserService],
    })
], CompanyModule);
exports.CompanyModule = CompanyModule;
//# sourceMappingURL=company.module.js.map