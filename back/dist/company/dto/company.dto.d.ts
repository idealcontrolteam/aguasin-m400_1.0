export declare class CreateCompanyDTO {
    name: string;
    logo: string;
    active: boolean;
}
