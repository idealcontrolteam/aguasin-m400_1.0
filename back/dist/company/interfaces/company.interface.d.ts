import { Document } from 'mongoose';
export interface Company extends Document {
    name: string;
    logo: string;
    active: boolean;
}
