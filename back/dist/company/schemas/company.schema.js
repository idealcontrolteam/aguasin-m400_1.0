"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.CompanySchema = new mongoose_1.Schema({
    name: String,
    logo: String,
    active: Boolean,
}, { versionKey: false });
//# sourceMappingURL=company.schema.js.map