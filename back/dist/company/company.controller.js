"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const company_dto_1 = require("./dto/company.dto");
const company_service_1 = require("./company.service");
let CompanyController = class CompanyController {
    constructor(companyService) {
        this.companyService = companyService;
    }
    createCompany(res, createCompanyDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newCompany = yield this.companyService.createCompany(createCompanyDTO);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Company created successfully',
                data: newCompany,
            });
        });
    }
    getCompanies(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const companies = yield this.companyService.getCompanies();
            let msg = companies.length == 0 ? 'Companies not found' : 'Companies fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: companies,
                count: companies.length,
            });
        });
    }
    getWorkPlacesByCompanyId(res, companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            const workplaces = yield this.companyService.getWorkPlacesByCompanyId(companyId);
            let msg = workplaces.length == 0 ? 'Work places not found' : 'Work places fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: workplaces,
                count: workplaces.length,
            });
        });
    }
    getUsersByCompanyId(res, companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.companyService.getUsersByCompanyId(companyId);
            let msg = users.length == 0 ? 'Users not found' : 'Users fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: users,
                count: users.length,
            });
        });
    }
    getCompany(res, companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Company id is not a valid ObjectId');
            }
            const company = yield this.companyService.getCompany(companyId);
            if (!company) {
                throw new common_1.NotFoundException('Company not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Company found',
                data: company,
            });
        });
    }
    updateCompany(res, body, companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Company id is not a valid ObjectId');
            }
            const updatedCompany = yield this.companyService.updateCompany(companyId, body);
            if (!updatedCompany) {
                throw new common_1.NotFoundException('Company not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Company updated successfully',
                data: updatedCompany,
            });
        });
    }
    deleteCompany(res, companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Company id is not a valid ObjectId');
            }
            const company = yield this.companyService.deleteCompany(companyId);
            if (!company) {
                throw new common_1.NotFoundException('Company not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Company Deleted',
                data: company,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, company_dto_1.CreateCompanyDTO]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "createCompany", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "getCompanies", null);
__decorate([
    common_1.Get('/:companyId/workPlace'),
    __param(0, common_1.Res()), __param(1, common_1.Param('companyId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "getWorkPlacesByCompanyId", null);
__decorate([
    common_1.Get('/:companyId/user'),
    __param(0, common_1.Res()), __param(1, common_1.Param('companyId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "getUsersByCompanyId", null);
__decorate([
    common_1.Get('/:companyId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('companyId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "getCompany", null);
__decorate([
    common_1.Put('/:companyId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('companyId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, company_dto_1.CreateCompanyDTO, Object]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "updateCompany", null);
__decorate([
    common_1.Delete('/:companyId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('companyId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CompanyController.prototype, "deleteCompany", null);
CompanyController = __decorate([
    common_1.Controller('company'),
    __metadata("design:paramtypes", [company_service_1.CompanyService])
], CompanyController);
exports.CompanyController = CompanyController;
//# sourceMappingURL=company.controller.js.map