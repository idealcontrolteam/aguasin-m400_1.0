"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tag_dto_1 = require("./dto/tag.dto");
const tag_service_1 = require("./tag.service");
const tag_config_control_service_1 = require("../tag-config-control/tag-config-control.service");
let TagController = class TagController {
    constructor(tagService, tagConfigControlService) {
        this.tagService = tagService;
        this.tagConfigControlService = tagConfigControlService;
        this.validateIds = body => {
            Object.keys(body).map(key => {
                if (key == 'tagTypeId' ||
                    key == 'zoneId' ||
                    key == 'locationId' ||
                    key == 'locationId' ||
                    key == 'deviceId') {
                    if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
                        throw new common_1.BadRequestException(`${key} is not a valid ObjectId`);
                    }
                }
            });
        };
    }
    updateTagMultiple(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            let updatedTag = {};
            body.map((Tag) => __awaiter(this, void 0, void 0, function* () {
                updatedTag = yield this.tagService.updateTag(Tag._id, Tag);
            }));
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag updated',
                data: updatedTag,
            });
        });
    }
    createModel(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            this.validateIds(body);
            const newTag = yield this.tagService.createTag(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Tag created successfully',
                data: newTag,
            });
        });
    }
    getTagsFilterRegister(res, valor) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagService.getTagsFilterRegister(valor);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getTags(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagService.getTags();
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getTagsByZoneIdWithMeasurements(res, zoneId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Zone id is not a valid  ObjectId');
            }
            const tags = yield this.tagService.getTagsByZoneIdWithMeasurements(zoneId, fini, ffin);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getTagsByLocationIdWithMeasurements(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid  ObjectId');
            }
            const tags = yield this.tagService.getTagsByLocationIdWithMeasurements(locationId, fini, ffin);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getTagsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagService.getTagsAll();
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getMeasurementsByTagId(res, tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid  ObjectId');
            }
            const measurements = yield this.tagService.getMeasurementsByTagId(tagId);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getAlarmsByTagId(res, tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid  ObjectId');
            }
            const alarms = yield this.tagService.getAlarmsByTagId(tagId);
            let msg = alarms.length == 0 ? 'Alarms not found' : 'Alarms fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: alarms,
                count: alarms.length,
            });
        });
    }
    getFailsByTagId(res, tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid  ObjectId');
            }
            const fails = yield this.tagService.getFailsByTagId(tagId);
            let msg = fails.length == 0 ? 'Fails not found' : 'Fails fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: fails,
                count: fails.length,
            });
        });
    }
    geTagConfigControlsByTagId(res, tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid  ObjectId');
            }
            const tagConfigControls = yield this.tagConfigControlService.geTagConfigControlsByTagId(tagId);
            let msg = tagConfigControls.length == 0
                ? 'Tag config controls not found'
                : 'Tag config controls fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tagConfigControls,
                count: tagConfigControls.length,
            });
        });
    }
    getTag(res, tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid  ObjectId');
            }
            const tag = yield this.tagService.getTag(tagId);
            if (!tag) {
                throw new common_1.NotFoundException('Tag not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag found',
                data: tag,
            });
        });
    }
    updateTag(res, body, tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid  ObjectId');
            }
            const updatedTag = yield this.tagService.updateTag(tagId, body);
            if (!updatedTag) {
                throw new common_1.NotFoundException('Tag not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag updated',
                data: updatedTag,
            });
        });
    }
    deleteTag(res, tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid  ObjectId');
            }
            const deletedTag = yield this.tagService.deleteTag(tagId);
            if (!deletedTag) {
                throw new common_1.NotFoundException('Tag not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag deleted',
                data: deletedTag,
            });
        });
    }
};
__decorate([
    common_1.Post('/multiple'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "updateTagMultiple", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, tag_dto_1.CreateTagDTO]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "createModel", null);
__decorate([
    common_1.Get('/filteroregister/:valor'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('valor')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getTagsFilterRegister", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getTags", null);
__decorate([
    common_1.Get('/zone/:zoneId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('zoneId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getTagsByZoneIdWithMeasurements", null);
__decorate([
    common_1.Get('/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getTagsByLocationIdWithMeasurements", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getTagsAll", null);
__decorate([
    common_1.Get('/:tagId/measurement'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getMeasurementsByTagId", null);
__decorate([
    common_1.Get('/:tagId/alarm'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getAlarmsByTagId", null);
__decorate([
    common_1.Get('/:tagId/fail'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getFailsByTagId", null);
__decorate([
    common_1.Get('/:tagId/tagConfigControl'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "geTagConfigControlsByTagId", null);
__decorate([
    common_1.Get('/:tagId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "getTag", null);
__decorate([
    common_1.Put('/:tagId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('tagId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, tag_dto_1.CreateTagDTO, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "updateTag", null);
__decorate([
    common_1.Delete('/:tagId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagController.prototype, "deleteTag", null);
TagController = __decorate([
    common_1.Controller('tag'),
    __metadata("design:paramtypes", [tag_service_1.TagService,
        tag_config_control_service_1.TagConfigControlService])
], TagController);
exports.TagController = TagController;
//# sourceMappingURL=tag.controller.js.map