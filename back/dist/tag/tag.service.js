"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const measurement_service_1 = require("../measurement/measurement.service");
const fail_service_1 = require("../fail/fail.service");
const alarm_service_1 = require("../alarm/alarm.service");
const lodash_1 = require("lodash");
let TagService = class TagService {
    constructor(tagModel, measurementService, failService, alarmService) {
        this.tagModel = tagModel;
        this.measurementService = measurementService;
        this.failService = failService;
        this.alarmService = alarmService;
    }
    getTagsFilterRegister(valor) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel.find({ "register": Boolean(valor) });
            return tags;
        });
    }
    getTags() {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel.find();
            return tags;
        });
    }
    getTag(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const tag = yield this.tagModel.findById(id);
            return tag;
        });
    }
    getTagsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel
                .find()
                .populate('tagTypeId zoneId locationId sensorId deviceId');
            return tags;
        });
    }
    getTagsByDeviceId(deviceId) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('[IN TAG SERVICE]', deviceId);
            const tags = yield this.tagModel.find({ deviceId: deviceId });
            return tags;
        });
    }
    getTagsByZoneId(zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel.find({ zoneId: zoneId }, 'nameAddress lastValue');
            return tags;
        });
    }
    getTagsByZoneIdWithMeasurements(zoneId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel.find({ zoneId: zoneId });
            let tagsIdZone = {};
            tags.map((tag, pos) => (tagsIdZone[tag._id] = pos));
            const measurements = yield this.measurementService.getMeasurementFilteredByTagsAndDate(Object.keys(tagsIdZone), fini, ffin);
            let tagsUsed = [];
            let responseJson = [];
            let positionsTags = {};
            measurements.map(measurement => {
                if (!tagsUsed.includes(measurement.tagId.toString())) {
                    const tagToUse = tags[tagsIdZone[measurement.tagId]];
                    responseJson.push(Object.assign({}, tagToUse.toJSON(), { measurements: [] }));
                    tagsUsed.push(measurement.tagId.toString());
                    positionsTags[measurement.tagId] = tagsUsed.length - 1;
                    responseJson[responseJson.length - 1].measurements.push(measurement);
                }
                else {
                    responseJson[positionsTags[measurement.tagId]]['measurements'].push(measurement);
                }
            });
            return responseJson;
        });
    }
    getTagsByLocationIdWithMeasurements(locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel.find({ locationId: locationId }).collation({ locale: 'en', strength: 2 }).sort({ shortName: 1 });
            let tagsIdLocationId = {};
            tags.map((tag, pos) => (tagsIdLocationId[tag._id] = pos));
            const measurements = yield this.measurementService.getMeasurementFilteredByTagsAndDate(Object.keys(tagsIdLocationId), fini, ffin);
            let tagsUsed = [];
            let responseJson = [];
            let positionsTags = {};
            measurements.map(measurement => {
                if (!tagsUsed.includes(measurement.tagId.toString())) {
                    const tagToUse = tags[tagsIdLocationId[measurement.tagId]];
                    responseJson.push(Object.assign({}, tagToUse.toJSON(), { measurements: [] }));
                    tagsUsed.push(measurement.tagId.toString());
                    positionsTags[measurement.tagId] = tagsUsed.length - 1;
                    responseJson[responseJson.length - 1].measurements.push(measurement);
                }
                else {
                    responseJson[positionsTags[measurement.tagId]]['measurements'].push(measurement);
                }
            });
            return lodash_1._.orderBy(responseJson, ['shortName'], ['desc']);
        });
    }
    getTagsByLocationId(locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel.find({ locationId: locationId });
            return tags;
        });
    }
    getTagsBySensorId(sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel.find({ sensorId: sensorId });
            return tags;
        });
    }
    getTagsByTagTyperId(tagTypeId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagModel.find({ tagTypeId: tagTypeId });
            return tags;
        });
    }
    getMeasurementsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurementsByTagId(tagId);
            return measurements;
        });
    }
    getAlarmsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const alarms = yield this.alarmService.getAlarmsByTagId(tagId);
            return alarms;
        });
    }
    getFailsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const fails = yield this.failService.getFailsByTagId(tagId);
            return fails;
        });
    }
    createTag(createTagDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newTag = new this.tagModel(createTagDTO);
            return yield newTag.save();
        });
    }
    deleteTag(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.tagModel.findByIdAndDelete(id);
        });
    }
    updateTag(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.tagModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
TagService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Tag')),
    __metadata("design:paramtypes", [mongoose_1.Model,
        measurement_service_1.MeasurementService,
        fail_service_1.FailService,
        alarm_service_1.AlarmService])
], TagService);
exports.TagService = TagService;
//# sourceMappingURL=tag.service.js.map