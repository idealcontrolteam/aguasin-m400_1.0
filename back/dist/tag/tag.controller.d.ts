import { CreateTagDTO } from './dto/tag.dto';
import { TagService } from './tag.service';
import { TagConfigControlService } from '../tag-config-control/tag-config-control.service';
export declare class TagController {
    private tagService;
    private tagConfigControlService;
    constructor(tagService: TagService, tagConfigControlService: TagConfigControlService);
    validateIds: (body: any) => void;
    updateTagMultiple(res: any, body: any): Promise<any>;
    createModel(res: any, body: CreateTagDTO): Promise<any>;
    getTagsFilterRegister(res: any, valor: any): Promise<any>;
    getTags(res: any): Promise<any>;
    getTagsByZoneIdWithMeasurements(res: any, zoneId: any, fini: any, ffin: any): Promise<any>;
    getTagsByLocationIdWithMeasurements(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    getTagsAll(res: any): Promise<any>;
    getMeasurementsByTagId(res: any, tagId: any): Promise<any>;
    getAlarmsByTagId(res: any, tagId: any): Promise<any>;
    getFailsByTagId(res: any, tagId: any): Promise<any>;
    geTagConfigControlsByTagId(res: any, tagId: any): Promise<any>;
    getTag(res: any, tagId: any): Promise<any>;
    updateTag(res: any, body: CreateTagDTO, tagId: any): Promise<any>;
    deleteTag(res: any, tagId: any): Promise<any>;
}
