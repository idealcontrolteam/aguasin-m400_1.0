"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.TagSchema = new mongoose_1.Schema({
    name: String,
    shortName: String,
    unity: String,
    nameAddress: String,
    address: String,
    factor: Number,
    offset: Number,
    lastValue: Number,
    lastValueWrite: Number,
    dateTimeLastValue: Date,
    tagTypeId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'TagType',
        required: true,
    },
    zoneId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Zone',
        required: true,
    },
    locationId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Location',
        required: true,
    },
    sensorId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Sensor',
        required: false,
    },
    active: Boolean,
    write: Boolean,
    read: Boolean,
    register: Boolean,
    deviceId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Device',
        required: false,
    },
    alertMax: Number,
    alertMin: Number,
    failMax: Number,
    failMin: Number,
    max: Number,
    min: Number,
    state: Number,
    banda: Boolean,
    tagId: Number,
}, { versionKey: false });
//# sourceMappingURL=tag.schema.js.map