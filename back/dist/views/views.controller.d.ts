import { ViewsService } from './views.service';
export declare class ViewsController {
    private measurementService;
    constructor(measurementService: ViewsService);
    getViews(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
}
