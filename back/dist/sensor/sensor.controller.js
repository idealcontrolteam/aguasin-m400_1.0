"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const sensor_dto_1 = require("./dto/sensor.dto");
const sensor_service_1 = require("./sensor.service");
let SensorController = class SensorController {
    constructor(sensorService) {
        this.sensorService = sensorService;
    }
    createSensor(res, createSensorDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const sensor = yield this.sensorService.createSensor(createSensorDTO);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Sensor created successfully',
                data: sensor,
            });
        });
    }
    getSensors(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const sensors = yield this.sensorService.getSensors();
            let msg = sensors.length == 0 ? 'Sensors not found' : 'Sensors fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: sensors,
                count: sensors.length,
            });
        });
    }
    getSensorsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const sensors = yield this.sensorService.getSensorsAll();
            let msg = sensors.length == 0 ? 'Sensors not found' : 'Sensors fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: sensors,
                count: sensors.length,
            });
        });
    }
    getMeasurementsBySensorId(res, sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.sensorService.getMeasurementsBySensorId(sensorId);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getTagsBySensorId(res, sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const tags = yield this.sensorService.getTagsBySensorId(sensorId);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getBrand(res, sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const sensor = yield this.sensorService.getSensor(sensorId);
            if (!sensor) {
                throw new common_1.NotFoundException('Sensor not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Sensor found',
                data: sensor,
            });
        });
    }
    updateSensor(res, body, sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const updatedSensor = yield this.sensorService.updateSensor(sensorId, body);
            if (!updatedSensor) {
                throw new common_1.NotFoundException('Sensor not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Sensor updated successfully',
                data: updatedSensor,
            });
        });
    }
    deleteSensor(res, sensorId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const sensor = yield this.sensorService.deleteSensor(sensorId);
            if (!sensor) {
                throw new common_1.NotFoundException('Sensor not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Sensor Deleted',
                data: sensor,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, sensor_dto_1.CreateSensorDTO]),
    __metadata("design:returntype", Promise)
], SensorController.prototype, "createSensor", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SensorController.prototype, "getSensors", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SensorController.prototype, "getSensorsAll", null);
__decorate([
    common_1.Get('/:sensorId/measurement'),
    __param(0, common_1.Res()), __param(1, common_1.Param('sensorId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SensorController.prototype, "getMeasurementsBySensorId", null);
__decorate([
    common_1.Get('/:sensorId/tag'),
    __param(0, common_1.Res()), __param(1, common_1.Param('sensorId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SensorController.prototype, "getTagsBySensorId", null);
__decorate([
    common_1.Get('/:sensorId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('sensorId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SensorController.prototype, "getBrand", null);
__decorate([
    common_1.Put('/:sensorId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('sensorId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, sensor_dto_1.CreateSensorDTO, Object]),
    __metadata("design:returntype", Promise)
], SensorController.prototype, "updateSensor", null);
__decorate([
    common_1.Delete('/:sensorId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('sensorId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SensorController.prototype, "deleteSensor", null);
SensorController = __decorate([
    common_1.Controller('sensor'),
    __metadata("design:paramtypes", [sensor_service_1.SensorService])
], SensorController);
exports.SensorController = SensorController;
//# sourceMappingURL=sensor.controller.js.map