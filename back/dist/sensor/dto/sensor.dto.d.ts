export declare class CreateSensorDTO {
    name: string;
    serialNumber: string;
    active: boolean;
    locationId: string;
    shortName: string;
    description: string;
}
