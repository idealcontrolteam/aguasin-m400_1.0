"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.SensorSchema = new mongoose_1.Schema({
    name: String,
    serialNumber: String,
    active: Boolean,
    locationId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Location',
        required: true,
    },
    shortName: String,
    description: String,
}, { versionKey: false });
//# sourceMappingURL=sensor.schema.js.map