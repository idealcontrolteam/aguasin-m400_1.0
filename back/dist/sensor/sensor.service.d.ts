import { Model } from 'mongoose';
import { CreateSensorDTO } from './dto/sensor.dto';
import { Sensor } from './interfaces/sensor.interface';
import { Measurement } from '../measurement/interfaces/measurement.interface';
import { MeasurementService } from '../measurement/measurement.service';
import { TagService } from '../tag/tag.service';
import { Tag } from '../tag/interfaces/tag.interface';
export declare class SensorService {
    private sensorModel;
    private measurementService;
    private tagService;
    constructor(sensorModel: Model<Sensor>, measurementService: MeasurementService, tagService: TagService);
    getSensors(): Promise<Sensor[]>;
    getSensorsAll(): Promise<Sensor[]>;
    getSensorsByLocationId(locationId: any): Promise<Sensor[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<Measurement[]>;
    getTagsBySensorId(sensorId: any): Promise<Tag[]>;
    getSensor(id: any): Promise<Sensor>;
    createSensor(createSensorDTO: CreateSensorDTO): Promise<Sensor>;
    deleteSensor(id: any): Promise<Sensor>;
    updateSensor(id: string, body: CreateSensorDTO): Promise<Sensor>;
}
