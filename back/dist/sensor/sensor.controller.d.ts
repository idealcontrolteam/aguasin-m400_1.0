import { CreateSensorDTO } from './dto/sensor.dto';
import { SensorService } from './sensor.service';
export declare class SensorController {
    private sensorService;
    constructor(sensorService: SensorService);
    createSensor(res: any, createSensorDTO: CreateSensorDTO): Promise<any>;
    getSensors(res: any): Promise<any>;
    getSensorsAll(res: any): Promise<any>;
    getMeasurementsBySensorId(res: any, sensorId: any): Promise<any>;
    getTagsBySensorId(res: any, sensorId: any): Promise<any>;
    getBrand(res: any, sensorId: any): Promise<any>;
    updateSensor(res: any, body: CreateSensorDTO, sensorId: any): Promise<any>;
    deleteSensor(res: any, sensorId: any): Promise<any>;
}
