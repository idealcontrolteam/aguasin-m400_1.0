export declare class CreateUserDTO {
    name: string;
    email: string;
    username: string;
    password: string;
    roleId: string;
    companyId: string;
    active: boolean;
    token: string;
}
