"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.UserSchema = new mongoose_1.Schema({
    name: String,
    email: String,
    username: String,
    password: String,
    roleId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Role',
        required: true,
    },
    companyId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Company',
        required: true,
    },
    active: Boolean,
    token: String,
    lastConnection: {
        type: Date,
        default: null,
    },
}, { versionKey: false });
//# sourceMappingURL=user.schema.js.map