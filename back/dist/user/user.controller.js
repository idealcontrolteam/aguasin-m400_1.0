"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const user_dto_1 = require("./dto/user.dto");
const user_service_1 = require("./user.service");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
let UserController = class UserController {
    constructor(userService) {
        this.userService = userService;
    }
    createUser(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newUser = yield this.userService.createUser(body);
            newUser.password = '';
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'User created successfully',
                data: newUser,
            });
        });
    }
    login(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const userDB = yield this.userService.login(body);
            if (!userDB || !(yield bcrypt.compare(body.password, userDB.password))) {
                return res.status(common_1.HttpStatus.UNAUTHORIZED).json({
                    statusCode: common_1.HttpStatus.UNAUTHORIZED,
                    message: 'Not authorized',
                });
            }
            userDB.lastConnection = new Date();
            const userUpdated = yield userDB.save();
            const token = this.getToken(userUpdated);
            const updateToken = yield this.userService.updateUserToken(userDB._id, { "token": token });
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'User loged in successfuly',
                data: {
                    rol: userDB.roleId,
                    user: userUpdated.name,
                    token: token,
                    expiresIn: 60 * 1000,
                },
            });
        });
    }
    getToken(user) {
        const { id, username, email, roleId } = user;
        return jwt.sign({ id, username, email, roleId }, 'ULTRASMEGAECRETO', {
            expiresIn: 60,
        });
    }
    checkToken(res, token) {
        return __awaiter(this, void 0, void 0, function* () {
            var decoded = jwt.decode(token);
            const user = yield this.userService.checkToken(decoded);
            if (!user) {
                throw new common_1.NotFoundException('User not found');
            }
            if (user.token != token) {
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: 'NO',
                    data: user,
                });
            }
            else {
                return res.status(common_1.HttpStatus.OK).json({
                    statusCode: common_1.HttpStatus.OK,
                    message: 'User found',
                    data: user,
                });
            }
        });
    }
    getUsers(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.userService.getUsers();
            let msg = users.length == 0 ? 'Users not found' : 'Users fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: users,
                count: users.length,
            });
        });
    }
    getUsersAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.userService.getUsersAll();
            let msg = users.length == 0 ? 'Users not found' : 'Users fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: users,
                count: users.length,
            });
        });
    }
    getUser(res, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Id is not a valid ObjectId');
            }
            const user = yield this.userService.getUser(userId);
            if (!user) {
                throw new common_1.NotFoundException('User not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'User found',
                data: user,
            });
        });
    }
    updateUser(res, body, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!userId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Id is not a valid  ObjectId');
            }
            const updatedUser = yield this.userService.updateUser(userId, body);
            if (!updatedUser) {
                throw new common_1.NotFoundException('User not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'User updated',
                data: updatedUser,
            });
        });
    }
    deleteUser(res, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedUser = yield this.userService.deleteUser(userId);
            if (!deletedUser) {
                throw new common_1.NotFoundException('User not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'User deleted',
                data: deletedUser,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_dto_1.CreateUserDTO]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "createUser", null);
__decorate([
    common_1.Post('/login'),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "login", null);
__decorate([
    common_1.Get('/check_token/:token'),
    __param(0, common_1.Res()), __param(1, common_1.Param('token')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "checkToken", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUsers", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUsersAll", null);
__decorate([
    common_1.Get('/:userId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('userId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getUser", null);
__decorate([
    common_1.Put('/:userId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('userId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, user_dto_1.CreateUserDTO, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "updateUser", null);
__decorate([
    common_1.Delete('/:userId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('userId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "deleteUser", null);
UserController = __decorate([
    common_1.Controller('user'),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map