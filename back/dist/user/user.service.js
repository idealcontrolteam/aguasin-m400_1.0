"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const bcrypt = require("bcryptjs");
let UserService = class UserService {
    constructor(userModel) {
        this.userModel = userModel;
        this.hashPassword = (password) => __awaiter(this, void 0, void 0, function* () {
            const hashedPassword = yield bcrypt.hash(password, 10);
            return hashedPassword;
        });
    }
    getUsers() {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.userModel.find();
            return users;
        });
    }
    getUsersByRoleId(roleId) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.userModel.find({ roleId: roleId });
            return users;
        });
    }
    getUsersAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.userModel.find().populate('companyId roleId');
            return users;
        });
    }
    getUsersByCompanyId(companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.userModel.find({ companyId: companyId });
            return users;
        });
    }
    getUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userModel.findById(id);
            return user;
        });
    }
    checkToken(u) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email } = u;
            const user = yield this.userModel.findOne({ email });
            return user;
        });
    }
    createUser(createUserDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            createUserDTO.password = yield this.hashPassword(createUserDTO.password);
            const newUser = new this.userModel(createUserDTO);
            return yield newUser.save();
        });
    }
    login(user) {
        return __awaiter(this, void 0, void 0, function* () {
            const { email } = user;
            const userDB = yield this.userModel.findOne({ email });
            return userDB;
        });
    }
    deleteUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userModel.findByIdAndDelete(id);
        });
    }
    deleteUsersByCompanyId(companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userModel.deleteMany({ companyId: companyId });
        });
    }
    updateUser(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
    updateUserToken(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
UserService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('User')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map