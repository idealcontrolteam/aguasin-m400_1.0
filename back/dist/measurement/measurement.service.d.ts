import { Model } from 'mongoose';
import { Measurement } from './interfaces/measurement.interface';
import { CreateMeasurementDTO } from './dto/measurement.dto';
export declare class MeasurementService {
    private measurementModel;
    constructor(measurementModel: Model<Measurement>);
    getMeasurements(): Promise<Measurement[]>;
    getMeasurementsAll(): Promise<Measurement[]>;
    getMeasurementByTagsEndfiltered(tagId: any): Promise<Measurement[]>;
    getMeasurementByTagfiltered(tagId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurementByTagfilteredXY(tagId: any, fini: any, ffin: any): Promise<any[]>;
    getMeasurementFilteredByTagsAndDate(tags: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurementBySensorfiltered(sensorId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurementByLocationfiltered(locationId: any, fini: any, ffin: any): Promise<Measurement[]>;
    getMeasurement(id: any): Promise<Measurement>;
    getMeasurementsByLocationId(locationId: any): Promise<Measurement[]>;
    getMeasurementsDateEndTagId(tagId: any, dateTime: any): Promise<Measurement[]>;
    getMeasurementsBySensorId(sensorId: any): Promise<Measurement[]>;
    getMeasurementsByTagId(tagId: any): Promise<Measurement[]>;
    createMeasurement(createMeasurementDTO: CreateMeasurementDTO): Promise<Measurement>;
    getMeasurementsArrayTag(body: any): Promise<any[]>;
    deleteMeasurement(id: any): Promise<Measurement>;
    updateMeasurement(id: string, body: CreateMeasurementDTO): Promise<Measurement>;
}
