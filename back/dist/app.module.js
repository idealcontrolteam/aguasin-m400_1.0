"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const brand_module_1 = require("./brand/brand.module");
const mongoose_1 = require("@nestjs/mongoose");
const model_module_1 = require("./model/model.module");
const tcp_connection_module_1 = require("./tcp-connection/tcp-connection.module");
const serial_connection_module_1 = require("./serial-connection/serial-connection.module");
const gateway_module_1 = require("./gateway/gateway.module");
const device_module_1 = require("./device/device.module");
const measurement_copy_module_1 = require("./measurement-copy/measurement-copy.module");
const category_module_1 = require("./category/category.module");
const company_module_1 = require("./company/company.module");
const work_place_module_1 = require("./work-place/work-place.module");
const user_module_1 = require("./user/user.module");
const role_module_1 = require("./role/role.module");
const zone_module_1 = require("./zone/zone.module");
const location_module_1 = require("./location/location.module");
const sensor_module_1 = require("./sensor/sensor.module");
const measurement_module_1 = require("./measurement/measurement.module");
const tag_module_1 = require("./tag/tag.module");
const tag_type_module_1 = require("./tag-type/tag-type.module");
const tag_config_control_module_1 = require("./tag-config-control/tag-config-control.module");
const alarm_module_1 = require("./alarm/alarm.module");
const fail_module_1 = require("./fail/fail.module");
const views_controller_1 = require("./views/views.controller");
const views_module_1 = require("./views/views.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            brand_module_1.BrandModule,
            mongoose_1.MongooseModule.forRoot('mongodb://idealcontrol:Ideal_cloud281@169.53.138.44:27017/bdaguasin?authSource=admin', {
                useNewUrlParser: true,
                useFindAndModify: false
            }),
            model_module_1.ModelModule,
            tcp_connection_module_1.TcpConnectionModule,
            serial_connection_module_1.SerialConnectionModule,
            gateway_module_1.GatewayModule,
            device_module_1.DeviceModule,
            measurement_copy_module_1.MeasurementCopyModule,
            category_module_1.CategoryModule,
            company_module_1.CompanyModule,
            work_place_module_1.WorkPlaceModule,
            user_module_1.UserModule,
            role_module_1.RoleModule,
            zone_module_1.ZoneModule,
            location_module_1.LocationModule,
            sensor_module_1.SensorModule,
            measurement_module_1.MeasurementModule,
            tag_module_1.TagModule,
            tag_type_module_1.TagTypeModule,
            tag_config_control_module_1.TagConfigControlModule,
            alarm_module_1.AlarmModule,
            fail_module_1.FailModule,
            views_module_1.ViewsModule,
        ],
        controllers: [app_controller_1.AppController, views_controller_1.ViewsController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map