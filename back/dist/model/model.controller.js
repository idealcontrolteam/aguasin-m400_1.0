"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const model_dto_1 = require("./dto/model.dto");
const model_service_1 = require("./model.service");
const device_service_1 = require("../device/device.service");
let ModelController = class ModelController {
    constructor(modelService, deviceService) {
        this.modelService = modelService;
        this.deviceService = deviceService;
    }
    createModel(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!body.brandId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('BrandId is not a valid  ObjectId');
            }
            const newModel = yield this.modelService.createModel(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Model created successfully',
                data: newModel,
            });
        });
    }
    getModels(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const models = yield this.modelService.getModels();
            let msg = models.length == 0 ? 'Models not found' : 'Models fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: models,
                count: models.length,
            });
        });
    }
    getModelsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const models = yield this.modelService.getModelsAll();
            let msg = models.length == 0 ? 'Models not found' : 'Models fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: models,
                count: models.length,
            });
        });
    }
    getDevicesByModelId(res, modelid) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceService.getDevicesByModelId(modelid);
            let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: devices,
                count: devices.length,
            });
        });
    }
    getModel(res, modelid) {
        return __awaiter(this, void 0, void 0, function* () {
            const model = yield this.modelService.getModel(modelid);
            if (!model) {
                throw new common_1.NotFoundException('Model not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Model found',
                data: model,
            });
        });
    }
    updateModel(res, body, modelid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!body.brandId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('BrandId is not a valid  ObjectId');
            }
            const updatedModel = yield this.modelService.updateModel(modelid, body);
            if (!updatedModel) {
                throw new common_1.NotFoundException('Model not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Model updated',
                data: updatedModel,
            });
        });
    }
    deleteModelsByBrandId(res, brandId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedModels = yield this.modelService.deleteModelsByBrandId(brandId);
            if (!deletedModels) {
                throw new common_1.NotFoundException('No models deleted');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Models deleted',
                data: deletedModels,
            });
        });
    }
    deleteModel(res, modelid) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedModel = yield this.modelService.deleteModel(modelid);
            if (!deletedModel) {
                throw new common_1.NotFoundException('Model not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Model deleted',
                data: deletedModel,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, model_dto_1.CreateModelDTO]),
    __metadata("design:returntype", Promise)
], ModelController.prototype, "createModel", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ModelController.prototype, "getModels", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ModelController.prototype, "getModelsAll", null);
__decorate([
    common_1.Get('/:modelid/device'),
    __param(0, common_1.Res()), __param(1, common_1.Param('modelid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ModelController.prototype, "getDevicesByModelId", null);
__decorate([
    common_1.Get('/:modelid'),
    __param(0, common_1.Res()), __param(1, common_1.Param('modelid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ModelController.prototype, "getModel", null);
__decorate([
    common_1.Put('/:modelid'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('modelid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, model_dto_1.CreateModelDTO, Object]),
    __metadata("design:returntype", Promise)
], ModelController.prototype, "updateModel", null);
__decorate([
    common_1.Delete('/deleteByBrandId/:brandId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('brandId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ModelController.prototype, "deleteModelsByBrandId", null);
__decorate([
    common_1.Delete('/:modelid'),
    __param(0, common_1.Res()), __param(1, common_1.Param('modelid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ModelController.prototype, "deleteModel", null);
ModelController = __decorate([
    common_1.Controller('model'),
    __metadata("design:paramtypes", [model_service_1.ModelService,
        device_service_1.DeviceService])
], ModelController);
exports.ModelController = ModelController;
//# sourceMappingURL=model.controller.js.map