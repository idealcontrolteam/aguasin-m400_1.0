/// <reference types="mongodb" />
import { Model } from 'mongoose';
import * as ModelI from './interfaces/model.interface';
import { CreateModelDTO } from './dto/model.dto';
import { DeviceService } from '../device/device.service';
import { Device } from '../device/interfaces/device.interface';
export declare class ModelService {
    private modelModel;
    private deviceService;
    constructor(modelModel: Model<ModelI.Model>, deviceService: DeviceService);
    getModels(): Promise<ModelI.Model[]>;
    getModel(id: any): Promise<ModelI.Model>;
    getModelsAll(): Promise<ModelI.Model[]>;
    getModelsByBrandId(brandId: any): Promise<ModelI.Model[]>;
    getDevicesByModelId(modelid: any): Promise<Device[]>;
    createModel(createModelDTO: CreateModelDTO): Promise<ModelI.Model>;
    deleteModel(id: any): Promise<ModelI.Model>;
    deleteModelsByBrandId(brandId: any): Promise<{
        ok?: number;
        n?: number;
    }>;
    updateModel(id: string, body: CreateModelDTO): Promise<ModelI.Model>;
}
