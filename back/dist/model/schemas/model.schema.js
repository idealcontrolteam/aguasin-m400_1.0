"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ModelSchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
    brandId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Brand',
        required: true,
    },
}, { versionKey: false });
//# sourceMappingURL=model.schema.js.map