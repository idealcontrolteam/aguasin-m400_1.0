export declare class CreateModelDTO {
    name: string;
    active: boolean;
    brandId: string;
}
