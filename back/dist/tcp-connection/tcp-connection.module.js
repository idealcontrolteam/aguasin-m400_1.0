"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tcp_connection_controller_1 = require("./tcp-connection.controller");
const tcp_connection_service_1 = require("./tcp-connection.service");
const tcpConnection_schema_1 = require("./schemas/tcpConnection.schema");
const mongoose_1 = require("@nestjs/mongoose");
const device_module_1 = require("../device/device.module");
const device_service_1 = require("../device/device.service");
let TcpConnectionModule = class TcpConnectionModule {
};
TcpConnectionModule = __decorate([
    common_1.Module({
        imports: [
            device_module_1.DeviceModule,
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'TcpConnection',
                    schema: tcpConnection_schema_1.TcpConnectionSchema,
                    collection: 'tcpConnection',
                },
            ]),
        ],
        controllers: [tcp_connection_controller_1.TcpConnectionController],
        providers: [tcp_connection_service_1.TcpConnectionService, device_service_1.DeviceService],
        exports: [tcp_connection_service_1.TcpConnectionService],
    })
], TcpConnectionModule);
exports.TcpConnectionModule = TcpConnectionModule;
//# sourceMappingURL=tcp-connection.module.js.map