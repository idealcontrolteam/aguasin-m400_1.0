import { Model } from 'mongoose';
import { TcpConnection } from './interfaces/tcpConnection.interface';
import { CreateTcpConnectionDTO } from './dto/tcpConnection.dto';
import { DeviceService } from '../device/device.service';
import { Device } from '../device/interfaces/device.interface';
export declare class TcpConnectionService {
    private tcpConnectionModel;
    private deviceService;
    constructor(tcpConnectionModel: Model<TcpConnection>, deviceService: DeviceService);
    getTcpConnections(): Promise<TcpConnection[]>;
    getTcpConnection(id: any): Promise<TcpConnection>;
    getDevicesByTcpConnectionId(tcpConnectionId: any): Promise<Device[]>;
    createTcpConnection(createTcpConnectionDTO: CreateTcpConnectionDTO): Promise<TcpConnection>;
    deleteTcpConnection(id: any): Promise<TcpConnection>;
    updateTcpConnection(id: string, body: CreateTcpConnectionDTO): Promise<TcpConnection>;
}
