import { CreateTcpConnectionDTO } from './dto/tcpConnection.dto';
import { TcpConnectionService } from './tcp-connection.service';
export declare class TcpConnectionController {
    private tcpConnectionService;
    constructor(tcpConnectionService: TcpConnectionService);
    createTcpConnection(res: any, body: CreateTcpConnectionDTO): Promise<any>;
    getTcpConnections(res: any): Promise<any>;
    getDevicesByTcpConnectionId(res: any, tcpConnectionId: any): Promise<any>;
    getTcpConnection(res: any, tcpConnectionId: any): Promise<any>;
    updateTcpConnection(res: any, body: CreateTcpConnectionDTO, tcpConnectionId: any): Promise<any>;
    deleteTcpConnection(res: any, tcpConnectionId: any): Promise<any>;
}
