"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.TcpConnectionSchema = new mongoose_1.Schema({
    ip: String,
    port: String,
}, { versionKey: false });
//# sourceMappingURL=tcpConnection.schema.js.map