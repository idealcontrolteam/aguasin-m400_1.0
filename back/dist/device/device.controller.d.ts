import { CreateDeviceDTO } from './dto/device.dto';
import { DeviceService } from './device.service';
export declare class DeviceController {
    private deviceService;
    constructor(deviceService: DeviceService);
    validateIds: (body: any) => void;
    createDevice(res: any, body: CreateDeviceDTO): Promise<any>;
    getDevices(res: any): Promise<any>;
    getDevicesAll(res: any): Promise<any>;
    getTagsByDeviceId(res: any, deviceid: any): Promise<any>;
    getDevice(res: any, deviceid: any): Promise<any>;
    updateDevice(res: any, body: CreateDeviceDTO, deviceid: any): Promise<any>;
    deleteDevice(res: any, deviceid: any): Promise<any>;
}
