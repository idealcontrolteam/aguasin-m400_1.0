"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const tag_service_1 = require("../tag/tag.service");
let DeviceService = class DeviceService {
    constructor(deviceModel, tagService) {
        this.deviceModel = deviceModel;
        this.tagService = tagService;
    }
    getDevices() {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceModel.find();
            return devices;
        });
    }
    getDevice(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const device = yield this.deviceModel.findById(id);
            return device;
        });
    }
    getDevicesAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceModel
                .find()
                .populate('modelId tcpConnectionId serialConnectionId gatewayId');
            return devices;
        });
    }
    getDevicesByModelId(modelId) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceModel.find({ modelId: modelId });
            return devices;
        });
    }
    getTagsByDeviceId(deviceId) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('[IN DEVICE SERVICE]', deviceId);
            const tags = yield this.tagService.getTagsByDeviceId(deviceId);
            return tags;
        });
    }
    getDevicesByTcpConnectionId(tcpConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceModel.find({
                tcpConnectionId: tcpConnectionId,
            });
            return devices;
        });
    }
    getDevicesBySerialConnectionId(serialConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceModel.find({
                serialConnectionId: serialConnectionId,
            });
            return devices;
        });
    }
    getDevicesByGatewayId(gatewayId) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceModel.find({
                gatewayId: gatewayId,
            });
            return devices;
        });
    }
    createDevice(createDeviceDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newDevice = new this.deviceModel(createDeviceDTO);
            return yield newDevice.save();
        });
    }
    deleteDevice(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.deviceModel.findByIdAndDelete(id);
        });
    }
    deleteDevicesByModelId(modelId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.deviceModel.deleteMany({ modelId: modelId });
        });
    }
    updateDevice(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.deviceModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
DeviceService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Device')),
    __metadata("design:paramtypes", [mongoose_1.Model,
        tag_service_1.TagService])
], DeviceService);
exports.DeviceService = DeviceService;
//# sourceMappingURL=device.service.js.map