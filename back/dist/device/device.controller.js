"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const device_dto_1 = require("./dto/device.dto");
const device_service_1 = require("./device.service");
let DeviceController = class DeviceController {
    constructor(deviceService) {
        this.deviceService = deviceService;
        this.validateIds = body => {
            Object.keys(body).map(key => {
                if (key != 'name' && key != 'active') {
                    if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
                        throw new common_1.BadRequestException(`${key} is not a valid ObjectId`);
                    }
                }
            });
        };
    }
    createDevice(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            this.validateIds(body);
            const newDevice = yield this.deviceService.createDevice(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Device created successfully',
                data: newDevice,
            });
        });
    }
    getDevices(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceService.getDevices();
            let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: devices,
                count: devices.length,
            });
        });
    }
    getDevicesAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.deviceService.getDevicesAll();
            let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: devices,
                count: devices.length,
            });
        });
    }
    getTagsByDeviceId(res, deviceid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!deviceid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Device id is not a valid ObjectId');
            }
            const tags = yield this.deviceService.getTagsByDeviceId(deviceid);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getDevice(res, deviceid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!deviceid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Device id is not a valid ObjectId');
            }
            const device = yield this.deviceService.getDevice(deviceid);
            if (!device) {
                throw new common_1.NotFoundException('Device not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Device found',
                data: device,
            });
        });
    }
    updateDevice(res, body, deviceid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!deviceid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Device id is not a valid ObjectId');
            }
            this.validateIds(body);
            const updatedDevice = yield this.deviceService.updateDevice(deviceid, body);
            if (!updatedDevice) {
                throw new common_1.NotFoundException('Device not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Device updated',
                data: updatedDevice,
            });
        });
    }
    deleteDevice(res, deviceid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!deviceid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Device id is not a valid ObjectId');
            }
            const deletedDevice = yield this.deviceService.deleteDevice(deviceid);
            if (!deletedDevice) {
                throw new common_1.NotFoundException('Device not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Device deleted',
                data: deletedDevice,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, device_dto_1.CreateDeviceDTO]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "createDevice", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "getDevices", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "getDevicesAll", null);
__decorate([
    common_1.Get('/:deviceid/tag'),
    __param(0, common_1.Res()), __param(1, common_1.Param('deviceid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "getTagsByDeviceId", null);
__decorate([
    common_1.Get('/:deviceid'),
    __param(0, common_1.Res()), __param(1, common_1.Param('deviceid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "getDevice", null);
__decorate([
    common_1.Put('/:deviceid'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('deviceid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, device_dto_1.CreateDeviceDTO, Object]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "updateDevice", null);
__decorate([
    common_1.Delete('/:deviceid'),
    __param(0, common_1.Res()), __param(1, common_1.Param('deviceid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], DeviceController.prototype, "deleteDevice", null);
DeviceController = __decorate([
    common_1.Controller('device'),
    __metadata("design:paramtypes", [device_service_1.DeviceService])
], DeviceController);
exports.DeviceController = DeviceController;
//# sourceMappingURL=device.controller.js.map