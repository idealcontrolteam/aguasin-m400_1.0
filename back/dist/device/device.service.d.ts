/// <reference types="mongodb" />
import { Model } from 'mongoose';
import { Device } from './interfaces/device.interface';
import { CreateDeviceDTO } from './dto/device.dto';
import { TagService } from '../tag/tag.service';
import { Tag } from '../tag/interfaces/tag.interface';
export declare class DeviceService {
    private deviceModel;
    private tagService;
    constructor(deviceModel: Model<Device>, tagService: TagService);
    getDevices(): Promise<Device[]>;
    getDevice(id: any): Promise<Device>;
    getDevicesAll(): Promise<Device[]>;
    getDevicesByModelId(modelId: any): Promise<Device[]>;
    getTagsByDeviceId(deviceId: any): Promise<Tag[]>;
    getDevicesByTcpConnectionId(tcpConnectionId: any): Promise<Device[]>;
    getDevicesBySerialConnectionId(serialConnectionId: any): Promise<Device[]>;
    getDevicesByGatewayId(gatewayId: any): Promise<Device[]>;
    createDevice(createDeviceDTO: CreateDeviceDTO): Promise<Device>;
    deleteDevice(id: any): Promise<Device>;
    deleteDevicesByModelId(modelId: any): Promise<{
        ok?: number;
        n?: number;
    }>;
    updateDevice(id: string, body: CreateDeviceDTO): Promise<Device>;
}
