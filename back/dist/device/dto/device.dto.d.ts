export declare class CreateDeviceDTO {
    name: string;
    active: boolean;
    modelId: string;
    tcpConnectionId?: string;
    serialConnectionId?: string;
    gatewayId: string;
}
