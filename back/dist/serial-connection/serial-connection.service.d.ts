import { Model } from 'mongoose';
import { SerialConnection } from './interfaces/serialConnection.interface';
import { CreateSerialConnectionDTO } from './dto/serialConnection.dto';
import { DeviceService } from '../device/device.service';
import { Device } from '../device/interfaces/device.interface';
export declare class SerialConnectionService {
    private serialConnectionModel;
    private deviceService;
    constructor(serialConnectionModel: Model<SerialConnection>, deviceService: DeviceService);
    getSerialConnections(): Promise<SerialConnection[]>;
    getDevicesBySerialConnectionId(serialConnectionId: any): Promise<Device[]>;
    getSerialConnection(id: any): Promise<SerialConnection>;
    createSerialConnection(createSerialConnectionDTO: CreateSerialConnectionDTO): Promise<SerialConnection>;
    deleteSerialConnection(id: any): Promise<SerialConnection>;
    updateSerialConnection(id: string, body: CreateSerialConnectionDTO): Promise<SerialConnection>;
}
