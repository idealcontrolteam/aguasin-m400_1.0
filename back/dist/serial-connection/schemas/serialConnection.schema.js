"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.SerialConnectionSchema = new mongoose_1.Schema({
    dataLength: String,
    parity: String,
    stopBits: String,
    baudRate: String,
    port: String,
}, { versionKey: false });
//# sourceMappingURL=serialConnection.schema.js.map