"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const serial_connection_controller_1 = require("./serial-connection.controller");
const serial_connection_service_1 = require("./serial-connection.service");
const serialConnection_schema_1 = require("./schemas/serialConnection.schema");
const mongoose_1 = require("@nestjs/mongoose");
const device_module_1 = require("../device/device.module");
const device_service_1 = require("../device/device.service");
let SerialConnectionModule = class SerialConnectionModule {
};
SerialConnectionModule = __decorate([
    common_1.Module({
        imports: [
            device_module_1.DeviceModule,
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'SerialConnection',
                    schema: serialConnection_schema_1.SerialConnectionSchema,
                    collection: 'serialConnection',
                },
            ]),
        ],
        controllers: [serial_connection_controller_1.SerialConnectionController],
        providers: [serial_connection_service_1.SerialConnectionService, device_service_1.DeviceService],
        exports: [serial_connection_service_1.SerialConnectionService],
    })
], SerialConnectionModule);
exports.SerialConnectionModule = SerialConnectionModule;
//# sourceMappingURL=serial-connection.module.js.map