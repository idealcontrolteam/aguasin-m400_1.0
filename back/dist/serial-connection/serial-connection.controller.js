"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const serialConnection_dto_1 = require("./dto/serialConnection.dto");
const serial_connection_service_1 = require("./serial-connection.service");
let SerialConnectionController = class SerialConnectionController {
    constructor(serialConnectionService) {
        this.serialConnectionService = serialConnectionService;
    }
    createSerialConnection(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newSerialConnection = yield this.serialConnectionService.createSerialConnection(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Serial Connection created successfully',
                data: newSerialConnection,
            });
        });
    }
    getSerialConnections(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const serialConnections = yield this.serialConnectionService.getSerialConnections();
            let msg = serialConnections.length == 0
                ? 'Serial connections not found'
                : 'Serial connections fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: serialConnections,
                count: serialConnections.length,
            });
        });
    }
    getDevicesBySerialConnectionId(res, serialConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            const devices = yield this.serialConnectionService.getDevicesBySerialConnectionId(serialConnectionId);
            let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: devices,
                count: devices.length,
            });
        });
    }
    getSerialConnection(res, serialConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!serialConnectionId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Serial connection is not a valid  ObjectId');
            }
            const serialConnection = yield this.serialConnectionService.getSerialConnection(serialConnectionId);
            if (!serialConnection) {
                throw new common_1.NotFoundException('Serial connection not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Serial connection found',
                data: serialConnection,
            });
        });
    }
    updateSerialConnection(res, body, serialConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!serialConnectionId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Serial connection id is not a valid ObjectId');
            }
            const updatedSerialConnection = yield this.serialConnectionService.updateSerialConnection(serialConnectionId, body);
            if (!updatedSerialConnection) {
                throw new common_1.NotFoundException('Serial connection not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Serial connection updated',
                data: updatedSerialConnection,
            });
        });
    }
    deleteSerialConnection(res, serialConnectionId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedSerialConnection = yield this.serialConnectionService.deleteSerialConnection(serialConnectionId);
            if (!deletedSerialConnection) {
                throw new common_1.NotFoundException('Serial Connection not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Serial Connection deleted',
                data: deletedSerialConnection,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, serialConnection_dto_1.CreateSerialConnectionDTO]),
    __metadata("design:returntype", Promise)
], SerialConnectionController.prototype, "createSerialConnection", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], SerialConnectionController.prototype, "getSerialConnections", null);
__decorate([
    common_1.Get('/:serialConnectionId/device'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('serialConnectionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SerialConnectionController.prototype, "getDevicesBySerialConnectionId", null);
__decorate([
    common_1.Get('/:serialConnectionId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('serialConnectionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SerialConnectionController.prototype, "getSerialConnection", null);
__decorate([
    common_1.Put('/:serialConnectionId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('serialConnectionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, serialConnection_dto_1.CreateSerialConnectionDTO, Object]),
    __metadata("design:returntype", Promise)
], SerialConnectionController.prototype, "updateSerialConnection", null);
__decorate([
    common_1.Delete('/:serialConnectionId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('serialConnectionId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], SerialConnectionController.prototype, "deleteSerialConnection", null);
SerialConnectionController = __decorate([
    common_1.Controller('serialConnection'),
    __metadata("design:paramtypes", [serial_connection_service_1.SerialConnectionService])
], SerialConnectionController);
exports.SerialConnectionController = SerialConnectionController;
//# sourceMappingURL=serial-connection.controller.js.map