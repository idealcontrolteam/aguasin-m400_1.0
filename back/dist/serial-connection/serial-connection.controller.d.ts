import { CreateSerialConnectionDTO } from './dto/serialConnection.dto';
import { SerialConnectionService } from './serial-connection.service';
export declare class SerialConnectionController {
    private serialConnectionService;
    constructor(serialConnectionService: SerialConnectionService);
    createSerialConnection(res: any, body: CreateSerialConnectionDTO): Promise<any>;
    getSerialConnections(res: any): Promise<any>;
    getDevicesBySerialConnectionId(res: any, serialConnectionId: any): Promise<any>;
    getSerialConnection(res: any, serialConnectionId: any): Promise<any>;
    updateSerialConnection(res: any, body: CreateSerialConnectionDTO, serialConnectionId: any): Promise<any>;
    deleteSerialConnection(res: any, serialConnectionId: any): Promise<any>;
}
