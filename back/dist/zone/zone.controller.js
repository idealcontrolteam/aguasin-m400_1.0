"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const zone_dto_1 = require("./dto/zone.dto");
const zone_service_1 = require("./zone.service");
let ZoneController = class ZoneController {
    constructor(zoneService) {
        this.zoneService = zoneService;
    }
    createZone(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!body.workPlaceId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Work place id is not a valid ObjectId');
            }
            const newZone = yield this.zoneService.createZone(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Zone created successfully',
                data: newZone,
            });
        });
    }
    getZones(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const zones = yield this.zoneService.getZones();
            let msg = zones.length == 0 ? 'Zones not found' : 'Zones fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: zones,
                count: zones.length,
            });
        });
    }
    getZonesAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const zones = yield this.zoneService.getZonesAll();
            let msg = zones.length == 0 ? 'Zones not found' : 'Zones fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: zones,
                count: zones.length,
            });
        });
    }
    getZoneWithTagsAndMeasurements(res, zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.zoneService.getZoneWithTagsAndMeasurements(zoneId);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getLocationsByZoneId(res, zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Zone id is not a valid ObjectId');
            }
            const locations = yield this.zoneService.getLocationsByZoneId(zoneId);
            let msg = locations.length == 0 ? 'Locations not found' : 'Locations fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: locations,
                count: locations.length,
            });
        });
    }
    getTagsByZoneId(res, zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Zone id is not a valid ObjectId');
            }
            const tags = yield this.zoneService.getTagsByZoneId(zoneId);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getZone(res, zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Zone id is not a valid ObjectId');
            }
            const zone = yield this.zoneService.getZone(zoneId);
            if (!zone) {
                throw new common_1.NotFoundException('Zone not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Zone found',
                data: zone,
            });
        });
    }
    updateZone(res, body, zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Zone id is not a valid ObjectId');
            }
            const updatedZone = yield this.zoneService.updateZone(zoneId, body);
            if (!updatedZone) {
                throw new common_1.NotFoundException('Zone not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Zone updated',
                data: updatedZone,
            });
        });
    }
    deleteZone(res, zoneId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedZone = yield this.zoneService.deleteZone(zoneId);
            if (!deletedZone) {
                throw new common_1.NotFoundException('Zone not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Zone deleted',
                data: deletedZone,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, zone_dto_1.CreateZoneDTO]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "createZone", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "getZones", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "getZonesAll", null);
__decorate([
    common_1.Get('/getZoneWithTagsAndMeasurements/:zoneId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('zoneId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "getZoneWithTagsAndMeasurements", null);
__decorate([
    common_1.Get('/:zoneId/location'),
    __param(0, common_1.Res()), __param(1, common_1.Param('zoneId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "getLocationsByZoneId", null);
__decorate([
    common_1.Get('/:zoneId/tag'),
    __param(0, common_1.Res()), __param(1, common_1.Param('zoneId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "getTagsByZoneId", null);
__decorate([
    common_1.Get('/:zoneId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('zoneId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "getZone", null);
__decorate([
    common_1.Put('/:zoneId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('zoneId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, zone_dto_1.CreateZoneDTO, Object]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "updateZone", null);
__decorate([
    common_1.Delete('/:zoneId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('zoneId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], ZoneController.prototype, "deleteZone", null);
ZoneController = __decorate([
    common_1.Controller('zone'),
    __metadata("design:paramtypes", [zone_service_1.ZoneService])
], ZoneController);
exports.ZoneController = ZoneController;
//# sourceMappingURL=zone.controller.js.map