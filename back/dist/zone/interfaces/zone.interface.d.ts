import { Document } from 'mongoose';
export interface Zone extends Document {
    name: string;
    active: boolean;
    workPlaceId: string;
}
