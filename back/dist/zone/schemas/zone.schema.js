"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.ZoneSchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
    workPlaceId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'WorkPlace',
        required: true,
    },
}, { versionKey: false });
//# sourceMappingURL=zone.schema.js.map