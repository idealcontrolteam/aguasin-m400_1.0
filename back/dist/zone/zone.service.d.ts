import { Model } from 'mongoose';
import { CreateZoneDTO } from './dto/zone.dto';
import { Zone } from './interfaces/zone.interface';
import { LocationService } from '../location/location.service';
import { Location } from '../location/interfaces/location.interface';
import { TagService } from '../tag/tag.service';
import { Tag } from '../tag/interfaces/tag.interface';
export declare class ZoneService {
    private zoneModel;
    private locationService;
    private tagService;
    constructor(zoneModel: Model<Zone>, locationService: LocationService, tagService: TagService);
    getZones(): Promise<Zone[]>;
    getZonesAll(): Promise<Zone[]>;
    getZoneWithTagsAndMeasurements(zoneId: any): Promise<Tag[]>;
    getLocationsByZoneId(zoneId: any): Promise<Location[]>;
    getZonesByWorkPlaceId(workPlaceId: any): Promise<Zone[]>;
    getZone(id: any): Promise<Zone>;
    getTagsByZoneId(zoneId: any): Promise<Tag[]>;
    createZone(createZoneDTO: CreateZoneDTO): Promise<Zone>;
    deleteZone(id: any): Promise<Zone>;
    updateZone(id: string, body: CreateZoneDTO): Promise<Zone>;
}
