import { CreateZoneDTO } from './dto/zone.dto';
import { ZoneService } from './zone.service';
export declare class ZoneController {
    private zoneService;
    constructor(zoneService: ZoneService);
    createZone(res: any, body: CreateZoneDTO): Promise<any>;
    getZones(res: any): Promise<any>;
    getZonesAll(res: any): Promise<any>;
    getZoneWithTagsAndMeasurements(res: any, zoneId: any): Promise<any>;
    getLocationsByZoneId(res: any, zoneId: any): Promise<any>;
    getTagsByZoneId(res: any, zoneId: any): Promise<any>;
    getZone(res: any, zoneId: any): Promise<any>;
    updateZone(res: any, body: CreateZoneDTO, zoneId: any): Promise<any>;
    deleteZone(res: any, zoneId: any): Promise<any>;
}
