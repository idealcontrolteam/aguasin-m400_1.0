"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.CategorySchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
}, { versionKey: false });
//# sourceMappingURL=category.schema.js.map