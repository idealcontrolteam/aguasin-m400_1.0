import { Model } from 'mongoose';
import { Category } from './interfaces/category.interface';
import { CreateCategoryDTO } from './dto/category.dto';
import { WorkPlaceService } from '../work-place/work-place.service';
import { WorkPlace } from '../work-place/interfaces/workPlace.interface';
export declare class CategoryService {
    private categoryModel;
    private workPlaceService;
    constructor(categoryModel: Model<Category>, workPlaceService: WorkPlaceService);
    getCategories(): Promise<Category[]>;
    getWorkPlacesByCategoryId(categoryId: any): Promise<WorkPlace[]>;
    getCategory(id: any): Promise<Category>;
    createCategory(createCategoryDTO: CreateCategoryDTO): Promise<Category>;
    deleteCategory(id: any): Promise<Category>;
    updateCategory(id: string, createCategoryDTO: CreateCategoryDTO): Promise<Category>;
}
