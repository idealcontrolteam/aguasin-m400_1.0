export declare class CreateCategoryDTO {
    name: string;
    active: boolean;
}
