"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const category_dto_1 = require("./dto/category.dto");
const category_service_1 = require("./category.service");
let CategoryController = class CategoryController {
    constructor(categoryService) {
        this.categoryService = categoryService;
    }
    createCategory(res, createCategoryDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newCategory = yield this.categoryService.createCategory(createCategoryDTO);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Category created successfully',
                data: newCategory,
            });
        });
    }
    getCategories(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const categories = yield this.categoryService.getCategories();
            let msg = categories.length == 0 ? 'Categories not found' : 'Categories fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: categories,
                count: categories.length,
            });
        });
    }
    getWorkPlacesByCategoryId(res, categoryId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Category id is not a valid ObjectId');
            }
            const workplaces = yield this.categoryService.getWorkPlacesByCategoryId(categoryId);
            let msg = workplaces.length == 0 ? 'Work places not found' : 'Work places fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: workplaces,
                count: workplaces.length,
            });
        });
    }
    getCategory(res, categoryId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Category id is not a valid ObjectId');
            }
            const category = yield this.categoryService.getCategory(categoryId);
            if (!category) {
                throw new common_1.NotFoundException('Category not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Category found',
                data: category,
            });
        });
    }
    updateCategory(res, body, categoryId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Category id is not a valid ObjectId');
            }
            const updatedCategory = yield this.categoryService.updateCategory(categoryId, body);
            if (!updatedCategory) {
                throw new common_1.NotFoundException('Category not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Category updated successfully',
                data: updatedCategory,
            });
        });
    }
    deleteCategory(res, categoryId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Category id is not a valid ObjectId');
            }
            const category = yield this.categoryService.deleteCategory(categoryId);
            if (!category) {
                throw new common_1.NotFoundException('Category not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Category Deleted',
                data: category,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, category_dto_1.CreateCategoryDTO]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "createCategory", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "getCategories", null);
__decorate([
    common_1.Get('/:categoryId/workPlace'),
    __param(0, common_1.Res()), __param(1, common_1.Param('categoryId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "getWorkPlacesByCategoryId", null);
__decorate([
    common_1.Get('/:categoryId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('categoryId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "getCategory", null);
__decorate([
    common_1.Put('/:categoryId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('categoryId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, category_dto_1.CreateCategoryDTO, Object]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "updateCategory", null);
__decorate([
    common_1.Delete('/:categoryId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('categoryId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], CategoryController.prototype, "deleteCategory", null);
CategoryController = __decorate([
    common_1.Controller('category'),
    __metadata("design:paramtypes", [category_service_1.CategoryService])
], CategoryController);
exports.CategoryController = CategoryController;
//# sourceMappingURL=category.controller.js.map