import { CreateCategoryDTO } from './dto/category.dto';
import { CategoryService } from './category.service';
export declare class CategoryController {
    private categoryService;
    constructor(categoryService: CategoryService);
    createCategory(res: any, createCategoryDTO: CreateCategoryDTO): Promise<any>;
    getCategories(res: any): Promise<any>;
    getWorkPlacesByCategoryId(res: any, categoryId: any): Promise<any>;
    getCategory(res: any, categoryId: any): Promise<any>;
    updateCategory(res: any, body: CreateCategoryDTO, categoryId: any): Promise<any>;
    deleteCategory(res: any, categoryId: any): Promise<any>;
}
