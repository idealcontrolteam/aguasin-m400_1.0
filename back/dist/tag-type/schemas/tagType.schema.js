"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.TagTypeSchema = new mongoose_1.Schema({
    name: String,
    active: Boolean,
}, { versionKey: false });
//# sourceMappingURL=tagType.schema.js.map