"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tagType_dto_1 = require("./dto/tagType.dto");
const tag_type_service_1 = require("./tag-type.service");
let TagTypeController = class TagTypeController {
    constructor(tagTypeService) {
        this.tagTypeService = tagTypeService;
    }
    createTagType(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newTagType = yield this.tagTypeService.createTagType(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Tag type created successfully',
                data: newTagType,
            });
        });
    }
    getTagType(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const tagtypes = yield this.tagTypeService.getTagTypes();
            let msg = tagtypes.length == 0 ? 'Tag types not found' : 'Tag types fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tagtypes,
                count: tagtypes.length,
            });
        });
    }
    getTagsByTagTypeId(res, tagTypeId) {
        return __awaiter(this, void 0, void 0, function* () {
            const tags = yield this.tagTypeService.getTagsByTagTypeId(tagTypeId);
            let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: tags,
                count: tags.length,
            });
        });
    }
    getTagTypeById(res, tagTypeId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagTypeId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag type id is not a valid ObjectId');
            }
            const tagType = yield this.tagTypeService.getTagType(tagTypeId);
            if (!tagType) {
                throw new common_1.NotFoundException('Tag type not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag type found',
                data: tagType,
            });
        });
    }
    updateTagType(res, body, tagTypeid) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagTypeid.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag type id is not a valid ObjectId');
            }
            const updatedTagType = yield this.tagTypeService.updateTagType(tagTypeid, body);
            if (!updatedTagType) {
                throw new common_1.NotFoundException('Tag type not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag type updated',
                data: updatedTagType,
            });
        });
    }
    deleteTagType(res, tagTypeid) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedTagType = yield this.tagTypeService.deleteTagType(tagTypeid);
            if (!deletedTagType) {
                throw new common_1.NotFoundException('Tag type not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Tag type deleted',
                data: deletedTagType,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, tagType_dto_1.CreateTagTypeDTO]),
    __metadata("design:returntype", Promise)
], TagTypeController.prototype, "createTagType", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TagTypeController.prototype, "getTagType", null);
__decorate([
    common_1.Get('/:tagTypeId/tag'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagTypeId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagTypeController.prototype, "getTagsByTagTypeId", null);
__decorate([
    common_1.Get('/:tagTypeId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagTypeId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagTypeController.prototype, "getTagTypeById", null);
__decorate([
    common_1.Put('/:tagTypeid'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('tagTypeid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, tagType_dto_1.CreateTagTypeDTO, Object]),
    __metadata("design:returntype", Promise)
], TagTypeController.prototype, "updateTagType", null);
__decorate([
    common_1.Delete('/:tagTypeid'),
    __param(0, common_1.Res()), __param(1, common_1.Param('tagTypeid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TagTypeController.prototype, "deleteTagType", null);
TagTypeController = __decorate([
    common_1.Controller('tagType'),
    __metadata("design:paramtypes", [tag_type_service_1.TagTypeService])
], TagTypeController);
exports.TagTypeController = TagTypeController;
//# sourceMappingURL=tag-type.controller.js.map