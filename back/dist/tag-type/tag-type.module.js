"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tag_type_controller_1 = require("./tag-type.controller");
const tag_type_service_1 = require("./tag-type.service");
const mongoose_1 = require("@nestjs/mongoose");
const tagType_schema_1 = require("./schemas/tagType.schema");
const tag_module_1 = require("../tag/tag.module");
const tag_service_1 = require("../tag/tag.service");
let TagTypeModule = class TagTypeModule {
};
TagTypeModule = __decorate([
    common_1.Module({
        imports: [
            tag_module_1.TagModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'TagType', schema: tagType_schema_1.TagTypeSchema, collection: 'tagType' },
            ]),
        ],
        controllers: [tag_type_controller_1.TagTypeController],
        providers: [tag_type_service_1.TagTypeService, tag_service_1.TagService],
    })
], TagTypeModule);
exports.TagTypeModule = TagTypeModule;
//# sourceMappingURL=tag-type.module.js.map