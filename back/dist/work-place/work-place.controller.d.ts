import { CreateWorkPlaceDTO } from './dto/workPlace.dto';
import { WorkPlaceService } from './work-place.service';
export declare class WorkPlaceController {
    private workPlaceService;
    constructor(workPlaceService: WorkPlaceService);
    validateIds: (body: any) => void;
    createWorkPlace(res: any, body: CreateWorkPlaceDTO): Promise<any>;
    getWorkPlaces(res: any): Promise<any>;
    getWorkPlacesAll(res: any): Promise<any>;
    getGateWaysByWorkPlaceId(res: any, idWorkPlace: any): Promise<any>;
    getZonesByWorkPlaceId(res: any, idWorkPlace: any): Promise<any>;
    getWorkPlace(res: any, idWorkPlace: any): Promise<any>;
    updateWorkPlace(res: any, body: CreateWorkPlaceDTO, idWorkPlace: any): Promise<any>;
    deleteWorkPlace(res: any, idWorkPlace: any): Promise<any>;
}
