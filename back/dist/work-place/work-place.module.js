"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const work_place_controller_1 = require("./work-place.controller");
const work_place_service_1 = require("./work-place.service");
const mongoose_1 = require("@nestjs/mongoose");
const workPlace_schema_1 = require("./schemas/workPlace.schema");
const gateway_module_1 = require("../gateway/gateway.module");
const gateway_service_1 = require("../gateway/gateway.service");
const zone_service_1 = require("../zone/zone.service");
const zone_module_1 = require("../zone/zone.module");
let WorkPlaceModule = class WorkPlaceModule {
};
WorkPlaceModule = __decorate([
    common_1.Module({
        imports: [
            gateway_module_1.GatewayModule,
            zone_module_1.ZoneModule,
            mongoose_1.MongooseModule.forFeature([
                { name: 'WorkPlace', schema: workPlace_schema_1.WorkPlaceSchema, collection: 'workPlace' },
            ]),
        ],
        controllers: [work_place_controller_1.WorkPlaceController],
        providers: [work_place_service_1.WorkPlaceService, gateway_service_1.GatewayService, zone_service_1.ZoneService],
        exports: [work_place_service_1.WorkPlaceService],
    })
], WorkPlaceModule);
exports.WorkPlaceModule = WorkPlaceModule;
//# sourceMappingURL=work-place.module.js.map