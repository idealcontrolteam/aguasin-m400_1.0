"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.WorkPlaceSchema = new mongoose_1.Schema({
    name: String,
    ubicacion: String,
    alertEmail: String,
    failEmail: String,
    active: Boolean,
    companyId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Company',
        required: true,
    },
    categoryId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Category',
        required: true,
    },
}, { versionKey: false });
//# sourceMappingURL=workPlace.schema.js.map