/// <reference types="mongodb" />
import { Model } from 'mongoose';
import { WorkPlace } from './interfaces/workPlace.interface';
import { CreateWorkPlaceDTO } from './dto/workPlace.dto';
import { GatewayService } from '../gateway/gateway.service';
import { Gateway } from '../gateway/interfaces/gateway.interface';
import { ZoneService } from '../zone/zone.service';
import { Zone } from '../zone/interfaces/zone.interface';
export declare class WorkPlaceService {
    private workPlaceModel;
    private gatewayService;
    private zoneService;
    constructor(workPlaceModel: Model<WorkPlace>, gatewayService: GatewayService, zoneService: ZoneService);
    getWorkPlaces(): Promise<WorkPlace[]>;
    getWorkPlacesAll(): Promise<WorkPlace[]>;
    getGateWaysByWorkPlaceId(workPlaceId: any): Promise<Gateway[]>;
    getZonesByWorkPlaceId(workPlaceId: any): Promise<Zone[]>;
    getWorkPlacesByCategoryId(categoryId: any): Promise<WorkPlace[]>;
    getWorkPlacesByCompanyId(companyId: any): Promise<WorkPlace[]>;
    getWorkPlace(id: any): Promise<WorkPlace>;
    createWorkPlace(createWorkPlaceDTO: CreateWorkPlaceDTO): Promise<WorkPlace>;
    deleteWorkPlace(id: any): Promise<WorkPlace>;
    deleteWorkPlacesByCompanyId(companyId: any): Promise<{
        ok?: number;
        n?: number;
    }>;
    updateWorkPlace(id: string, body: CreateWorkPlaceDTO): Promise<WorkPlace>;
}
