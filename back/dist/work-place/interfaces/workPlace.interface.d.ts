import { Document } from 'mongoose';
export interface WorkPlace extends Document {
    name: string;
    ubicacion: string;
    alertEmail: string;
    failEmail: string;
    active: boolean;
    companyId: string;
    categoryId: string;
}
