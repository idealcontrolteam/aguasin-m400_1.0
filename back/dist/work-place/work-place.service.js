"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
const gateway_service_1 = require("../gateway/gateway.service");
const zone_service_1 = require("../zone/zone.service");
let WorkPlaceService = class WorkPlaceService {
    constructor(workPlaceModel, gatewayService, zoneService) {
        this.workPlaceModel = workPlaceModel;
        this.gatewayService = gatewayService;
        this.zoneService = zoneService;
    }
    getWorkPlaces() {
        return __awaiter(this, void 0, void 0, function* () {
            const workPlaces = yield this.workPlaceModel.find();
            return workPlaces;
        });
    }
    getWorkPlacesAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const workPlaces = yield this.workPlaceModel
                .find()
                .populate('companyId categoryId');
            return workPlaces;
        });
    }
    getGateWaysByWorkPlaceId(workPlaceId) {
        return __awaiter(this, void 0, void 0, function* () {
            const gateways = yield this.gatewayService.getGateWaysByWorkPlaceId(workPlaceId);
            return gateways;
        });
    }
    getZonesByWorkPlaceId(workPlaceId) {
        return __awaiter(this, void 0, void 0, function* () {
            const zones = yield this.zoneService.getZonesByWorkPlaceId(workPlaceId);
            return zones;
        });
    }
    getWorkPlacesByCategoryId(categoryId) {
        return __awaiter(this, void 0, void 0, function* () {
            const workplaces = yield this.workPlaceModel.find({
                categoryId: categoryId,
            });
            return workplaces;
        });
    }
    getWorkPlacesByCompanyId(companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            const workplaces = yield this.workPlaceModel.find({
                companyId: companyId,
            });
            return workplaces;
        });
    }
    getWorkPlace(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const workPlace = yield this.workPlaceModel.findById(id);
            return workPlace;
        });
    }
    createWorkPlace(createWorkPlaceDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newWorkPlace = new this.workPlaceModel(createWorkPlaceDTO);
            return yield newWorkPlace.save();
        });
    }
    deleteWorkPlace(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.workPlaceModel.findByIdAndDelete(id);
        });
    }
    deleteWorkPlacesByCompanyId(companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.workPlaceModel.deleteMany({ companyId: companyId });
        });
    }
    updateWorkPlace(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.workPlaceModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
};
WorkPlaceService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('WorkPlace')),
    __metadata("design:paramtypes", [mongoose_1.Model,
        gateway_service_1.GatewayService,
        zone_service_1.ZoneService])
], WorkPlaceService);
exports.WorkPlaceService = WorkPlaceService;
//# sourceMappingURL=work-place.service.js.map