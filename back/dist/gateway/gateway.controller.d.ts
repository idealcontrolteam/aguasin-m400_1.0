import { CreateGatewayDTO } from './dto/gateway.dto';
import { GatewayService } from './gateway.service';
export declare class GatewayController {
    private gatewayService;
    constructor(gatewayService: GatewayService);
    createGateway(res: any, body: CreateGatewayDTO): Promise<any>;
    getGateways(res: any): Promise<any>;
    getGatewaysAll(res: any): Promise<any>;
    getDevicesByGatewayId(res: any, gatewayId: any): Promise<any>;
    getGateway(res: any, gatewayId: any): Promise<any>;
    updateGateway(res: any, body: CreateGatewayDTO, gatewayId: any): Promise<any>;
    deleteGateway(res: any, gatewayId: any): Promise<any>;
}
