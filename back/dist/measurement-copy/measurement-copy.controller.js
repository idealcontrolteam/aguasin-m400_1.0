"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurementCopy_dto_1 = require("./dto/measurementCopy.dto");
const measurement_copy_service_1 = require("./measurement-copy.service");
let MeasurementCopyController = class MeasurementCopyController {
    constructor(measurementService) {
        this.measurementService = measurementService;
        this.validateIds = body => {
            Object.keys(body).map(key => {
                if (key != 'value' && key != 'dateTime' && key != 'active') {
                    if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
                        throw new common_1.BadRequestException(`${key} is not a valid ObjectId`);
                    }
                }
            });
        };
    }
    createMeasurement(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            this.validateIds(body);
            const newMeasurement = yield this.measurementService.createMeasurement(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Measurement created successfully',
                data: newMeasurement,
            });
        });
    }
    getMeasurements(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurements();
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const measurements = yield this.measurementService.getMeasurementsAll();
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurement(res, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Measurement id is not a valid ObjectId');
            }
            const measurement = yield this.measurementService.getMeasurement(measurementId);
            if (!measurement) {
                throw new common_1.NotFoundException('Measurement not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement found',
                data: measurement,
            });
        });
    }
    getMeasurementByTagfiltered(res, tagId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Tag id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByTagfiltered(tagId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementBySensorfiltered(res, sensorId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Sensor id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementBySensorfiltered(sensorId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    getMeasurementByLocationfiltered(res, locationId, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Location id is not a valid ObjectId');
            }
            const measurements = yield this.measurementService.getMeasurementByLocationfiltered(locationId, fini, ffin);
            let msg = measurements.length == 0
                ? 'Measurements not found'
                : 'Measurements fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: measurements,
                count: measurements.length,
            });
        });
    }
    updateMeasurement(res, body, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Measurement id is not a valid ObjectId');
            }
            this.validateIds(body);
            const updatedMeasurement = yield this.measurementService.updateMeasurement(measurementId, body);
            if (!updatedMeasurement) {
                throw new common_1.NotFoundException('Measurement not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement updated',
                data: updatedMeasurement,
            });
        });
    }
    deleteMeasurement(res, measurementId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedMeasurement = yield this.measurementService.deleteMeasurement(measurementId);
            if (!deletedMeasurement) {
                throw new common_1.NotFoundException('Measurement not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Measurement deleted',
                data: deletedMeasurement,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, measurementCopy_dto_1.CreateMeasurementCopyDTO]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "createMeasurement", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "getMeasurements", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "getMeasurementsAll", null);
__decorate([
    common_1.Get('/:measurementId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "getMeasurement", null);
__decorate([
    common_1.Get('/tag/:tagId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('tagId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "getMeasurementByTagfiltered", null);
__decorate([
    common_1.Get('/sensor/:sensorId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('sensorId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "getMeasurementBySensorfiltered", null);
__decorate([
    common_1.Get('/location/:locationId/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('locationId')),
    __param(2, common_1.Param('fini')),
    __param(3, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "getMeasurementByLocationfiltered", null);
__decorate([
    common_1.Put('/:measurementId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, measurementCopy_dto_1.CreateMeasurementCopyDTO, Object]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "updateMeasurement", null);
__decorate([
    common_1.Delete('/:measurementId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('measurementId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], MeasurementCopyController.prototype, "deleteMeasurement", null);
MeasurementCopyController = __decorate([
    common_1.Controller('measurementCopy'),
    __metadata("design:paramtypes", [measurement_copy_service_1.MeasurementCopyService])
], MeasurementCopyController);
exports.MeasurementCopyController = MeasurementCopyController;
//# sourceMappingURL=measurement-copy.controller.js.map