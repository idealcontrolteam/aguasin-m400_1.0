import { CreateMeasurementCopyDTO } from './dto/measurementCopy.dto';
import { MeasurementCopyService } from './measurement-copy.service';
export declare class MeasurementCopyController {
    private measurementService;
    constructor(measurementService: MeasurementCopyService);
    validateIds: (body: any) => void;
    createMeasurement(res: any, body: CreateMeasurementCopyDTO): Promise<any>;
    getMeasurements(res: any): Promise<any>;
    getMeasurementsAll(res: any): Promise<any>;
    getMeasurement(res: any, measurementId: any): Promise<any>;
    getMeasurementByTagfiltered(res: any, tagId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementBySensorfiltered(res: any, sensorId: any, fini: any, ffin: any): Promise<any>;
    getMeasurementByLocationfiltered(res: any, locationId: any, fini: any, ffin: any): Promise<any>;
    updateMeasurement(res: any, body: CreateMeasurementCopyDTO, measurementId: any): Promise<any>;
    deleteMeasurement(res: any, measurementId: any): Promise<any>;
}
