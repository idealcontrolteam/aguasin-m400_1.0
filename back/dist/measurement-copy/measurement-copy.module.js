"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const measurement_copy_controller_1 = require("./measurement-copy.controller");
const measurement_copy_service_1 = require("./measurement-copy.service");
const mongoose_1 = require("@nestjs/mongoose");
const measurementCopy_schema_1 = require("./schemas/measurementCopy.schema");
let MeasurementCopyModule = class MeasurementCopyModule {
};
MeasurementCopyModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                {
                    name: 'MeasurementCopy',
                    schema: measurementCopy_schema_1.MeasurementCopySchema,
                    collection: 'measurementCopy',
                },
            ]),
        ],
        controllers: [measurement_copy_controller_1.MeasurementCopyController],
        providers: [measurement_copy_service_1.MeasurementCopyService],
    })
], MeasurementCopyModule);
exports.MeasurementCopyModule = MeasurementCopyModule;
//# sourceMappingURL=measurement-copy.module.js.map