"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let AlarmService = class AlarmService {
    constructor(alarmModel) {
        this.alarmModel = alarmModel;
    }
    getAlarms() {
        return __awaiter(this, void 0, void 0, function* () {
            const alarms = yield this.alarmModel.find();
            return alarms;
        });
    }
    getAlarmsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const alarms = yield this.alarmModel.find().populate('tagId');
            return alarms;
        });
    }
    getAlarmsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const alarms = yield this.alarmModel.find({ tagId: tagId });
            return alarms;
        });
    }
    getAlarm(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const alarm = yield this.alarmModel.findById(id);
            return alarm;
        });
    }
    createAlarm(createAlarmDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newAlarm = new this.alarmModel(createAlarmDTO);
            return yield newAlarm.save();
        });
    }
    deleteAlarm(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.alarmModel.findByIdAndDelete(id);
        });
    }
    updateAlarm(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.alarmModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
    getAlarmByfilteredDate(fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const alarms = yield this.alarmModel.find({
                dateTimeIni: { $gte: fini, $lte: ffin },
            });
            return alarms;
        });
    }
};
AlarmService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Alarm')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], AlarmService);
exports.AlarmService = AlarmService;
//# sourceMappingURL=alarm.service.js.map