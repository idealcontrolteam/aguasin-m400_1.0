"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const alarm_service_1 = require("./alarm.service");
const alarm_dto_1 = require("./dto/alarm.dto");
let AlarmController = class AlarmController {
    constructor(alarmService) {
        this.alarmService = alarmService;
    }
    createAlarm(res, body) {
        return __awaiter(this, void 0, void 0, function* () {
            const newAlarm = yield this.alarmService.createAlarm(body);
            return res.status(common_1.HttpStatus.CREATED).json({
                statusCode: common_1.HttpStatus.CREATED,
                message: 'Alarm created successfully',
                data: newAlarm,
            });
        });
    }
    getAlarms(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const alarms = yield this.alarmService.getAlarms();
            let msg = alarms.length == 0 ? 'Alarms not found' : 'Alarms fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: alarms,
                count: alarms.length,
            });
        });
    }
    getAlarmByfilteredDate(res, fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const alarms = yield this.alarmService.getAlarmByfilteredDate(fini, ffin);
            let msg = alarms.length == 0
                ? 'alarms not found'
                : 'alarms fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: alarms,
                count: alarms.length,
            });
        });
    }
    getAlarmsAll(res) {
        return __awaiter(this, void 0, void 0, function* () {
            const alarms = yield this.alarmService.getAlarmsAll();
            let msg = alarms.length == 0 ? 'Alarms not found' : 'Alarms fetched';
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: msg,
                data: alarms,
                count: alarms.length,
            });
        });
    }
    getAlarm(res, alarmId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!alarmId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Alarm id is not a valid  ObjectId');
            }
            const alarm = yield this.alarmService.getAlarm(alarmId);
            if (!alarm) {
                throw new common_1.NotFoundException('Alarm not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Alarm found',
                data: alarm,
            });
        });
    }
    updateAlarm(res, body, alarmId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!alarmId.match(/^[0-9a-fA-F]{24}$/)) {
                throw new common_1.BadRequestException('Alarm id is not a valid  ObjectId');
            }
            const updatedAlarm = yield this.alarmService.updateAlarm(alarmId, body);
            if (!updatedAlarm) {
                throw new common_1.NotFoundException('Alarm not updated');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Alarm updated',
                data: updatedAlarm,
            });
        });
    }
    deleteAlarm(res, alarmId) {
        return __awaiter(this, void 0, void 0, function* () {
            const deletedAlarm = yield this.alarmService.deleteAlarm(alarmId);
            if (!deletedAlarm) {
                throw new common_1.NotFoundException('Alarm not found');
            }
            return res.status(common_1.HttpStatus.OK).json({
                statusCode: common_1.HttpStatus.OK,
                message: 'Alarm deleted',
                data: deletedAlarm,
            });
        });
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, alarm_dto_1.CreateAlarmDTO]),
    __metadata("design:returntype", Promise)
], AlarmController.prototype, "createAlarm", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AlarmController.prototype, "getAlarms", null);
__decorate([
    common_1.Get('/:fini/:ffin'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('fini')),
    __param(2, common_1.Param('ffin')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AlarmController.prototype, "getAlarmByfilteredDate", null);
__decorate([
    common_1.Get('/all'),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AlarmController.prototype, "getAlarmsAll", null);
__decorate([
    common_1.Get('/:alarmId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Param('alarmId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AlarmController.prototype, "getAlarm", null);
__decorate([
    common_1.Put('/:alarmId'),
    __param(0, common_1.Res()),
    __param(1, common_1.Body()),
    __param(2, common_1.Param('alarmId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, alarm_dto_1.CreateAlarmDTO, Object]),
    __metadata("design:returntype", Promise)
], AlarmController.prototype, "updateAlarm", null);
__decorate([
    common_1.Delete('/:alarmId'),
    __param(0, common_1.Res()), __param(1, common_1.Param('alarmId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AlarmController.prototype, "deleteAlarm", null);
AlarmController = __decorate([
    common_1.Controller('alarm'),
    __metadata("design:paramtypes", [alarm_service_1.AlarmService])
], AlarmController);
exports.AlarmController = AlarmController;
//# sourceMappingURL=alarm.controller.js.map