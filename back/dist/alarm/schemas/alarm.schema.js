"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.AlarmSchema = new mongoose_1.Schema({
    dateTimeIni: Date,
    dateTimeTer: Date,
    description: String,
    tagId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Tag',
        required: true,
    },
    active: Boolean,
    presentValue: Number,
    alarmValue: Number,
    sentEmail: Boolean,
    accepted: Boolean,
}, { versionKey: false });
//# sourceMappingURL=alarm.schema.js.map