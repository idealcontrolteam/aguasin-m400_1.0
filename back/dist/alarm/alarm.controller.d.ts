import { AlarmService } from './alarm.service';
import { CreateAlarmDTO } from './dto/alarm.dto';
export declare class AlarmController {
    private alarmService;
    constructor(alarmService: AlarmService);
    createAlarm(res: any, body: CreateAlarmDTO): Promise<any>;
    getAlarms(res: any): Promise<any>;
    getAlarmByfilteredDate(res: any, fini: any, ffin: any): Promise<any>;
    getAlarmsAll(res: any): Promise<any>;
    getAlarm(res: any, alarmId: any): Promise<any>;
    updateAlarm(res: any, body: CreateAlarmDTO, alarmId: any): Promise<any>;
    deleteAlarm(res: any, alarmId: any): Promise<any>;
}
