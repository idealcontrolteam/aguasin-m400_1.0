import { Model } from 'mongoose';
import { CreateConfigControlDTO } from './dto/TagConfigControl.dto';
import { TagConfigControl } from './interfaces/TagConfigControl.interface';
export declare class TagConfigControlService {
    private tagConfigControlModel;
    constructor(tagConfigControlModel: Model<TagConfigControl>);
    getTagConfigControls(): Promise<TagConfigControl[]>;
    getTagConfigControlsAll(): Promise<TagConfigControl[]>;
    geTagConfigControlsByTagId(tagId: any): Promise<TagConfigControl[]>;
    getTagConfigControl(id: any): Promise<TagConfigControl>;
    createTagConfigControl(createConfigControlDTO: CreateConfigControlDTO): Promise<TagConfigControl>;
    deleteTagConfigControl(id: any): Promise<TagConfigControl>;
    updateTagConfigControl(id: string, body: CreateConfigControlDTO): Promise<TagConfigControl>;
}
