import { Document } from 'mongoose';
export interface TagConfigControl extends Document {
    name: string;
    setpoint: number;
    histeresis: number;
    timeOn: number;
    timeOff: number;
    active: boolean;
    tagId: string;
}
