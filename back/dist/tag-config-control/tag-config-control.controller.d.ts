import { TagConfigControlService } from './tag-config-control.service';
import { CreateConfigControlDTO } from './dto/TagConfigControl.dto';
export declare class TagConfigControlController {
    private tagConfigService;
    constructor(tagConfigService: TagConfigControlService);
    createTagConfigControl(res: any, body: CreateConfigControlDTO): Promise<any>;
    getTagConfigControls(res: any): Promise<any>;
    getTagConfigControlsAll(res: any): Promise<any>;
    getTagConfigControl(res: any, tagConfigControlId: any): Promise<any>;
    updateTagConfigControl(res: any, body: CreateConfigControlDTO, tagConfigControlId: any): Promise<any>;
    deleteTagConfigControl(res: any, tagConfigControlId: any): Promise<any>;
}
