"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.TagConfigControlSchema = new mongoose_1.Schema({
    name: String,
    setpoint: Number,
    histeresis: Number,
    timeOn: Number,
    timeOff: Number,
    active: Boolean,
    tagId: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Tag',
        required: false,
    },
}, { versionKey: false });
//# sourceMappingURL=TagConfigControl.schema.js.map