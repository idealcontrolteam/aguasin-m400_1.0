"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const fail_controller_1 = require("./fail.controller");
const fail_service_1 = require("./fail.service");
const mongoose_1 = require("@nestjs/mongoose");
const fail_schema_1 = require("./schemas/fail.schema");
let FailModule = class FailModule {
};
FailModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'Fail', schema: fail_schema_1.FailSchema, collection: 'fail' },
            ]),
        ],
        controllers: [fail_controller_1.FailController],
        providers: [fail_service_1.FailService],
        exports: [fail_service_1.FailService],
    })
], FailModule);
exports.FailModule = FailModule;
//# sourceMappingURL=fail.module.js.map