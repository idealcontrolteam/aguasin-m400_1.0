"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let FailService = class FailService {
    constructor(failModel) {
        this.failModel = failModel;
    }
    getFails() {
        return __awaiter(this, void 0, void 0, function* () {
            const fails = yield this.failModel.find();
            return fails;
        });
    }
    getFailsAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const fails = yield this.failModel.find().populate('tagId');
            return fails;
        });
    }
    getFailsByTagId(tagId) {
        return __awaiter(this, void 0, void 0, function* () {
            const fails = yield this.failModel.find({ tagId: tagId });
            return fails;
        });
    }
    getFail(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const fail = yield this.failModel.findById(id);
            return fail;
        });
    }
    createFail(createFailDTO) {
        return __awaiter(this, void 0, void 0, function* () {
            const newFail = new this.failModel(createFailDTO);
            return yield newFail.save();
        });
    }
    deleteFail(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.failModel.findByIdAndDelete(id);
        });
    }
    updateFail(id, body) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.failModel.findByIdAndUpdate(id, body, {
                new: true,
            });
        });
    }
    getFailByfilteredDate(fini, ffin) {
        return __awaiter(this, void 0, void 0, function* () {
            const fails = yield this.failModel.find({
                dateTimeIni: { $gte: fini, $lte: ffin },
            });
            return fails;
        });
    }
};
FailService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Fail')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], FailService);
exports.FailService = FailService;
//# sourceMappingURL=fail.service.js.map