import { Model } from 'mongoose';
import { CreateFailDTO } from './dto/fail.dto';
import { Fail } from './interfaces/fail.interface';
export declare class FailService {
    private failModel;
    constructor(failModel: Model<Fail>);
    getFails(): Promise<Fail[]>;
    getFailsAll(): Promise<Fail[]>;
    getFailsByTagId(tagId: any): Promise<Fail[]>;
    getFail(id: any): Promise<Fail>;
    createFail(createFailDTO: CreateFailDTO): Promise<Fail>;
    deleteFail(id: any): Promise<Fail>;
    updateFail(id: string, body: CreateFailDTO): Promise<Fail>;
    getFailByfilteredDate(fini: any, ffin: any): Promise<Fail[]>;
}
