import { Schema } from 'mongoose';

export const UserSchema = new Schema(
  {
    name: String,
    email: String,
    username: String,
    password: String,
    roleId: {
      type: Schema.Types.ObjectId,
      ref: 'Role',
      required: true,
    },
    companyId: {
      type: Schema.Types.ObjectId,
      ref: 'Company',
      required: true,
    },
    active: Boolean,
    token: String,
    lastConnection: {
      type: Date,
      default: null,
    },
  },
  { versionKey: false },
);
