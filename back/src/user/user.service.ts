import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDTO } from './dto/user.dto';
import { User } from './interfaces/user.interface';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private userModel: Model<User>) {}

  async getUsers(): Promise<User[]> {
    const users = await this.userModel.find();
    return users;
  }

  async getUsersByRoleId(roleId): Promise<User[]> {
    const users = await this.userModel.find({ roleId: roleId });
    return users;
  }

  async getUsersAll(): Promise<User[]> {
    const users = await this.userModel.find().populate('companyId roleId');
    return users;
  }

  async getUsersByCompanyId(companyId): Promise<User[]> {
    const users = await this.userModel.find({ companyId: companyId });
    return users;
  }

  async getUser(id): Promise<User> {
    const user = await this.userModel.findById(id);
    return user;
  }

  async checkToken(u): Promise<User> {
    const { email } = u;
    const user = await this.userModel.findOne({email});
    return user;
  }

  hashPassword = async password => {
    const hashedPassword = await bcrypt.hash(password, 10);
    return hashedPassword;
  };

  async createUser(createUserDTO: CreateUserDTO): Promise<User> {
    createUserDTO.password = await this.hashPassword(createUserDTO.password);
    const newUser = new this.userModel(createUserDTO);
    return await newUser.save();
  }

  async login(user): Promise<User> {
    const { email } = user;
    const userDB = await this.userModel.findOne({ email });
    return userDB;
  }

  async deleteUser(id): Promise<User> {
    return await this.userModel.findByIdAndDelete(id);
  }

  async deleteUsersByCompanyId(companyId) {
    return await this.userModel.deleteMany({ companyId: companyId });
  }

  async updateUser(id: string, body: CreateUserDTO): Promise<User> {
    return await this.userModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
  async updateUserToken(id: string, body: any): Promise<User> {
    return await this.userModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
