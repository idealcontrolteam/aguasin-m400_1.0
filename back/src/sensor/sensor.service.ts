import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateSensorDTO } from './dto/sensor.dto';
import { Sensor } from './interfaces/sensor.interface';
import { Measurement } from '../measurement/interfaces/measurement.interface';
import { MeasurementService } from '../measurement/measurement.service';
import { TagService } from '../tag/tag.service';
import { Tag } from '../tag/interfaces/tag.interface';

@Injectable()
export class SensorService {
  constructor(
    @InjectModel('Sensor') private sensorModel: Model<Sensor>,
    private measurementService: MeasurementService,
    private tagService: TagService,
  ) {}

  async getSensors(): Promise<Sensor[]> {
    const sensors = await this.sensorModel.find();
    return sensors;
  }

  async getSensorsAll(): Promise<Sensor[]> {
    const sensors = await this.sensorModel.find().populate('locationId');
    return sensors;
  }

  async getSensorsByLocationId(locationId): Promise<Sensor[]> {
    const sensors = await this.sensorModel.find({ locationId: locationId });
    return sensors;
  }

  async getMeasurementsBySensorId(sensorId): Promise<Measurement[]> {
    const measurement = await this.measurementService.getMeasurementsBySensorId(
      sensorId,
    );
    return measurement;
  }

  async getTagsBySensorId(sensorId): Promise<Tag[]> {
    const tags = await this.tagService.getTagsBySensorId(sensorId);
    return tags;
  }

  async getSensor(id): Promise<Sensor> {
    const sensor = await this.sensorModel.findById(id);
    return sensor;
  }

  async createSensor(createSensorDTO: CreateSensorDTO): Promise<Sensor> {
    const newSensor = new this.sensorModel(createSensorDTO);
    return await newSensor.save();
  }

  async deleteSensor(id): Promise<Sensor> {
    return await this.sensorModel.findByIdAndDelete(id);
  }

  async updateSensor(id: string, body: CreateSensorDTO): Promise<Sensor> {
    return await this.sensorModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
