import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { TagConfigControlService } from './tag-config-control.service';
import { CreateConfigControlDTO } from './dto/TagConfigControl.dto';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('tagConfigControl')
//@UseGuards(new AuthGuard())
export class TagConfigControlController {
  constructor(private tagConfigService: TagConfigControlService) {}

  @Post()
  async createTagConfigControl(
    @Res() res,
    @Body() body: CreateConfigControlDTO,
  ) {
    const newTagConfigControl = await this.tagConfigService.createTagConfigControl(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Tag config control created successfully',
      data: newTagConfigControl,
    });
  }

  @Get()
  async getTagConfigControls(@Res() res) {
    const tagConfigControls = await this.tagConfigService.getTagConfigControls();

    let msg =
      tagConfigControls.length == 0
        ? 'Tag config control not found'
        : 'Tag config control fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tagConfigControls,
      count: tagConfigControls.length,
    });
  }

  @Get('/all')
  async getTagConfigControlsAll(@Res() res) {
    const tagConfigControls = await this.tagConfigService.getTagConfigControlsAll();

    let msg =
      tagConfigControls.length == 0
        ? 'Tag config control not found'
        : 'Tag config control fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tagConfigControls,
      count: tagConfigControls.length,
    });
  }

  @Get('/:tagConfigControlId')
  async getTagConfigControl(
    @Res() res,
    @Param('tagConfigControlId') tagConfigControlId,
  ) {
    if (!tagConfigControlId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException(
        'Tag config control id is not a valid ObjectId',
      );
    }

    const tagConfigControl = await this.tagConfigService.getTagConfigControl(
      tagConfigControlId,
    );
    if (!tagConfigControl) {
      throw new NotFoundException('Tag config control not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag config control found',
      data: tagConfigControl,
    });
  }

  @Put('/:roleId')
  async updateTagConfigControl(
    @Res() res,
    @Body() body: CreateConfigControlDTO,
    @Param('tagConfigControlId') tagConfigControlId,
  ) {
    if (!tagConfigControlId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException(
        'Tag config control id is not a valid ObjectId',
      );
    }
    const updatedTagConfigControl = await this.tagConfigService.updateTagConfigControl(
      tagConfigControlId,
      body,
    );
    if (!updatedTagConfigControl) {
      throw new NotFoundException('Tag config control not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag config control updated',
      data: updatedTagConfigControl,
    });
  }

  @Delete('/:tagConfigControlId')
  async deleteTagConfigControl(
    @Res() res,
    @Param('tagConfigControlId') tagConfigControlId,
  ) {
    const deletedTagConfigControl = await this.tagConfigService.deleteTagConfigControl(
      tagConfigControlId,
    );

    if (!deletedTagConfigControl) {
      throw new NotFoundException('Tag config control not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag config control deleted',
      data: deletedTagConfigControl,
    });
  }
}
