import { Schema } from 'mongoose';

export const TagConfigControlSchema = new Schema(
  {
    name: String,
    setpoint: Number,
    histeresis: Number,
    timeOn: Number,
    timeOff: Number,
    active: Boolean,
    tagId: {
      type: Schema.Types.ObjectId,
      ref: 'Tag',
      required: false,
    },
  },
  { versionKey: false },
);
