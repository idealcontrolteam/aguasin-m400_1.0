export class CreateConfigControlDTO {
  name: string;
  setpoint: number;
  histeresis: number;
  timeOn: number;
  timeOff: number;
  active: boolean;
  tagId: string;
}
