import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Gateway } from './interfaces/gateway.interface';
import { CreateGatewayDTO } from './dto/gateway.dto';
import { DeviceService } from '../device/device.service';
import { Device } from '../device/interfaces/device.interface';

@Injectable()
export class GatewayService {
  constructor(
    @InjectModel('Gateway') private gatewayModel: Model<Gateway>,
    private deviceService: DeviceService,
  ) {}

  async getGateways(): Promise<Gateway[]> {
    const gateways = await this.gatewayModel.find();
    return gateways;
  }

  async getGateway(id): Promise<Gateway> {
    const gateway = await this.gatewayModel.findById(id);
    return gateway;
  }

  async getGatewaysAll(): Promise<Gateway[]> {
    const gateways = await this.gatewayModel.find().populate('workPlaceId');
    return gateways;
  }

  async getGateWaysByWorkPlaceId(workPlaceId): Promise<Gateway[]> {
    const gateways = await this.gatewayModel.find({ workPlaceId: workPlaceId });
    return gateways;
  }

  async createGateway(createGatewayDTO: CreateGatewayDTO): Promise<Gateway> {
    const newGateway = new this.gatewayModel(createGatewayDTO);
    return await newGateway.save();
  }

  async getDevicesByGatewayId(gatewayId): Promise<Device[]> {
    const devices = await this.deviceService.getDevicesByGatewayId(gatewayId);
    return devices;
  }

  async deleteGateway(id): Promise<Gateway> {
    return await this.gatewayModel.findByIdAndDelete(id);
  }

  async deleteGatewaysByWorkPlaceId(workPlaceId) {
    return await this.gatewayModel.deleteMany({ workPlaceId: workPlaceId });
  }

  async updateGateway(id: string, body: CreateGatewayDTO): Promise<Gateway> {
    return await this.gatewayModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
