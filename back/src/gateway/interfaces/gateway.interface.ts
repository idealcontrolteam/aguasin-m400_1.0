import { Document } from 'mongoose';

export interface Gateway extends Document {
  name: string;
  description: string;
  active: boolean;
  workPlaceId: string;
}
