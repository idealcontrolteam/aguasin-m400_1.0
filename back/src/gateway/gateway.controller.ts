import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateGatewayDTO } from './dto/gateway.dto';
import { GatewayService } from './gateway.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('gateway')
//@UseGuards(new AuthGuard())
export class GatewayController {
  constructor(private gatewayService: GatewayService) {}

  @Post()
  async createGateway(@Res() res, @Body() body: CreateGatewayDTO) {
    if (
      body.workPlaceId !== undefined &&
      !body.workPlaceId.match(/^[0-9a-fA-F]{24}$/)
    ) {
      throw new BadRequestException('Work place id is not a valid ObjectId');
    }

    const newGateway = await this.gatewayService.createGateway(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Gateway created successfully',
      data: newGateway,
    });
  }

  @Get()
  async getGateways(@Res() res) {
    const gateways = await this.gatewayService.getGateways();

    let msg = gateways.length == 0 ? 'Gateways not found' : 'Gateways fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: gateways,
      count: gateways.length,
    });
  }

  @Get('/all')
  async getGatewaysAll(@Res() res) {
    const gateways = await this.gatewayService.getGatewaysAll();

    let msg = gateways.length == 0 ? 'Gateways not found' : 'Gateways fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: gateways,
      count: gateways.length,
    });
  }

  @Get(':gatewayId/device')
  async getDevicesByGatewayId(@Res() res, @Param('gatewayId') gatewayId) {
    const devices = await this.gatewayService.getDevicesByGatewayId(gatewayId);

    let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: devices,
      count: devices.length,
    });
  }

  @Get('/:gatewayId')
  async getGateway(@Res() res, @Param('gatewayId') gatewayId) {
    if (!gatewayId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Gateway id is not a valid ObjectId');
    }

    const gateway = await this.gatewayService.getGateway(gatewayId);
    if (!gateway) {
      throw new NotFoundException('Gateway not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Gateway found',
      data: gateway,
    });
  }

  @Put('/:gatewayId')
  async updateGateway(
    @Res() res,
    @Body() body: CreateGatewayDTO,
    @Param('gatewayId') gatewayId,
  ) {
    if (!gatewayId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Gateway id is not a valid ObjectId');
    }
    if (
      body.workPlaceId !== undefined &&
      !body.workPlaceId.match(/^[0-9a-fA-F]{24}$/)
    ) {
      throw new BadRequestException('Work place id is not a valid ObjectId');
    }

    const updatedGateway = await this.gatewayService.updateGateway(
      gatewayId,
      body,
    );
    if (!updatedGateway) {
      throw new NotFoundException('Gateway not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Gateway updated',
      data: updatedGateway,
    });
  }

  @Delete('/:gatewayId')
  async deleteGateway(@Res() res, @Param('gatewayId') gatewayId) {
    const deletedGateway = await this.gatewayService.deleteGateway(gatewayId);

    if (!deletedGateway) {
      throw new NotFoundException('Gateway not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Gateway deleted',
      data: deletedGateway,
    });
  }
}
