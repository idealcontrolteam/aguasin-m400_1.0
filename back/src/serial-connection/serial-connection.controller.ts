import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateSerialConnectionDTO } from './dto/serialConnection.dto';
import { SerialConnectionService } from './serial-connection.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('serialConnection')
//@UseGuards(new AuthGuard())
export class SerialConnectionController {
  constructor(private serialConnectionService: SerialConnectionService) {}

  @Post()
  async createSerialConnection(
    @Res() res,
    @Body() body: CreateSerialConnectionDTO,
  ) {
    const newSerialConnection = await this.serialConnectionService.createSerialConnection(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Serial Connection created successfully',
      data: newSerialConnection,
    });
  }

  @Get()
  async getSerialConnections(@Res() res) {
    const serialConnections = await this.serialConnectionService.getSerialConnections();

    let msg =
      serialConnections.length == 0
        ? 'Serial connections not found'
        : 'Serial connections fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: serialConnections,
      count: serialConnections.length,
    });
  }

  @Get('/:serialConnectionId/device')
  async getDevicesBySerialConnectionId(
    @Res() res,
    @Param('serialConnectionId') serialConnectionId,
  ) {
    const devices = await this.serialConnectionService.getDevicesBySerialConnectionId(
      serialConnectionId,
    );

    let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: devices,
      count: devices.length,
    });
  }

  @Get('/:serialConnectionId')
  async getSerialConnection(
    @Res() res,
    @Param('serialConnectionId') serialConnectionId,
  ) {
    if (!serialConnectionId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException(
        'Serial connection is not a valid  ObjectId',
      );
    }

    const serialConnection = await this.serialConnectionService.getSerialConnection(
      serialConnectionId,
    );
    if (!serialConnection) {
      throw new NotFoundException('Serial connection not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Serial connection found',
      data: serialConnection,
    });
  }

  @Put('/:serialConnectionId')
  async updateSerialConnection(
    @Res() res,
    @Body() body: CreateSerialConnectionDTO,
    @Param('serialConnectionId') serialConnectionId,
  ) {
    if (!serialConnectionId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException(
        'Serial connection id is not a valid ObjectId',
      );
    }

    const updatedSerialConnection = await this.serialConnectionService.updateSerialConnection(
      serialConnectionId,
      body,
    );
    if (!updatedSerialConnection) {
      throw new NotFoundException('Serial connection not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Serial connection updated',
      data: updatedSerialConnection,
    });
  }

  @Delete('/:serialConnectionId')
  async deleteSerialConnection(
    @Res() res,
    @Param('serialConnectionId') serialConnectionId,
  ) {
    const deletedSerialConnection = await this.serialConnectionService.deleteSerialConnection(
      serialConnectionId,
    );

    if (!deletedSerialConnection) {
      throw new NotFoundException('Serial Connection not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Serial Connection deleted',
      data: deletedSerialConnection,
    });
  }
}
