import { Module } from '@nestjs/common';
import { SerialConnectionController } from './serial-connection.controller';
import { SerialConnectionService } from './serial-connection.service';
import { SerialConnectionSchema } from './schemas/serialConnection.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { DeviceModule } from '../device/device.module';
import { DeviceService } from '../device/device.service';

@Module({
  imports: [
    DeviceModule,
    MongooseModule.forFeature([
      {
        name: 'SerialConnection',
        schema: SerialConnectionSchema,
        collection: 'serialConnection',
      },
    ]),
  ],
  controllers: [SerialConnectionController],
  providers: [SerialConnectionService, DeviceService],
  exports: [SerialConnectionService],
})
export class SerialConnectionModule {}
