import { Document } from 'mongoose';

export interface SerialConnection extends Document {
  dataLength: string;
  parity: string;
  stopBits: string;
  baudRate: string;
  port: string;
}
