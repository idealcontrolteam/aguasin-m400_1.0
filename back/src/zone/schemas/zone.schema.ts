import { Schema } from 'mongoose';

export const ZoneSchema = new Schema(
  {
    name: String,
    active: Boolean,
    workPlaceId: {
      type: Schema.Types.ObjectId,
      ref: 'WorkPlace',
      required: true,
    },
  },
  { versionKey: false },
);
