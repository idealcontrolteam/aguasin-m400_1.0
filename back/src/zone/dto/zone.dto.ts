export class CreateZoneDTO {
  name: string;
  active: boolean;
  workPlaceId: string;
}
