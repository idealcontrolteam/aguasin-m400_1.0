import { Module } from '@nestjs/common';
import { ZoneController } from './zone.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ZoneService } from './zone.service';
import { ZoneSchema } from './schemas/zone.schema';
import { LocationModule } from '../location/location.module';
import { LocationService } from '../location/location.service';
import { TagModule } from '../tag/tag.module';
import { TagService } from '../tag/tag.service';

@Module({
  imports: [
    LocationModule,
    TagModule,
    MongooseModule.forFeature([
      { name: 'Zone', schema: ZoneSchema, collection: 'zone' },
    ]),
  ],
  controllers: [ZoneController],
  providers: [ZoneService, LocationService, TagService],
  exports: [ZoneService],
})
export class ZoneModule {}
