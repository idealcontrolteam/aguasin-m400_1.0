import { IsString, IsBoolean, IsOptional } from 'class-validator';

export class CreateDeviceDTO {
  @IsString()
  name: string;

  @IsBoolean()
  active: boolean;

  @IsString()
  modelId: string;

  @IsString()
  @IsOptional()
  tcpConnectionId?: string;

  @IsString()
  @IsOptional()
  serialConnectionId?: string;

  @IsString()
  gatewayId: string;
}
