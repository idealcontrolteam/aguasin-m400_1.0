import { Schema } from 'mongoose';
//import { ObjectID, Double } from 'bson';

export const ViewSchema = new Schema(
  {
    name: String,
    shortName: String,
    unity: Number,
    meditions:[{
        dateTime:Date,
        value:Number,
        active:Boolean
    }]
  },
  { versionKey: false },
);