import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
//import { CreateMeasurement_TagDTO } from '../measurement_tag/dto/measurement_tag.dto';
import { ViewsService } from './views.service';

@Controller('views')
export class ViewsController {
    constructor(private measurementService: ViewsService) {}
  //   @Get('all')
  // async getMeasurements_Tag(@Res() res) {
  //   const measurements = await this.measurementService.getViews();
  //   console.log('respuesta',measurements);
  //   let msg =
  //     measurements.length == 0
  //       ? 'Measurements not found'
  //       : 'Measurements fetched';

  //   return res.status(HttpStatus.OK).json({
  //     statusCode: HttpStatus.OK,
  //     message: msg,
  //     data: measurements,
  //     count: measurements.length,
  //   });
  //   }

    @Get('/xy/tag/:tagId/:fini/:ffin')
  async getViews(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getViews(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

        //---------------------Aqui


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }
}
