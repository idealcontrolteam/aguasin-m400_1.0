import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateCompanyDTO } from './dto/company.dto';
import { CompanyService } from './company.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('company')
//@UseGuards(new AuthGuard())
export class CompanyController {
  constructor(private companyService: CompanyService) {}

  @Post()
  async createCompany(@Res() res, @Body() createCompanyDTO: CreateCompanyDTO) {
    const newCompany = await this.companyService.createCompany(
      createCompanyDTO,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Company created successfully',
      data: newCompany,
    });
  }

  @Get()
  async getCompanies(@Res() res) {
    const companies = await this.companyService.getCompanies();

    let msg =
      companies.length == 0 ? 'Companies not found' : 'Companies fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: companies,
      count: companies.length,
    });
  }

  @Get('/:companyId/workPlace')
  async getWorkPlacesByCompanyId(@Res() res, @Param('companyId') companyId) {
    const workplaces = await this.companyService.getWorkPlacesByCompanyId(
      companyId,
    );

    let msg =
      workplaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workplaces,
      count: workplaces.length,
    });
  }

  @Get('/:companyId/user')
  async getUsersByCompanyId(@Res() res, @Param('companyId') companyId) {
    const users = await this.companyService.getUsersByCompanyId(companyId);

    let msg = users.length == 0 ? 'Users not found' : 'Users fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: users,
      count: users.length,
    });
  }

  @Get('/:companyId')
  async getCompany(@Res() res, @Param('companyId') companyId) {
    if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Company id is not a valid ObjectId');
    }
    const company = await this.companyService.getCompany(companyId);
    if (!company) {
      throw new NotFoundException('Company not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Company found',
      data: company,
    });
  }

  @Put('/:companyId')
  async updateCompany(
    @Res() res,
    @Body() body: CreateCompanyDTO,
    @Param('companyId') companyId,
  ) {
    if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Company id is not a valid ObjectId');
    }
    const updatedCompany = await this.companyService.updateCompany(
      companyId,
      body,
    );
    if (!updatedCompany) {
      throw new NotFoundException('Company not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Company updated successfully',
      data: updatedCompany,
    });
  }

  @Delete('/:companyId')
  async deleteCompany(@Res() res, @Param('companyId') companyId) {
    if (!companyId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Company id is not a valid ObjectId');
    }
    const company = await this.companyService.deleteCompany(companyId);
    if (!company) {
      throw new NotFoundException('Company not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Company Deleted',
      data: company,
    });
  }
}
