import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Company } from './interfaces/company.interface';
import { CreateCompanyDTO } from './dto/company.dto';
import { WorkPlaceService } from '../work-place/work-place.service';
import { WorkPlace } from '../work-place/interfaces/workPlace.interface';
import { UserService } from '../user/user.service';
import { User } from '../user/interfaces/user.interface';
import { from } from 'rxjs';

@Injectable()
export class CompanyService {
  constructor(
    @InjectModel('Company') private companyModel: Model<Company>,
    private workPlaceService: WorkPlaceService,
    private userService: UserService,
  ) {}

  async getCompanies(): Promise<Company[]> {
    const companies = await this.companyModel.find();
    return companies;
  }

  async countCompanies() {
    const countCompanies = await this.companyModel.count({});
    return countCompanies;
  }

  async getWorkPlacesByCompanyId(companyId): Promise<WorkPlace[]> {
    const workplaces = await this.workPlaceService.getWorkPlacesByCompanyId(
      companyId,
    );
    return workplaces;
  }

  async getUsersByCompanyId(companyId): Promise<User[]> {
    const users = await this.userService.getUsersByCompanyId(companyId);
    return users;
  }

  async getCompany(id): Promise<Company> {
    const company = await this.companyModel.findById(id);
    return company;
  }

  async createCompany(createCompanyDTO: CreateCompanyDTO): Promise<Company> {
    const newCompany = new this.companyModel(createCompanyDTO);
    return await newCompany.save();
  }

  async deleteCompany(id): Promise<Company> {
    await this.workPlaceService.deleteWorkPlacesByCompanyId(id);
    await this.userService.deleteUsersByCompanyId(id);
    return await this.companyModel.findByIdAndDelete(id);
  }

  async updateCompany(
    id: string,
    createCompanyDTO: CreateCompanyDTO,
  ): Promise<Company> {
    return await this.companyModel.findByIdAndUpdate(id, createCompanyDTO, {
      new: true,
    });
  }
}
