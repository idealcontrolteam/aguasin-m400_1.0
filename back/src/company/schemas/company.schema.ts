import { Schema } from 'mongoose';

export const CompanySchema = new Schema(
  {
    name: String,
    logo: String,
    active: Boolean,
  },
  { versionKey: false },
);
