import { Document } from 'mongoose';

export interface MeasurementCopy extends Document {
  value: number;
  dateTime: Date;
  tagId: string;
  sensorId?: string;
  locationId: string;
  active: boolean;
}
