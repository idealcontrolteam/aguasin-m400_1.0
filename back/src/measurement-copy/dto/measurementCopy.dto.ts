export class CreateMeasurementCopyDTO {
  value: number;
  dateTime: Date;
  tagId: string;
  sensorId?: string;
  locationId: string;
  active: boolean;
}
