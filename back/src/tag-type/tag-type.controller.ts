import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateTagTypeDTO } from './dto/tagType.dto';
import { TagTypeService } from './tag-type.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('tagType')
//@UseGuards(new AuthGuard())
export class TagTypeController {
  constructor(private tagTypeService: TagTypeService) {}

  @Post()
  async createTagType(@Res() res, @Body() body: CreateTagTypeDTO) {
    const newTagType = await this.tagTypeService.createTagType(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Tag type created successfully',
      data: newTagType,
    });
  }

  @Get()
  async getTagType(@Res() res) {
    const tagtypes = await this.tagTypeService.getTagTypes();

    let msg =
      tagtypes.length == 0 ? 'Tag types not found' : 'Tag types fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tagtypes,
      count: tagtypes.length,
    });
  }

  @Get('/:tagTypeId/tag')
  async getTagsByTagTypeId(@Res() res, @Param('tagTypeId') tagTypeId) {
    const tags = await this.tagTypeService.getTagsByTagTypeId(tagTypeId);

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/:tagTypeId')
  async getTagTypeById(@Res() res, @Param('tagTypeId') tagTypeId) {
    if (!tagTypeId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag type id is not a valid ObjectId');
    }

    const tagType = await this.tagTypeService.getTagType(tagTypeId);
    if (!tagType) {
      throw new NotFoundException('Tag type not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag type found',
      data: tagType,
    });
  }

  @Put('/:tagTypeid')
  async updateTagType(
    @Res() res,
    @Body() body: CreateTagTypeDTO,
    @Param('tagTypeid') tagTypeid,
  ) {
    if (!tagTypeid.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag type id is not a valid ObjectId');
    }
    const updatedTagType = await this.tagTypeService.updateTagType(
      tagTypeid,
      body,
    );
    if (!updatedTagType) {
      throw new NotFoundException('Tag type not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag type updated',
      data: updatedTagType,
    });
  }

  @Delete('/:tagTypeid')
  async deleteTagType(@Res() res, @Param('tagTypeid') tagTypeid) {
    const deletedTagType = await this.tagTypeService.deleteTagType(tagTypeid);

    if (!deletedTagType) {
      throw new NotFoundException('Tag type not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag type deleted',
      data: deletedTagType,
    });
  }
}
