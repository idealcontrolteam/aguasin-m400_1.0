import { Schema } from 'mongoose';

export const TagTypeSchema = new Schema(
  {
    name: String,
    active: Boolean,
  },
  { versionKey: false },
);
