import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { TagService } from '../tag/tag.service';
import { Tag } from '../tag/interfaces/tag.interface';
import { TagType } from './interfaces/tagType.interface';
import { CreateTagTypeDTO } from './dto/tagType.dto';

@Injectable()
export class TagTypeService {
  constructor(
    @InjectModel('TagType') private tagTypeModel: Model<TagType>,
    private tagService: TagService,
  ) {}

  async getTagTypes(): Promise<TagType[]> {
    const tagTypes = await this.tagTypeModel.find();
    return tagTypes;
  }

  async getTagType(id): Promise<TagType> {
    const tagType = await this.tagTypeModel.findById(id);
    return tagType;
  }

  async getTagsByTagTypeId(tagTypeId): Promise<Tag[]> {
    const tags = await this.tagService.getTagsByTagTyperId(tagTypeId);
    return tags;
  }

  async createTagType(createTagTypeDTO: CreateTagTypeDTO): Promise<TagType> {
    const newTagType = new this.tagTypeModel(createTagTypeDTO);
    return await newTagType.save();
  }

  async deleteTagType(id): Promise<TagType> {
    return await this.tagTypeModel.findByIdAndDelete(id);
  }

  async updateTagType(id: string, body: CreateTagTypeDTO): Promise<TagType> {
    return await this.tagTypeModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
