import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BrandModule } from './brand/brand.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ModelModule } from './model/model.module';
import { TcpConnectionModule } from './tcp-connection/tcp-connection.module';
import { SerialConnectionModule } from './serial-connection/serial-connection.module';
import { GatewayModule } from './gateway/gateway.module';
import { DeviceModule } from './device/device.module';
import { MeasurementCopyModule } from './measurement-copy/measurement-copy.module';
import { CategoryModule } from './category/category.module';
import { CompanyModule } from './company/company.module';
import { WorkPlaceModule } from './work-place/work-place.module';
import { UserModule } from './user/user.module';
import { RoleModule } from './role/role.module';
import { ZoneModule } from './zone/zone.module';
import { LocationModule } from './location/location.module';
import { SensorModule } from './sensor/sensor.module';
import { MeasurementModule } from './measurement/measurement.module';
import { TagModule } from './tag/tag.module';
import { TagTypeModule } from './tag-type/tag-type.module';
import { TagConfigControlModule } from './tag-config-control/tag-config-control.module';
import { AlarmModule } from './alarm/alarm.module';
import { FailModule } from './fail/fail.module';
import { ViewsController } from './views/views.controller';
import { ViewsModule } from './views/views.module';

@Module({
  imports: [
    BrandModule,    
    //MongooseModule.forRoot('mongodb://idealcontrol:Ideal_cloud281@127.0.0.1:27017/bdaguasin?authSource=admin', {
    //MongooseModule.forRoot('mongodb://idealcontrol:Ideal_cloud281@169.53.138.44:27017/bdaguasin?authSource=admin', {
    MongooseModule.forRoot('mongodb://127.0.0.1:27017/bdperal', {
    //MongooseModule.forRoot('mongodb://172.30.2.212:27017/bdaguasin', {
      useNewUrlParser: true,
      useFindAndModify: false 
    }),
    ModelModule,
    TcpConnectionModule,
    SerialConnectionModule,
    GatewayModule,
    DeviceModule,
    MeasurementCopyModule,
    CategoryModule,
    CompanyModule,
    WorkPlaceModule,
    UserModule,
    RoleModule,
    ZoneModule,
    LocationModule,
    SensorModule,
    MeasurementModule,
    TagModule,
    TagTypeModule,
    TagConfigControlModule,
    AlarmModule,
    FailModule,
    ViewsModule,
  ],
  controllers: [AppController,ViewsController],
  providers: [AppService],
})
export class AppModule {}
