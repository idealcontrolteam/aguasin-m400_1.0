export class CreateCategoryDTO {
  name: string;
  active: boolean;
}
