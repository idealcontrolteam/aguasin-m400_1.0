import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Category } from './interfaces/category.interface';
import { CreateCategoryDTO } from './dto/category.dto';
import { WorkPlaceService } from '../work-place/work-place.service';
import { WorkPlace } from '../work-place/interfaces/workPlace.interface';

@Injectable()
export class CategoryService {
  constructor(
    @InjectModel('Category') private categoryModel: Model<Category>,
    private workPlaceService: WorkPlaceService,
  ) {}

  async getCategories(): Promise<Category[]> {
    const categories = await this.categoryModel.find();
    return categories;
  }

  async getWorkPlacesByCategoryId(categoryId): Promise<WorkPlace[]> {
    const workplaces = await this.workPlaceService.getWorkPlacesByCategoryId(
      categoryId,
    );
    return workplaces;
  }

  async getCategory(id): Promise<Category> {
    const category = await this.categoryModel.findById(id);
    return category;
  }

  async createCategory(
    createCategoryDTO: CreateCategoryDTO,
  ): Promise<Category> {
    const newCategory = new this.categoryModel(createCategoryDTO);
    return await newCategory.save();
  }

  async deleteCategory(id): Promise<Category> {
    return await this.categoryModel.findByIdAndDelete(id);
  }

  async updateCategory(
    id: string,
    createCategoryDTO: CreateCategoryDTO,
  ): Promise<Category> {
    return await this.categoryModel.findByIdAndUpdate(id, createCategoryDTO, {
      new: true,
    });
  }
}
