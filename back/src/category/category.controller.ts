import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateCategoryDTO } from './dto/category.dto';
import { CategoryService } from './category.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('category')
//@UseGuards(new AuthGuard())
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Post()
  async createCategory(
    @Res() res,
    @Body() createCategoryDTO: CreateCategoryDTO,
  ) {
    const newCategory = await this.categoryService.createCategory(
      createCategoryDTO,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Category created successfully',
      data: newCategory,
    });
  }

  @Get()
  async getCategories(@Res() res) {
    const categories = await this.categoryService.getCategories();

    let msg =
      categories.length == 0 ? 'Categories not found' : 'Categories fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: categories,
      count: categories.length,
    });
  }

  @Get('/:categoryId/workPlace')
  async getWorkPlacesByCategoryId(@Res() res, @Param('categoryId') categoryId) {
    if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Category id is not a valid ObjectId');
    }
    const workplaces = await this.categoryService.getWorkPlacesByCategoryId(
      categoryId,
    );

    let msg =
      workplaces.length == 0 ? 'Work places not found' : 'Work places fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: workplaces,
      count: workplaces.length,
    });
  }

  @Get('/:categoryId')
  async getCategory(@Res() res, @Param('categoryId') categoryId) {
    if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Category id is not a valid ObjectId');
    }
    const category = await this.categoryService.getCategory(categoryId);
    if (!category) {
      throw new NotFoundException('Category not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Category found',
      data: category,
    });
  }

  @Put('/:categoryId')
  async updateCategory(
    @Res() res,
    @Body() body: CreateCategoryDTO,
    @Param('categoryId') categoryId,
  ) {
    if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Category id is not a valid ObjectId');
    }
    const updatedCategory = await this.categoryService.updateCategory(
      categoryId,
      body,
    );
    if (!updatedCategory) {
      throw new NotFoundException('Category not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Category updated successfully',
      data: updatedCategory,
    });
  }

  @Delete('/:categoryId')
  async deleteCategory(@Res() res, @Param('categoryId') categoryId) {
    if (!categoryId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Category id is not a valid ObjectId');
    }
    const category = await this.categoryService.deleteCategory(categoryId);
    if (!category) {
      throw new NotFoundException('Category not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Category Deleted',
      data: category,
    });
  }
}
