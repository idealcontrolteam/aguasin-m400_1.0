import { IsString, IsBoolean, IsNotEmpty } from 'class-validator';

export class CreateModelDTO {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsBoolean()
  active: boolean;

  @IsString()
  @IsNotEmpty()
  brandId: string;
}
