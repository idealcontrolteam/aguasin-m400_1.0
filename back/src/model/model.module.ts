import { Module } from '@nestjs/common';
import { ModelController } from './model.controller';
import { ModelService } from './model.service';
import { BrandService } from '../brand/brand.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ModelSchema } from './schemas/model.schema';
import { BrandModule } from '../brand/brand.module';
import { DeviceModule } from '../device/device.module';
import { DeviceService } from '../device/device.service';
import { from } from 'rxjs';

@Module({
  imports: [
    DeviceModule,
    MongooseModule.forFeature([
      { name: 'Model', schema: ModelSchema, collection: 'model' },
    ]),
  ],
  controllers: [ModelController],
  providers: [ModelService, DeviceService],
  exports: [ModelService],
})
export class ModelModule {}
