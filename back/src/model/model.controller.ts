import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateModelDTO } from './dto/model.dto';
import { ModelService } from './model.service';
import { DeviceService } from '../device/device.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('model')
//@UseGuards(new AuthGuard())
export class ModelController {
  constructor(
    private modelService: ModelService,
    private deviceService: DeviceService,
  ) {}

  @Post()
  async createModel(@Res() res, @Body() body: CreateModelDTO) {
    if (!body.brandId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('BrandId is not a valid  ObjectId');
    }

    const newModel = await this.modelService.createModel(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Model created successfully',
      data: newModel,
    });
  }

  @Get()
  async getModels(@Res() res) {
    const models = await this.modelService.getModels();

    let msg = models.length == 0 ? 'Models not found' : 'Models fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: models,
      count: models.length,
    });
  }

  @Get('/all')
  async getModelsAll(@Res() res) {
    const models = await this.modelService.getModelsAll();

    let msg = models.length == 0 ? 'Models not found' : 'Models fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: models,
      count: models.length,
    });
  }

  @Get('/:modelid/device')
  async getDevicesByModelId(@Res() res, @Param('modelid') modelid) {
    const devices = await this.deviceService.getDevicesByModelId(modelid);

    let msg = devices.length == 0 ? 'Devices not found' : 'Devices fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: devices,
      count: devices.length,
    });
  }

  @Get('/:modelid')
  async getModel(@Res() res, @Param('modelid') modelid) {
    const model = await this.modelService.getModel(modelid);
    if (!model) {
      throw new NotFoundException('Model not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Model found',
      data: model,
    });
  }

  @Put('/:modelid')
  async updateModel(
    @Res() res,
    @Body() body: CreateModelDTO,
    @Param('modelid') modelid,
  ) {
    if (!body.brandId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('BrandId is not a valid  ObjectId');
    }

    const updatedModel = await this.modelService.updateModel(modelid, body);
    if (!updatedModel) {
      throw new NotFoundException('Model not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Model updated',
      data: updatedModel,
    });
  }

  @Delete('/deleteByBrandId/:brandId')
  async deleteModelsByBrandId(@Res() res, @Param('brandId') brandId) {
    const deletedModels = await this.modelService.deleteModelsByBrandId(
      brandId,
    );

    if (!deletedModels) {
      throw new NotFoundException('No models deleted');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Models deleted',
      data: deletedModels,
    });
  }

  @Delete('/:modelid')
  async deleteModel(@Res() res, @Param('modelid') modelid) {
    const deletedModel = await this.modelService.deleteModel(modelid);

    if (!deletedModel) {
      throw new NotFoundException('Model not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Model deleted',
      data: deletedModel,
    });
  }
}
