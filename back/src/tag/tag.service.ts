import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Tag } from './interfaces/tag.interface';
import { CreateTagDTO } from './dto/tag.dto';
import { MeasurementService } from '../measurement/measurement.service';
import { Measurement } from '../measurement/interfaces/measurement.interface';
import { FailService } from '../fail/fail.service';
import { Fail } from '../fail/interfaces/fail.interface';
import { AlarmService } from '../alarm/alarm.service';
import { Alarm } from '../alarm/interfaces/alarm.interface';
import * as mongoose from 'mongoose';
import {_,orderBy} from 'lodash';


@Injectable()
export class TagService {
  constructor(
    @InjectModel('Tag') private tagModel: Model<Tag>,
    private measurementService: MeasurementService,
    private failService: FailService,
    private alarmService: AlarmService,
  ) {}

  

  async getTagsFilterRegister(valor): Promise<Tag[]> { 
     const tags = await this.tagModel.find({"register":Boolean(valor)});
    return tags;
  }
  async getTags(): Promise<Tag[]> {
    const tags = await this.tagModel.find();
    return tags;
  }

  async getTag(id): Promise<Tag> {
    const tag = await this.tagModel.findById(id);
    return tag;
  }

  async getTagsAll(): Promise<Tag[]> {
    const tags = await this.tagModel
      .find()
      .populate('tagTypeId zoneId locationId sensorId deviceId');
    return tags;
  }

  async getTagsByDeviceId(deviceId): Promise<Tag[]> {
    console.log('[IN TAG SERVICE]', deviceId);
    const tags = await this.tagModel.find({ deviceId: deviceId });
    return tags;
  }

  async getTagsByZoneId(zoneId): Promise<Tag[]> {

    const tags = await this.tagModel.find({ zoneId: zoneId },'nameAddress lastValue');
    return tags;
  }

  async getTagsByZoneIdWithMeasurements(zoneId, fini, ffin) {
    const tags = await this.tagModel.find({ zoneId: zoneId });

    let tagsIdZone = {};

    tags.map((tag, pos) => (tagsIdZone[tag._id] = pos));

    const measurements = await this.measurementService.getMeasurementFilteredByTagsAndDate(
      Object.keys(tagsIdZone),
      fini,
      ffin,
    );

    let tagsUsed = [];
    let responseJson = [];
    let positionsTags = {};

    measurements.map(measurement => {
      if (!tagsUsed.includes(measurement.tagId.toString())) {
        const tagToUse = tags[tagsIdZone[measurement.tagId]];
        responseJson.push({
          ...tagToUse.toJSON(),
          measurements: [],
        });
        tagsUsed.push(measurement.tagId.toString());
        positionsTags[measurement.tagId] = tagsUsed.length - 1;
        responseJson[responseJson.length - 1].measurements.push(measurement);
      } else {
        responseJson[positionsTags[measurement.tagId]]['measurements'].push(
          measurement,
        );
      }
    });

    return responseJson;
  }

  async getTagsByLocationIdWithMeasurements(locationId, fini, ffin) {
    const tags = await this.tagModel.find({ locationId: locationId }).collation({locale:'en',strength: 2}).sort({shortName:1});

    let tagsIdLocationId = {};
    //console.log(tags);

    tags.map((tag, pos) => (tagsIdLocationId[tag._id] = pos));

    const measurements = await this.measurementService.getMeasurementFilteredByTagsAndDate(
      Object.keys(tagsIdLocationId),
      fini,
      ffin,
    );

   // console.log(measurements);

    let tagsUsed = [];
    let responseJson = [];
    let positionsTags = {};

    measurements.map(measurement => {
      if (!tagsUsed.includes(measurement.tagId.toString())) {
        const tagToUse = tags[tagsIdLocationId[measurement.tagId]];
        responseJson.push({
          ...tagToUse.toJSON(),
          measurements: [],
        });
        tagsUsed.push(measurement.tagId.toString());
        positionsTags[measurement.tagId] = tagsUsed.length - 1;
        responseJson[responseJson.length - 1].measurements.push(measurement);
      } else {
        responseJson[positionsTags[measurement.tagId]]['measurements'].push(
          measurement,
        );
      }
    });

    // responseJson.forEach((resp)=>{
    //     resp.name.sort();
    // });

    //console.log();

    return _.orderBy(responseJson, ['shortName'],['desc']);
  }

  async getTagsByLocationId(locationId): Promise<Tag[]> {
    const tags = await this.tagModel.find({ locationId: locationId });
    return tags;
  }

  async getTagsBySensorId(sensorId): Promise<Tag[]> {
    const tags = await this.tagModel.find({ sensorId: sensorId });
    return tags;
  }

  async getTagsByTagTyperId(tagTypeId): Promise<Tag[]> {
    const tags = await this.tagModel.find({ tagTypeId: tagTypeId });
    return tags;
  }

  async getMeasurementsByTagId(tagId): Promise<Measurement[]> {
    const measurements = await this.measurementService.getMeasurementsByTagId(
      tagId,
    );
    return measurements;
  }

  async getAlarmsByTagId(tagId): Promise<Alarm[]> {
    const alarms = await this.alarmService.getAlarmsByTagId(tagId);
    return alarms;
  }

  async getFailsByTagId(tagId): Promise<Fail[]> {
    const fails = await this.failService.getFailsByTagId(tagId);
    return fails;
  }

  async createTag(createTagDTO: CreateTagDTO): Promise<Tag> {
    const newTag = new this.tagModel(createTagDTO);
    return await newTag.save();
  }

  async deleteTag(id): Promise<Tag> {
    return await this.tagModel.findByIdAndDelete(id);
  }

  async updateTag(id: string, body: CreateTagDTO): Promise<Tag> {
    return await this.tagModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
