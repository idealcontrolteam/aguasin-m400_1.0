import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateTagDTO } from './dto/tag.dto';
import { TagService } from './tag.service';
import { TagConfigControlService } from '../tag-config-control/tag-config-control.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('tag')
// //@UseGuards(new AuthGuard())
export class TagController {
  constructor(
    private tagService: TagService,
    private tagConfigControlService: TagConfigControlService,
  ) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (
        key == 'tagTypeId' ||
        key == 'zoneId' ||
        key == 'locationId' ||
        key == 'locationId' ||
        key == 'deviceId'
      ) {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };


  @Post('/multiple')
  async updateTagMultiple(@Res() res, @Body() body) {
    //this.validateIds(body);
   // console.log(body);
    let updatedTag = {};
    body.map(async (Tag) => {      
      updatedTag =  await this.tagService.updateTag(Tag._id, Tag);         
    });  
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag updated',
      data: updatedTag,
    });
  }

  @Post()
  async createModel(@Res() res, @Body() body: CreateTagDTO) {
    this.validateIds(body);

    const newTag = await this.tagService.createTag(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Tag created successfully',
      data: newTag,
    });
  }

  @Get('/filteroregister/:valor')
  async getTagsFilterRegister(
    @Res() res,  
    @Param('valor') valor,
    ) {
    const tags = await this.tagService.getTagsFilterRegister(valor);

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get()
  async getTags(@Res() res) {
    const tags = await this.tagService.getTags();

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/zone/:zoneId/:fini/:ffin')
  async getTagsByZoneIdWithMeasurements(
    @Res() res,
    @Param('zoneId') zoneId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!zoneId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Zone id is not a valid  ObjectId');
    }

    const tags = await this.tagService.getTagsByZoneIdWithMeasurements(
      zoneId,
      fini,
      ffin,
    );

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/location/:locationId/:fini/:ffin')
  async getTagsByLocationIdWithMeasurements(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid  ObjectId');
    }

    const tags = await this.tagService.getTagsByLocationIdWithMeasurements(
      locationId,
      fini,
      ffin,
    );

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/all')
  async getTagsAll(@Res() res) {
    const tags = await this.tagService.getTagsAll();

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/:tagId/measurement')
  async getMeasurementsByTagId(@Res() res, @Param('tagId') tagId) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid  ObjectId');
    }

    const measurements = await this.tagService.getMeasurementsByTagId(tagId);

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/:tagId/alarm')
  async getAlarmsByTagId(@Res() res, @Param('tagId') tagId) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid  ObjectId');
    }

    const alarms = await this.tagService.getAlarmsByTagId(tagId);

    let msg = alarms.length == 0 ? 'Alarms not found' : 'Alarms fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: alarms,
      count: alarms.length,
    });
  }

  @Get('/:tagId/fail')
  async getFailsByTagId(@Res() res, @Param('tagId') tagId) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid  ObjectId');
    }

    const fails = await this.tagService.getFailsByTagId(tagId);

    let msg = fails.length == 0 ? 'Fails not found' : 'Fails fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: fails,
      count: fails.length,
    });
  }

  @Get('/:tagId/tagConfigControl')
  async geTagConfigControlsByTagId(@Res() res, @Param('tagId') tagId) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid  ObjectId');
    }

    const tagConfigControls = await this.tagConfigControlService.geTagConfigControlsByTagId(
      tagId,
    );

    let msg =
      tagConfigControls.length == 0
        ? 'Tag config controls not found'
        : 'Tag config controls fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tagConfigControls,
      count: tagConfigControls.length,
    });
  }

  @Get('/:tagId')
  async getTag(@Res() res, @Param('tagId') tagId) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid  ObjectId');
    }

    const tag = await this.tagService.getTag(tagId);
    if (!tag) {
      throw new NotFoundException('Tag not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag found',
      data: tag,
    });
  }

  @Put('/:tagId')
  async updateTag(
    @Res() res,
    @Body() body: CreateTagDTO,
    @Param('tagId') tagId,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid  ObjectId');
    }

    const updatedTag = await this.tagService.updateTag(tagId, body);
    if (!updatedTag) {
      throw new NotFoundException('Tag not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag updated',
      data: updatedTag,
    });
  }

  @Delete('/:tagId')
  async deleteTag(@Res() res, @Param('tagId') tagId) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid  ObjectId');
    }

    const deletedTag = await this.tagService.deleteTag(tagId);

    if (!deletedTag) {
      throw new NotFoundException('Tag not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Tag deleted',
      data: deletedTag,
    });
  }
}
