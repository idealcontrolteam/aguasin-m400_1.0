import { Module } from '@nestjs/common';
import { TagController } from './tag.controller';
import { TagService } from './tag.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TagSchema } from './schemas/tag.schema';
import { MeasurementModule } from '../measurement/measurement.module';
import { MeasurementService } from '../measurement/measurement.service';
import { TagConfigControlModule } from '../tag-config-control/tag-config-control.module';
import { TagConfigControlService } from '../tag-config-control/tag-config-control.service';
import { AlarmService } from '../alarm/alarm.service';
import { AlarmModule } from '../alarm/alarm.module';
import { FailModule } from '../fail/fail.module';
import { FailService } from '../fail/fail.service';

@Module({
  imports: [
    MeasurementModule,
    TagConfigControlModule,
    AlarmModule,
    FailModule,
    MongooseModule.forFeature([
      { name: 'Tag', schema: TagSchema, collection: 'tag' },
    ]),
  ],
  controllers: [TagController],
  providers: [
    TagService,
    MeasurementService,
    TagConfigControlService,
    AlarmService,
    FailService,
  ],
  exports: [TagService],
})
export class TagModule {}
