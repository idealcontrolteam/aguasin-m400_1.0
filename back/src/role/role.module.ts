import { Module } from '@nestjs/common';
import { RoleController } from './role.controller';
import { RoleService } from './role.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RoleSchema } from './schemas/role.schema';
import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';

@Module({
  imports: [
    UserModule,
    MongooseModule.forFeature([
      { name: 'Role', schema: RoleSchema, collection: 'role' },
    ]),
  ],
  controllers: [RoleController],
  providers: [RoleService, UserService],
})
export class RoleModule {}
