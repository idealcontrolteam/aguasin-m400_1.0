import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateRoleDTO } from './dto/role.dto';
import { Role } from './interfaces/role.interface';
import { UserService } from '../user/user.service';
import { User } from '../user/interfaces/user.interface';

@Injectable()
export class RoleService {
  constructor(
    @InjectModel('Role') private roleModel: Model<Role>,
    private userService: UserService,
  ) {}

  async getRoles(): Promise<Role[]> {
    const roles = await this.roleModel.find();
    return roles;
  }

  async getUsersByRoleId(roleId): Promise<User[]> {
    const users = await this.userService.getUsersByRoleId(roleId);
    return users;
  }

  async getRole(id): Promise<Role> {
    const role = await this.roleModel.findById(id);
    return role;
  }

  async createRole(createRoleDTO: CreateRoleDTO): Promise<Role> {
    const newRole = new this.roleModel(createRoleDTO);
    return await newRole.save();
  }

  async deleteRole(id): Promise<Role> {
    return await this.roleModel.findByIdAndDelete(id);
  }

  async updateRole(id: string, body: CreateRoleDTO): Promise<Role> {
    return await this.roleModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
