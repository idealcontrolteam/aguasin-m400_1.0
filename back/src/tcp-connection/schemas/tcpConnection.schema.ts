import { Schema } from 'mongoose';

export const TcpConnectionSchema = new Schema(
  {
    ip: String,
    port: String,
  },
  { versionKey: false },
);
