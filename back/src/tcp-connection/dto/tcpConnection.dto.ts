import { IsString, IsNotEmpty } from 'class-validator';

export class CreateTcpConnectionDTO {
  @IsString()
  @IsNotEmpty()
  ip: string;

  @IsString()
  @IsNotEmpty()
  port: string;
}
