import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { TcpConnection } from './interfaces/tcpConnection.interface';
import { CreateTcpConnectionDTO } from './dto/tcpConnection.dto';
import { DeviceService } from '../device/device.service';
import { Device } from '../device/interfaces/device.interface';

@Injectable()
export class TcpConnectionService {
  constructor(
    @InjectModel('TcpConnection')
    private tcpConnectionModel: Model<TcpConnection>,
    private deviceService: DeviceService,
  ) {}

  async getTcpConnections(): Promise<TcpConnection[]> {
    const tcpConnections = await this.tcpConnectionModel.find();
    return tcpConnections;
  }

  async getTcpConnection(id): Promise<TcpConnection> {
    const tcpConnection = await this.tcpConnectionModel.findById(id);
    return tcpConnection;
  }

  async getDevicesByTcpConnectionId(tcpConnectionId): Promise<Device[]> {
    const devices = await this.deviceService.getDevicesByTcpConnectionId(
      tcpConnectionId,
    );
    return devices;
  }

  async createTcpConnection(
    createTcpConnectionDTO: CreateTcpConnectionDTO,
  ): Promise<TcpConnection> {
    const newTcpConnection = new this.tcpConnectionModel(
      createTcpConnectionDTO,
    );
    return await newTcpConnection.save();
  }

  async deleteTcpConnection(id): Promise<TcpConnection> {
    return await this.tcpConnectionModel.findByIdAndDelete(id);
  }

  async updateTcpConnection(
    id: string,
    body: CreateTcpConnectionDTO,
  ): Promise<TcpConnection> {
    return await this.tcpConnectionModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
