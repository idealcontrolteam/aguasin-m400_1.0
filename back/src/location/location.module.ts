import { Module } from '@nestjs/common';
import { LocationController } from './location.controller';
import { LocationService } from './location.service';
import { MongooseModule } from '@nestjs/mongoose';
import { LocationSchema } from './schemas/location.chema';
import { MeasurementModule } from '../measurement/measurement.module';
import { MeasurementService } from '../measurement/measurement.service';
import { SensorModule } from '../sensor/sensor.module';
import { SensorService } from '../sensor/sensor.service';
import { TagModule } from '../tag/tag.module';
import { TagService } from '../tag/tag.service';

@Module({
  imports: [
    MeasurementModule,
    SensorModule,
    TagModule,
    MongooseModule.forFeature([
      { name: 'Location', schema: LocationSchema, collection: 'location' },
    ]),
  ],
  controllers: [LocationController],
  providers: [LocationService, MeasurementService, SensorService, TagService],
  exports: [LocationService],
})
export class LocationModule {}
