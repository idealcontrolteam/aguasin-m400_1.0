import { Schema } from 'mongoose';

export const LocationSchema = new Schema(
  {
    name: String,
    active: Boolean,
    zoneId: {
      type: Schema.Types.ObjectId,
      ref: 'Zone',
      required: true,
    },
  },
  { versionKey: false },
);
