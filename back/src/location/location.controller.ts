import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateLcationDTO } from './dto/location.dto';
import { LocationService } from './location.service';
import { AuthGuard } from 'src/shared/auth.guard';

@Controller('location')
//@UseGuards(new AuthGuard())
export class LocationController {
  constructor(private locationService: LocationService) {}

  @Post()
  async createLocation(@Res() res, @Body() body: CreateLcationDTO) {
    if (!body.zoneId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('ZoneId is not a valid ObjectId');
    }

    const newLocation = await this.locationService.createLocation(body);
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Location created successfully',
      data: newLocation,
    });
  }

  @Get()
  async getLocations(@Res() res) {
    const locations = await this.locationService.getLocations();

    let msg =
      locations.length == 0 ? 'Locations not found' : 'Locations fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: locations,
      count: locations.length,
    });
  }

  @Get('/all')
  async getLocationsAll(@Res() res) {
    const locations = await this.locationService.getLocationsAll();

    let msg =
      locations.length == 0 ? 'Locations not found' : 'Locations fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: locations,
      count: locations.length,
    });
  }

  @Get('/:locationId/measurement')
  async getMesurementsByLocationId(
    @Res() res,
    @Param('locationId') locationId,
  ) {
    const measurements = await this.locationService.getMesurementsByLocationId(
      locationId,
    );

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/:locationId/sensor')
  async getSensorsByLocationId(@Res() res, @Param('locationId') locationId) {
    const sensors = await this.locationService.getSensorsByLocationId(
      locationId,
    );

    let msg = sensors.length == 0 ? 'Sensors not found' : 'Sensors fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: sensors,
      count: sensors.length,
    });
  }

  @Get('/:locationId/tag')
  async getTagsByLocationId(@Res() res, @Param('locationId') locationId) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const tags = await this.locationService.getTagsByLocationId(locationId);

    let msg = tags.length == 0 ? 'Tags not found' : 'Tags fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: tags,
      count: tags.length,
    });
  }

  @Get('/:locationId')
  async getLocation(@Res() res, @Param('locationId') locationId) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const location = await this.locationService.getLocation(locationId);
    if (!location) {
      throw new NotFoundException('Location not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Location found',
      data: location,
    });
  }

  @Put('/:locationId')
  async updateGateway(
    @Res() res,
    @Body() body: CreateLcationDTO,
    @Param('locationId') locationId,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }
    if (!body.zoneId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('zoneId is not a valid ObjectId');
    }

    const updatedLocation = await this.locationService.updateLocation(
      locationId,
      body,
    );
    if (!updatedLocation) {
      throw new NotFoundException('Location not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Location updated',
      data: updatedLocation,
    });
  }

  @Delete('/:locationId')
  async deleteLocation(@Res() res, @Param('locationId') locationId) {
    const deletedLocation = await this.locationService.deleteLocation(
      locationId,
    );

    if (!deletedLocation) {
      throw new NotFoundException('Location not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Location deleted',
      data: deletedLocation,
    });
  }
}
