import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { WorkPlace } from './interfaces/workPlace.interface';
import { CreateWorkPlaceDTO } from './dto/workPlace.dto';
import { GatewayService } from '../gateway/gateway.service';
import { Gateway } from '../gateway/interfaces/gateway.interface';
import { ZoneService } from '../zone/zone.service';
import { Zone } from '../zone/interfaces/zone.interface';

@Injectable()
export class WorkPlaceService {
  constructor(
    @InjectModel('WorkPlace') private workPlaceModel: Model<WorkPlace>,
    private gatewayService: GatewayService,
    private zoneService: ZoneService,
  ) {}

  async getWorkPlaces(): Promise<WorkPlace[]> {
    const workPlaces = await this.workPlaceModel.find();
    return workPlaces;
  }

  async getWorkPlacesAll(): Promise<WorkPlace[]> {
    const workPlaces = await this.workPlaceModel
      .find()
      .populate('companyId categoryId');
    return workPlaces;
  }

  async getGateWaysByWorkPlaceId(workPlaceId): Promise<Gateway[]> {
    const gateways = await this.gatewayService.getGateWaysByWorkPlaceId(
      workPlaceId,
    );
    return gateways;
  }

  async getZonesByWorkPlaceId(workPlaceId): Promise<Zone[]> {
    const zones = await this.zoneService.getZonesByWorkPlaceId(workPlaceId);
    return zones;
  }

  async getWorkPlacesByCategoryId(categoryId): Promise<WorkPlace[]> {
    const workplaces = await this.workPlaceModel.find({
      categoryId: categoryId,
    });
    return workplaces;
  }

  async getWorkPlacesByCompanyId(companyId): Promise<WorkPlace[]> {
    const workplaces = await this.workPlaceModel.find({
      companyId: companyId,
    });
    return workplaces;
  }

  async getWorkPlace(id): Promise<WorkPlace> {
    const workPlace = await this.workPlaceModel.findById(id);
    return workPlace;
  }

  async createWorkPlace(
    createWorkPlaceDTO: CreateWorkPlaceDTO,
  ): Promise<WorkPlace> {
    const newWorkPlace = new this.workPlaceModel(createWorkPlaceDTO);
    return await newWorkPlace.save();
  }

  async deleteWorkPlace(id): Promise<WorkPlace> {
    return await this.workPlaceModel.findByIdAndDelete(id);
  }

  async deleteWorkPlacesByCompanyId(companyId) {
    return await this.workPlaceModel.deleteMany({ companyId: companyId });
  }

  async updateWorkPlace(
    id: string,
    body: CreateWorkPlaceDTO,
  ): Promise<WorkPlace> {
    return await this.workPlaceModel.findByIdAndUpdate(id, body, {
      new: true,
    });
  }
}
