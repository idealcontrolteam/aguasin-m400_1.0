export class CreateWorkPlaceDTO {
  name: string;
  ubicacion: string;
  alertEmail: string;
  failEmail: string;
  active: boolean;
  companyId: string;
  categoryId: string;
}
