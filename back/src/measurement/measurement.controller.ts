import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Res,
  HttpStatus,
  Body,
  Param,
  NotFoundException,
  BadRequestException,
  UseGuards,
} from '@nestjs/common';
import { CreateMeasurementDTO } from './dto/measurement.dto';
import { MeasurementService } from './measurement.service';
import { AuthGuard } from 'src/shared/auth.guard';
import {_,orderBy} from 'lodash';
import { Measurement } from './interfaces/measurement.interface';
import { TagService } from '../tag/tag.service';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';


@Controller('measurement')
// //@UseGuards(new AuthGuard())
export class MeasurementController {
  constructor(private measurementService: MeasurementService,
              private tagService: TagService,
              @InjectConnection() private connection: Connection
  ) {}

  public validateIds = body => {
    Object.keys(body).map(key => {
      if (key != 'value' && key != 'dateTime' && key != 'active') {
        if (!body[key].match(/^[0-9a-fA-F]{24}$/)) {
          throw new BadRequestException(`${key} is not a valid ObjectId`);
        }
      }
    });
  };

  @Post('/multiple')
  async createMeasurementMultiple(@Res() res, @Body() body) {
     //this.validateIds(body);
    if(this.connection.readyState===0){
      throw new BadRequestException('Sin Conexión');
    }
    let newMeasurement = {};
    body.map(async (Measurement) => { 
      // console.log(Measurement);    
      const find_medicion=await this.measurementService.getMeasurementsDateEndTagId(Measurement.tagId,Measurement.dateTime) 
      if(find_medicion.length==0){
        // console.log(find_medicion.length)
        newMeasurement =  await this.measurementService.createMeasurement(Measurement);
        try{
            let tagId=Object.keys(Measurement).length>0&&Measurement.tagId
            let find_tag= await this.tagService.getTag(tagId)
            if(new Date(find_tag.dateTimeLastValue)<new Date(Measurement.dateTime)){
              find_tag.dateTimeLastValue=Measurement.dateTime;
              find_tag.lastValue=Measurement.value;
              let update_tag= await this.tagService.updateTag(tagId,find_tag)
              // return console.log(update_tag);
          }
        }catch(e){
          console.log("problemas");
        }
      }
    });  
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }
  
  @Post()
  async createMeasurement(@Res() res, @Body() body: CreateMeasurementDTO) {
    this.validateIds(body);

    const newMeasurement = await this.measurementService.createMeasurement(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }

  @Post('/array_tags')
  async getMeasurementArrayTag(@Res() res, @Body() body: any) {

    const newMeasurement = await this.measurementService.getMeasurementsArrayTag(
      body,
    );
    return res.status(HttpStatus.CREATED).json({
      statusCode: HttpStatus.CREATED,
      message: 'Measurement created successfully',
      data: newMeasurement,
    });
  }

  @Get()
  async getMeasurements(@Res() res) {
    const measurements = await this.measurementService.getMeasurements();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/all')
  async getMeasurementsAll(@Res() res) {
    const measurements = await this.measurementService.getMeasurementsAll();

    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/:measurementId')
  async getMeasurement(@Res() res, @Param('measurementId') measurementId) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    const measurement = await this.measurementService.getMeasurement(
      measurementId,
    );
    if (!measurement) {
      throw new NotFoundException('Measurement not found');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement found',
      data: measurement,
    });
  }

  @Post('/endtags')
  async getMeasurementByTagsEndfiltered(
    @Res() res,
    @Body() body: any
  ) {
    const measurements = await this.measurementService.getMeasurementByTagsEndfiltered(
      body,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

        //---------------------Aqui


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/tag/:tagId/:fini/:ffin')
  async getMeasurementByTagfiltered(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfiltered(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

        //---------------------Aqui


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/xy/tag/:tagId/:fini/:ffin')
  async getMeasurementByTagfilteredXY(
    @Res() res,
    @Param('tagId') tagId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!tagId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Tag id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByTagfilteredXY(
      tagId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

        //---------------------Aqui


    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }


  @Get('/sensor/:sensorId/:fini/:ffin')
  async getMeasurementBySensorfiltered(
    @Res() res,
    @Param('sensorId') sensorId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!sensorId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Sensor id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementBySensorfiltered(
      sensorId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Get('/location/:locationId/:fini/:ffin')
  async getMeasurementByLocationfiltered(
    @Res() res,
    @Param('locationId') locationId,
    @Param('fini') fini,
    @Param('ffin') ffin,
  ) {
    if (!locationId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Location id is not a valid ObjectId');
    }

    const measurements = await this.measurementService.getMeasurementByLocationfiltered(
      locationId,
      fini,
      ffin,
    );
    let msg =
      measurements.length == 0
        ? 'Measurements not found'
        : 'Measurements fetched';

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: msg,
      data: measurements,
      count: measurements.length,
    });
  }

  @Put('/:measurementId')
  async updateMeasurement(
    @Res() res,
    @Body() body: CreateMeasurementDTO,
    @Param('measurementId') measurementId,
  ) {
    if (!measurementId.match(/^[0-9a-fA-F]{24}$/)) {
      throw new BadRequestException('Measurement id is not a valid ObjectId');
    }

    this.validateIds(body);

    const updatedMeasurement = await this.measurementService.updateMeasurement(
      measurementId,
      body,
    );
    if (!updatedMeasurement) {
      throw new NotFoundException('Measurement not updated');
    }

    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement updated',
      data: updatedMeasurement,
    });
  }

  @Delete('/:measurementId')
  async deleteMeasurement(@Res() res, @Param('measurementId') measurementId) {
    const deletedMeasurement = await this.measurementService.deleteMeasurement(
      measurementId,
    );

    if (!deletedMeasurement) {
      throw new NotFoundException('Measurement not found');
    }
    return res.status(HttpStatus.OK).json({
      statusCode: HttpStatus.OK,
      message: 'Measurement deleted',
      data: deletedMeasurement,
    });
  }
}
