import { Schema } from 'mongoose';

export const MeasurementSchema = new Schema(
  {
    value: Number,
    dateTime: Date,
    tagId: {
      type: Schema.Types.ObjectId,
      ref: 'Tag',
      required: true,
    },
    sensorId: {
      type: Schema.Types.ObjectId,
      ref: 'Sensor',
      required: false,
    },
    locationId: {
      type: Schema.Types.ObjectId,
      ref: 'Location',
      required: true,
    },
    active: Boolean,
  },
  { versionKey: false },
);
