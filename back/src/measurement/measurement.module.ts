
import { Module,forwardRef } from '@nestjs/common';
import { MeasurementController } from './measurement.controller';
import { MeasurementService } from './measurement.service';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementSchema } from './schemas/measurement.schema';
// import { TagSchema } from 'src/tag/schemas/tag.schema';
// import { TagService } from './../tag/tag.service';
import { TagModule } from '../tag/tag.module';
//import { ViewsModule } from '../views/views.module';
//import {ViewsService} from '../views/views.service';


@Module({
  imports: [
    //ViewsModule,
    MongooseModule.forFeature([
      {
        name: 'Measurement',
        schema: MeasurementSchema,
        collection: 'measurement',
      },
    ]),
    forwardRef(() => TagModule),
  ],
  controllers: [MeasurementController],
  providers: [MeasurementService
              //ViewsService
  ],
  exports: [MeasurementService]
})
export class MeasurementModule {}
