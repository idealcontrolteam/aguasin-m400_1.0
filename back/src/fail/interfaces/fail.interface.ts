import { Document } from 'mongoose';

export interface Fail extends Document {
  dateTimeIni: Date;
  dateTimeTer: Date;
  description: string;
  tagId: string;
  active: boolean;
  presentValue: number;
  failValue: number;
  sentEmail: boolean;
  accepted: boolean;
}
