import { Schema } from 'mongoose';

export const FailSchema = new Schema(
  {
    dateTimeIni: Date,
    dateTimeTer: Date,
    description: String,
    tagId: {
      type: Schema.Types.ObjectId,
      ref: 'Tag',
      required: true,
    },
    active: Boolean,
    presentValue: Number,
    failValue: Number,
    sentEmail: Boolean,
    accepted: Boolean,
  },
  { versionKey: false },
);
