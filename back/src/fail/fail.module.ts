import { Module } from '@nestjs/common';
import { FailController } from './fail.controller';
import { FailService } from './fail.service';
import { MongooseModule } from '@nestjs/mongoose';
import { FailSchema } from './schemas/fail.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Fail', schema: FailSchema, collection: 'fail' },
    ]),
  ],
  controllers: [FailController],
  providers: [FailService],
  exports: [FailService],
})
export class FailModule {}
