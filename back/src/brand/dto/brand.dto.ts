export class CreateBrandDTO {
  name: string;
  active: boolean;
}
