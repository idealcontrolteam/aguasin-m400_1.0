import { Module } from '@nestjs/common';
import { BrandController } from './brand.controller';
import { BrandService } from './brand.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BrandSchema } from './schemas/brand.schema';
import { ModelModule } from '../model/model.module';
import { ModelService } from '../model/model.service';
import { from } from 'rxjs';

@Module({
  imports: [
    ModelModule,
    MongooseModule.forFeature([
      { name: 'Brand', schema: BrandSchema, collection: 'brand' },
    ]),
  ],
  controllers: [BrandController],
  providers: [BrandService, ModelService],
  exports: [BrandService],
})
export class BrandModule {}
