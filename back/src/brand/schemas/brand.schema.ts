import { Schema } from 'mongoose';

export const BrandSchema = new Schema(
  {
    name: String,
    active: Boolean,
  },
  { versionKey: false },
);

/*BrandSchema.methods.deleteModels = function(brandId) {
  return this.model('Model').find({ brandId: brandId });
};*/
